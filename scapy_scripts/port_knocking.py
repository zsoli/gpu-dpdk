#!/usr/bin/python3

import enum
import threading, time, argparse
from scapy.all import *
from scapy.layers.inet import *

DEFAULT_INTERFACE: str = "eth1"
DEFAULT_FILTER: str = "udp or tcp"
DEFAULT_REPEATS: int = 2
SEND_INTERVAL: float = 0.005

# num of pkts from CIDR
NUM_OF_PKTS: int = 4
NUM_OF_PORTS: int = 2
PORT_START_RANGE: int = 50_000

DST_IP: str = "10.0.2.0"
DST_MAC: str = "1c:34:da:6b:e9:74"

SRC_IP: str = "10.0.1.1"
SRC_MAC: str = "00:07:43:55:fc:70"


def sendpkt(repeats: int, interface: str) -> None:
    base_pkt = Ether(dst=DST_MAC, src=SRC_MAC)/IP(dst=DST_IP, src=SRC_IP)

    pkt = base_pkt/UDP(sport=11111, dport=[12345, 11111, 11111, 11111])
    sendp(pkt, iface=interface, inter=SEND_INTERVAL, count=repeats, realtime=True)

    time.sleep(0.5)
    
    pkt = base_pkt/UDP(sport=11111, dport=[23456, 11111, 11111, 11111])
    sendp(pkt, iface=interface, inter=SEND_INTERVAL, count=repeats, realtime=True)

    time.sleep(0.5)

    pkt = base_pkt/UDP(sport=11111, dport=[34567, 11111, 11111, 11111])
    sendp(pkt, iface=interface, inter=SEND_INTERVAL, count=repeats, realtime=True)
    
    time.sleep(0.5)

    pkt = base_pkt/TCP(sport=11111, dport=[22, 11111, 11111, 11111])
    sendp(pkt, iface=interface, inter=SEND_INTERVAL, count=repeats, realtime=True)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--repeats', type=int)
    parser.add_argument('-i', '--interface', type=str)
    return parser.parse_args()


def main(args) -> None:
    repeats: int = args.repeats if args.repeats else DEFAULT_REPEATS
    interface: str = args.interface if args.interface else DEFAULT_INTERFACE

    print("input options:")
    print(f"\t repeats: {repeats}")
    print(f"\t interface: {interface}")

    sendpkt(repeats, interface)


if __name__ == "__main__":
    args = parse_args()
    main(args)
