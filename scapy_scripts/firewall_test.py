#!/usr/bin/python3

import enum
import threading, time, argparse
from scapy.all import *
from scapy.layers.inet import *

DEFAULT_INTERFACE: str = "eth1"
DEFAULT_FILTER: str = "udp or tcp"
DEFAULT_REPEATS: int = 2
SEND_INTERVAL: float = 0.005

# num of pkts from CIDR
NUM_OF_PKTS: int = 4
NUM_OF_PORTS: int = 2
PORT_START_RANGE: int = 50_000

DST_IP: str = "10.0.2.0/30"
DST_MAC: str = "1c:34:da:6b:e9:74"

SRC_IP: str = "10.0.1.0/30"
SRC_MAC: str = "00:07:43:55:fc:70"


class SniffThread(threading.Thread):
    def __init__(self, sniff_timeout: int, sniff_count: int, repeats: int, sniff_filter: str, interface: str) -> None:
        threading.Thread.__init__(self)
        self.sniff_timeout = sniff_timeout
        self.sniff_count = sniff_count
        self.repeats = repeats
        self.sniff_filter = sniff_filter
        self.interface = interface


    def process_pkt(self, p) -> None:
        p.show()


    def calc_pkt_hash(self, p) -> int:
        ret: int = 0

        dst_ip_bytes: list = [int(x) for x in p["IP"].dst.split('.')]
        src_ip_bytes: list = [int(x) for x in p["IP"].src.split('.')]
        proto: str = "UDP" if UDP in p else "TCP"

        ret += dst_ip_bytes[0] * 373
        ret += dst_ip_bytes[1] * 137
        ret += dst_ip_bytes[2] * 593
        ret += dst_ip_bytes[3] * 643
        
        ret += src_ip_bytes[0] * 113
        ret += src_ip_bytes[1] * 109
        ret += src_ip_bytes[2] * 571
        ret += src_ip_bytes[3] * 727

        ret += 739 if proto == "UDP" else 269
        ret += p[proto].sport * 443 + p[proto].dport * 233

        return ret


    def run(self) -> None:
        packets = sniff(filter=self.sniff_filter, timeout=self.sniff_timeout, iface=self.interface, count=self.sniff_count)
        pkts: dict = dict()

        for i in range(len(packets)):
            p = packets[i]

            src_ip = p["IP"].src
            dst_ip = p["IP"].dst
            proto = "UDP" if UDP in p else "TCP"
            sport = p[proto].sport
            dport = p[proto].dport
            print(f"sent {proto} packet with ip: {src_ip} -> {dst_ip}, port: {sport} -> {dport}")
            
            pkt_hash: int = self.calc_pkt_hash(p)
            tx: bool = True if p["Ether"].src == DST_MAC else False

            if pkt_hash not in pkts:
                #print(f"src: {p['Ether'].src}, dst: {p['Ether'].dst}")
                pkts[pkt_hash] = {"c": 0, "pkt": p, "is_tx": tx}
            else:
                pkts[pkt_hash]["c"] += 1
                pkts[pkt_hash]["is_tx"] = tx
        
        print(f"number of unique rx pkts: {len(pkts)}")

        print("missed packets:")
        for hash in pkts:
            #print(pkts[hash]["c"])
            if pkts[hash]["c"] < 2 and not pkts[hash]["is_tx"]:
                p = pkts[hash]['pkt']
                src_ip = p["IP"].src
                dst_ip = p["IP"].dst
                proto = "UDP" if UDP in p else "TCP"
                sport = p[proto].sport
                dport = p[proto].dport
                print(f"{proto} packet with ip: {src_ip} -> {dst_ip}, port: {sport} -> {dport}")


def sendpkt(repeats: int, interface: str) -> None:
    sport_range: list = [x for x in range(PORT_START_RANGE, PORT_START_RANGE + NUM_OF_PORTS)]
    dport_range: list = [x for x in range(PORT_START_RANGE, PORT_START_RANGE + NUM_OF_PORTS)]

    base_pkt = Ether(dst=DST_MAC, src=SRC_MAC)/IP(dst=DST_IP, src=SRC_IP)
    udp_pkt = base_pkt/UDP(sport=sport_range, dport=dport_range)
    tcp_pkt = base_pkt/TCP(sport=sport_range, dport=dport_range)

   #print("UDP test packet:")
   #udp_pkt.show()
   #print("TCP test packet:")
   #tcp_pkt.show()
    
    sendp(udp_pkt, iface=interface, inter=SEND_INTERVAL, count=repeats, realtime=True)
    sendp(tcp_pkt, iface=interface, inter=SEND_INTERVAL, count=repeats, realtime=True)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--repeats', type=int)
    parser.add_argument('-i', '--interface', type=str)
    parser.add_argument('-f', '--filter', type=str)
    return parser.parse_args()


def main(args) -> None:
    repeats: int = args.repeats if args.repeats else DEFAULT_REPEATS
    interface: str = args.interface if args.interface else DEFAULT_INTERFACE
    sniff_filter: str = args.filter if args.filter else DEFAULT_FILTER

    sniff_count: int = pow(NUM_OF_PKTS, 2) * pow(NUM_OF_PORTS, 2) * repeats * 2 * 2
    print(f"sniff count: {sniff_count}")
    sniff_timeout: float = max(SEND_INTERVAL * sniff_count, 3)

    print("input options:")
    print(f"\t repeats: {repeats}")
    print(f"\t interface: {interface}")
    print(f"\t sniff_filter: {sniff_filter}")

    t = SniffThread(sniff_timeout, sniff_count, repeats, sniff_filter, interface)
    t.start()

    time.sleep(1)
    sendpkt(repeats, interface)

    t.join()


if __name__ == "__main__":
    args = parse_args()
    main(args)
