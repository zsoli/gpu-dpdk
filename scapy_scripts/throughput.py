#!/usr/bin/python3

import threading, argparse
from scapy.all import *
from scapy.layers.inet import *

DEFAULT_PACKET_AMOUNT: int = 12_000_000
DEFAULT_QUEUE_AMOUNT: int = 4
DEFAULT_PAYLOAD_SIZE: int = 1000
DEFAULT_INTERFACE: str = "enp11s0f4"
DEFAULT_THREAD_AMOUNT: int = 8

DST_IP: str = "10.0.1.0/24"
DST_MAC: str = "1c:34:da:6b:e9:74"

SRC_IP: str = "10.0.1.2"
SRC_MAC: str = "00:07:43:55:fc:70"


class GeneratorThread (threading.Thread):
    def __init__(self, name, packet, interface, loops) -> None:
        threading.Thread.__init__(self)
        self.name = name
        self.packet = packet
        self.interface= interface
        self.loops = loops
        
    
    def run(self) -> None:
        print("starting " + self.name)
        self.generate_pkts()
        print("exiting " + self.name)


    def generate_pkts(self) -> None:
        #packet.show()
        #sendp(packet, iface=interface, count=pkt_count)
        sendpfast(self.packet, iface=self.interface, loop=self.loops)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--packets', type=int)
    parser.add_argument('-q', '--queues', type=int)
    parser.add_argument('-i', '--interface', type=str)
    parser.add_argument('-t', '--threads', type=int)
    parser.add_argument('--payload-size', type=int)
    return parser.parse_args()


def main(args) -> None:
    packet_amount: int = args.packets if args.packets else DEFAULT_PACKET_AMOUNT
    queue_amount: int = args.queues if args.queues else DEFAULT_QUEUE_AMOUNT
    payload_size: int = args.payload_size if args.payload_size else DEFAULT_PAYLOAD_SIZE
    interface: str = args.interface if args.interface else DEFAULT_INTERFACE
    thread_amount: int = args.threads if args.threads else DEFAULT_THREAD_AMOUNT

    payload = open("/dev/urandom","rb").read(payload_size)
    total_pkt_count: int = packet_amount * queue_amount
    loop_count: int = round(total_pkt_count / 256 / thread_amount)

    print("input options:")
    print(f"\t payload size: {payload_size} bytes")
    print(f"\t queues: {queue_amount}")
    print(f"\t threads: {thread_amount}")
    print(f"\t interface: {interface}")
    print(f"\t packets per queue: {packet_amount:,}")

    packet = Ether(dst=DST_MAC, src=SRC_MAC)/IP(dst=DST_IP, src=SRC_IP)/ICMP()
    print("\ntest packet without payload:")
    packet.show()
    packet = packet/payload

    threads = []
    for i in range(0, thread_amount):
        t = GeneratorThread(f"thread-{i}", packet, interface, loop_count)
        threads.append(t)
        t.start()

    for t in threads:
        t.join()

    print("exiting Main Thread")


if __name__ == "__main__":
    args = parse_args()
    main(args)
