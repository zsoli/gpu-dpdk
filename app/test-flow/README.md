# Flow bifurcation for CPU and GPU

## Synopsis

This is a sample application which demonstrates two techniques which are useful in using GPU+DPDK:

 - [flow bifurcation](https://doc.dpdk.org/guides/howto/flow_bifurcation.html#using-flow-bifurcation-on-mellanox-connectx) to direct certain traffic to CPU-accessible memory and some traffic to GPU-accessible memory.
 - Buffer Split on Rx - an addition on top of vanilla DPDK which makes certain portions of the packets to be written to either CPU or GPU memory

### flow bifurcation

The discriminating factor is the src MAC address in the ethernet header of the packet. There are two receive queues. Then, NIC is programmed to direct traffic matching certain MAC addresses to RX queue 0 and traffic matching certain other MAC addresses to RX queue 1. Furthermore, flow isolation mode is enabled so that only matching traffic is directed to the queues.

## Running

Run one instance of the application as the receiver:

```
sudo ./build/app/dpdk-test-flow -a b5:00.1 -- --recv
```

and another instance of the application on another server as the sender:

```
sudo ./build/app/dpdk-test-flow -a b5:00.1 -- --send
```

### Understanding output

Sample output from the sender:
```
$ sudo  ./build/app/dpdk-test-flow -a b5:00.1 -- --send
EAL: Detected 24 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket
EAL: Selected IOVA mode 'PA'
EAL: Probing VFIO support...
EAL: VFIO support initialized
EAL: Probe PCI driver: mlx5_pci (15b3:101d) device: 0000:b5:00.1 (socket 0)
mlx5_pci: Size 0xFFFF is not power of 2, will be aligned to 0x10000.
EAL: No legacy callbacks, legacy socket not created
Will transmit 512 packets in total
TX iteration 0 complete. src_mac0 payload: (0x11,0x22). src_mac1 payload: (0xaa,0xbb)
```

Sample output from the receiver:
```
$ sudo  ./build/app/dpdk-test-flow -a b5:00.1 -- --recv
EAL: Detected 24 lcore(s)
EAL: Detected 1 NUMA nodes
EAL: Multi-process socket /var/run/dpdk/rte/mp_socket
EAL: Selected IOVA mode 'PA'
EAL: Probing VFIO support...
EAL: VFIO support initialized
EAL: Probe PCI driver: mlx5_pci (15b3:101d) device: 0000:b5:00.1 (socket 0)
mlx5_pci: Size 0xFFFF is not power of 2, will be aligned to 0x10000.
EAL: No legacy callbacks, legacy socket not created
Will receive 512 packets in total, 256 on each queue
RX iteration 0 complete. Total pkts RX'd: 0 / 0
Queue 0 msgs:
    ==>  0x11 (NON-GPU) |  0x22 (NON-GPU)
    ==>  0x11 (NON-GPU) |  0x22 (NON-GPU)
    ==>  0x11 (NON-GPU) |  0x22 (NON-GPU)
    ==>  0x11 (NON-GPU) |  0x22 (NON-GPU)
All messages received in correct memory locations
Queue 1 msgs:
    ==>  0xaa (NON-GPU) |  0xbb (GPU)
    ==>  0xaa (NON-GPU) |  0xbb (GPU)
    ==>  0xaa (NON-GPU) |  0xbb (GPU)
    ==>  0xaa (NON-GPU) |  0xbb (GPU)
All messages received in correct memory locations
```

In the outputs above one can see that sender sends frames to two different MAC addresses; frames to different MAC addresses carry different messages at predetermined offsets in the message.

The receiver sets up flow steering rules so that it receives messages matching MAC address 0 on RX queue 0 and messages matching MAC address 1 on RX queue 1. Furthermore, queue 1 uses buffer split between CPU and GPU memory. The split on queue 1 is set up so that the first message is received in CPU memory and the second message is received in GPU memory. You can see in the parentheses where the message is being read from on the receiver.

You can also use the --forever 

## Known limitations

- Many parameters are hard-coded as `#define` instead of being provided as options.
- Modifications required by this applications have only been introduced for the Mellanox drivers
