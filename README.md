# GPU DPDK

## Synopsis

[DPDK](http://dpdk.org) is a set of libraries and drivers for fast packet processing.

In this repo we present a modified version of DPDK 20.11 with GPU-specific modifications, extensions and sample code. The modifications are centered around enabling fast packet processing on the GPUs by using [GPUDirect RDMA](https://docs.nvidia.com/cuda/gpudirect-rdma/index.html) to receive and send ethernet traffic directly using GPU memory.

Main changes with respect to vanilla DPDK are:
- A new `librte_nv` library containing a modified version of the [GDRCopy](https://github.com/NVIDIA/gdrcopy)
- A sample application, `l2fwd-nv` demonstrating L2 forwarding of packets using GPUs
- Modifications to the `testpmd` application, allowing IO forwarding of packets using GPU device memory
- A sample application which uses flow bifurcation to direct certain traffic to CPU memory and other traffic to GPU memory
- The default `RTE_PKTMBUF_HEADROOM` has been changed to 0 to improve GPUDirect RDMA performance
- A number of meson build options were added to allow changing GPU DPDK settings easily. For a full list of available options, run `meson configure`

## System Requirements

Supported SW/HW configuration:
- Ubuntu 16.04 or newer
- Linux Kernel 4.4 or newer
- Intel x86 CPUs
- Mellanox ConnectX-5 VPI/EN 100Gbps (or newer)

NVIDIA GPUs:
- A Tesla- or Quadro-class GPU
- CUDA Toolkit 11.1 or newer with the corresponding driver
- For the best GPUDirect RDMA performance, a PCIe switch should be used and the GPU and the NIC should be placed on the same PCIe switch. For example:
```
+-[0000:3a]-+-00.0-[3b-41]----00.0-[3c-41]--+-04.0-[3d]----00.0  NVIDIA Corporation GV100GL [Tesla V100 PCIe 16GB]
                                            +-08.0-[3e]--
                                            +-0c.0-[3f]--+-00.0  Mellanox Technologies MT27800 Family [ConnectX-5]
```

For detailed instructions on setting up the system, please see the sections at the end of this document.

## GPUDirect RDMA

### DPDK support for GPU pinned mbuf pools

Since DPDK release 20.05, it is possible to perform GPUDirect RDMA on Mellanox NICs using upstream APIs which is the approach that we encourage. The overall approach can be broken down into the following steps:

1. Allocate unmanaged GPU memory with `cudaMalloc`
2. Make this memory "visible" to DPDK with `rte_extmem_register`
3. Ensure that the NICs with which you intend to use this memory can create appropriate DMA mappings with `rte_dev_dma_map`
4. Finally, create a pktmbuf pool referencing the GPU memory using `rte_pktmbuf_pool_create_extbuf`
5. On exit, memory cleanup can be achieved by destroying the mempool, calling `rte_dev_dma_unmap`, `rte_extmem_unregister` and finally `cudaFree`.

The code snippped below should serve as an introductory example but we strongly encourage users to study the documentation of the functions involved.

```c
#define CUDA_CHECK(expr) \
    do { \
        cudaError_t e = expr; \
        if (e != cudaSuccess) \
            rte_panic("CUDA ERROR. %s returned %d:%s\n", #expr, e, cudaGetErrorString(e)); \
    } while (0)
#define GPU_PAGE_SZ (1UL<<16)

unsigned int mpool_cnt = <...>; /* the desired mbuf count */
uint16_t droom_sz = <...>; /* the desired dataroom size */

/* Select the appropriate GPU */
CUDA_CHECK(cudaSetDevice(GPU_ID));

/* Create the external memory descriptor */
struct rte_pktmbuf_extmem gpu_mem;
gpu_mem.buf_iova = RTE_BAD_IOVA; /* we'll ask NIC to create its own mappings */
gpu_mem.buf_len = RTE_ALIGN(mpool_cnt * droom_sz, GPU_PAGE_SZ);
gpu_mem.elt_size = droom_sz;

/* Allocate GPU memory */
CUDA_CHECK(cudaMalloc(&gpu_mem.buf_ptr, gpu_mem.buf_len));
if (NULL == gpu_mem.buf_ptr)
    rte_panic("Could not allocate GPU device memory\n");

/* Make the GPU memory visible to DPDK */
if (0 != rte_extmem_register(gpu_mem.buf_ptr, gpu_mem.buf_len, NULL, gpu_mem.buf_iova, NV_GPU_PAGE_SIZE))
    rte_panic("Unable to register addr 0x%p\n", gpu_mem.buf_ptr);

/* Create DMA mappings on the NIC */
if (0 != rte_dev_dma_map(rte_eth_devices[PORT_ID].device, gpu_mem.buf_ptr, gpu_mem.buf_iova, gpu_mem.buf_len))
    rte_panic("Unable to DMA map addr 0x%p for device %s\n",
            gpu_mem.buf_ptr, rte_eth_devices[PORT_ID].data->name);

/* Create the actual mempool */
struct rte_mempool *mpool = rte_pktmbuf_pool_create_extbuf(
                                    "gpu_mempool",     /* name of the mempool */
                                    mpool_cnt,         /* number of mbufs */
                                    MEMPOOL_CACHE,     /* size of the mempool cache */
                                    0,                 /* private size in each mbuf */
                                    droom_sz,          /* dataroom size of each mbuf */
                                    rte_socket_id(),   /* the socket id for memory allocation */
                                    &gpu_mem, 1        /* external memory description */
                                );
if (mpool == NULL)
    rte_panic("Could not create gpu mempool\n");
```

#### Behind the scenes

On a high level, the mechanics of enabling GPUDirect RDMA are as follows: when user requests that DMA mapping should be created (`rte_dev_dma_map`), Mellanox drivers recognize that the virtual address provided in the call represents GPU memory (that is, if `nv_peer_mem` is loaded).
The kernel-mode driver then interacts with the `nvidia` driver to page-lock the GPU memory and expose it via BAR1 mappings.
These BAR1 mappings are then used by the NIC to populate its Memory Regions corresponding to the GPU virtual addresses. 

In each of the mbufs, `buf_addr` will contain a GPU VA address which can be passed to a GPU kernel for accessing the received data.

For more detailed description we recommend reviewing the GPUDirect RDMA documentation [available online](https://docs.nvidia.com/cuda/gpudirect-rdma/index.html).

### NVIDIA Examples

#### `l2fwd-nv`

To provide an example of the different use cases with DPDK-GPU, we extended the vanilla examples/l2fwd by adding mbuf pool with GPU pinned data buffers. The modified app is available under examples/l2fwd-nv (see [examples/l2fwd-nv/README.md](examples/l2fwd-nv/README.md) for further details).

#### `test-flow`

This is a sample application which demonstrates two techniques which are useful in using GPU+DPDK:

 - [flow bifurcation](https://doc.dpdk.org/guides/howto/flow_bifurcation.html#using-flow-bifurcation-on-mellanox-connectx) to direct certain traffic to CPU-accessible memory and some traffic to GPU-accessible memory.
 - Buffer Split on Rx - an addition on top of vanilla DPDK which makes certain portions of the packets to be written to either CPU or GPU memory

Please see [app/test-flow/README.md](app/test-flow/README.md) for more details

#### `testpmd`

##### IO forwarding in GPU memory

Run `testpmd` in IO forwarding mode as usual, providing mbuf size with a 'g' suffix (e.g. `--mbuf-size=2048g`). This will cause the mempool used for IO forwarding to contain buffers from the first GPU enumerated by CUDA. You can control which GPU is being used by setting the environment variable [`CUDA_VISIBLE_DEVICES`](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#env-vars).

##### Buffer Split on Rx

It is also possible to configure testpmd to perform buffer split on Rx. The `--mbuf-size` option recognizes multiple sizes joined by a comma. E.g. `--mbuf-size=1024,2048g` would cause testpmd to use two mempools - one with CPU buffers with 1024 bytes of dataroom (assuming 0-byte headroom) and another one with GPU buffers with 2048 bytes of dataroom. The 'g' suffix distinguishes GPU mempools from CPU mempools. To split the incoming packets between the two mempools defined earlier, use the `--rxpkts` option. E.g. `--rxpkts=32,2048` would cause the first 32 bytes of each packet to be written to the 1024B mbuf from CPU memory and the remaining 2048 bytes (or less) - into GPU memory. 

Note that when doing Buffer Split on Rx with the first segment received in CPU memory, one can re-enable inlining on the PMD level, up to the number of bytes delivered to CPU memory. To learn more about this, please see the Limitations section at the end of this document.

Note also that for buffer split to work in IO forwarding, it needs two additional options: `--rx-offloads=0x00102000` to enable `DEV_RX_OFFLOAD_SCATTER` and `RTE_ETH_RX_OFFLOAD_BUFFER_SPLIT`, and  `--tx-offloads=0x00008000` to enable `DEV_TX_OFFLOAD_MULTI_SEGS`.

# System setup

#### BIOS setup
For maximum performance, we advise to disable Advanced Power Management or set the profile to "Performance".
We also recommend first testing GPU DPDK performance with HyperThreading disabled.
Finally, while in BIOS, please verify the enumaration scheme of the CPU cores.
Some multi-socket platforms enumerate CPU cores in a round-robin fashion, so that Linux core #0 would correspond to the first CPU core on the first socket, Linux core #1 would correspond to the first CPU core on the second socket, etc.
Please either change this enumeration scheme in BIOS or account for it while setting up the system and running the applications.

#### Software prerequisites: Ubuntu 16.04
```
sudo apt-get install build-essential libnuma1 libnuma-dev libmnl-dev libmnl0 libnl-3-200 \
        libnl-3-dev libnl-genl-3-200 libnl-genl-3-dev libnl-route-3-200 libnl-route-3-dev \
        python3-setuptools ninja-build
```

#### Installing Meson
```
wget https://github.com/mesonbuild/meson/releases/download/0.56.0/meson-0.56.0.tar.gz
tar xvfz meson-0.56.0.tar.gz
cd meson-0.56.0
sudo python3 setup.py install
```

#### Kernel configuration

Ensure that your kernel parameters include the following list:
```
default_hugepagesz=1G hugepagesz=1G hugepages=16 tsc=reliable clocksource=tsc intel_idle.max_cstate=0 mce=ignore_ce processor.max_cstate=0 audit=0 idle=poll isolcpus=2-21 nohz_full=2-21 rcu_nocbs=2-21 rcu_nocb_poll nosoftlockup iommu=off intel_iommu=off
```
Note that `2-21` corresponds to the list of CPUs you intend to use for the DPDK application and the value of this parameter needs to be changed depending on the HW configuration.

To permanently include these items in the kernel parameters, open `/etc/default/grub` with your favourite text editor and add them to the variable named `GRUB_CMDLINE_LINUX_DEFAULT`. Save this file, install new GRUB configuration and reboot the server:

```
$ sudo vim /etc/default/grub
$ sudo update-grub
$ sudo reboot
```

After reboot, verify that the changes have been applied. Example output:

```
$ cat /proc/cmdline 
BOOT_IMAGE=/vmlinuz-5.4.0-53-lowlatency root=/dev/mapper/ubuntu--vg-ubuntu--lv ro maybe-ubiquity default_hugepagesz=1G hugepagesz=1G hugepages=16 tsc=reliable clocksource=tsc intel_idle.max_cstate=0 mce=ignore_ce processor.max_cstate=0 idle=poll isolcpus=2-21 nohz_full=2-21 rcu_nocbs=2-21 nosoftlockup iommu=off intel_iommu=off
$ grep -i huge /proc/meminfo
AnonHugePages:         0 kB
ShmemHugePages:        0 kB
HugePages_Total:      16
HugePages_Free:       15
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:    1048576 kB
Hugetlb:        16777216 kB
```

#### GDRdrv

The `l2fwd-nv` application relies on GDRCopy for interacting directly with GPU memory from the CPU threads. In this repository, we include a modified version of [GDRCopy](https://github.com/NVIDIA/gdrcopy). You can build and install the kernelmode component using the following commands:
```
$ cd kernel/linux/gdrdrv/x86_64
$ make
$ sudo ./insmod.sh
```

#### Mellanox NIC setup

1. Download the latest Mellanox OFED SW from [here](http://www.mellanox.com/page/products_dyn?product_family=26)
2. Install OFED for DPDK with `sudo ./mlnxofedinstall --upstream-libs --dpdk --with-mft`
    * Make sure Mellanox OFED is installed on the system before building gpu-dpdk to guarantee that mlx5 PMD is built 
3. Enable CQE compression `mlxconfig -d <NIC PCIe address> set CQE_COMPRESSION=1`
4. Build and install the [nv_peer_mem](https://github.com/Mellanox/nv_peer_memory) kernel module
    * In case of NVIDIA Tegra devices `nv_peer_mem` is not required

If the Mellanox NIC supports IB and Ethernet mode (VPI adapters):
1. Set the IB card as an Ethernet card `mlxconfig -d <NIC PCIe address> set LINK_TYPE_P1=2 LINK_TYPE_P2=2`
2. Reboot the server or `mlxfwreset -d <NIC PCIe address> reset` and `/etc/init.d/openibd restart`

#### System tuning

Please note that most of these settings do not persist across reboots, so these steps will be required after each boot.

##### Disable flow control on the ethernet link:
```
ethtool -A <eth iface name> rx off tx off
```
Please note that in case of a dual-port NIC (e.g. Mellanox ConnectX-5) you need to disable the flow control separately on each port.

##### For Jumbo frames, increase the MTU size:
```
ifconfig <eth iface name> mtu <size>
```

##### Enable persistence mode on the GPUs to be used:
```
sudo nvidia-smi -pm ENABLED
```

##### Lock the GPU clocks to the maximum supported frequency:
```
sudo nvidia-smi -i 0 -lgc $(sudo nvidia-smi -i 0 --query-supported-clocks=graphics --format=csv,noheader,nounits | sort -h | tail -n 1)
```

##### Set PCIe MaxReadRequest parameter on the NIC

For each of the NIC ports, set the PCIe MaxReadReq parameter to 2048B (if you expect to work with regular Ethernet frames). For larger JUMBO frames, consider setting this to 4096B.
Please see [Mellanox documentation](https://community.mellanox.com/s/article/understanding-pcie-configuration-for-maximum-performance#PCIe-Max-Read-Request) for detailed instructions.

##### Various other OS tunings
```
sysctl -w vm.zone_reclaim_mode=0
sysctl -w vm.swappiness=0
# Move all IRQs to far NUMA node
IRQBALANCE_BANNED_CPUS=$LOCAL_NUMA_CPUMAP irqbalance --oneshot
systemctl stop irqbalance
```

## Building GPU DPDK

In the dpdk main directory:

```
meson build
meson configure build -Dexamples=l2fwd-nv   # <-- To also build the l2fwd-nv example
meson configure build -Dmachine=default     # <-- To use builder-independent baseline -march
meson configure build -Dnvidia=false        # <-- To disable Nvidia libraries and examples
meson configure build -Dheadroom=256        # <-- To change packet headroom from 0 to 256 bytes
cd build
ninja
```

If CUDA is installed to a non-standard location (i.e. not `/usr/local/cuda`), please also modify the following settings before building:
```
meson configure build -Dhdr_cuda_dir=/path/to/cuda/headers (e.g. cuda.h)
meson configure build -Dlib_cuda_dir=/path/to/libcuda.so
meson configure build -Dlib_cudart_dir=/path/to/libcudart.so
meson configure build -Dbin_cuda_dir=/path/to/cuda/binaries (e.g. nvcc)
```

## Benchmarking GPU DPDK

This section is intended to provide guidance on how to do basic assessment of GPU DPDK performance.

### Throughput measurements

#### Methodology

In this section, we describe a way to perform rudimental measurements of throughput forwarded by GPU DPDK apps if supplied with full linerate traffic. Specifically, note that this is *not* zero packet loss testing (e.g. as described in RFC 2544).

#### Traffic generator

Unless you use a professional-grade traffic generator, we suggest using testpmd as the traffic generator. An example command line looks like the one below:
```
sudo ./build/app/dpdk-testpmd -l 2-10 --main-lcore=2 -a b5:00.1 -- \
        --port-numa-config=0,0 --socket-num=0 --burst=64 --txd=1024 \
        --rxd=1024 --mbcache=512 --rxq=8 --txq=8 --forward-mode=txonly \
        -i --nb-cores=8 --txonly-multi-flow
```

In the interactive mode, use command like `set txpkts`, `start` and `stop` to control the generation of packets. Remember that the packet size set in testpmd using `set texpkts` does NOT account for the FCS, so e.g. to send a 64B packet, you have to `set txpkts 60`.

#### Throughput measurement tools.

Instead of relying on the values reported by DPDK, we've found it preferable to query HW counters of the NIC and calculate the instantaneous bandwidth from that. Of course, if you're using a professional-grade traffic generator and analyzer, this does not apply.

A sample bash script for measuring throughput on a selected NIC looks like this:
<details>
  <summary>eth_stat.sh</summary>

```
#!/bin/bash

IFG_PLUS_PREAMBLE=20 # IFG 12B + Preamble 8B

update_stats () { # $name $index
    TS_LAST[$2]=${TS[$2]}
    RX_OOB_LAST[$2]=${R_OOB[$2]}
    R_PKT_LAST[$2]=${R_PKT[$2]}
    R_BYTE_LAST[$2]=${R_BYTE[$2]}
    T_PKT_LAST[$2]=${T_PKT[$2]}
    T_BYTE_LAST[$2]=${T_BYTE[$2]}
    R_DISC_LAST[$2]=${R_DISC[$2]}
    T_DISC_LAST[$2]=${T_DISC[$2]}
    R_DISC_PRIO0_LAST[$2]=${R_DISC_PRIO0[$2]}

    ETHTOOL=($(ethtool -S $1 | awk '/rx_out_of_buffer/{print $2} /tx_packets_phy/{print $2} /rx_packets_phy/{print $2} /tx_bytes_phy/{print $2} /rx_bytes_phy/{print $2} /rx_discards_phy/{print $2} /tx_discards_phy/{print $2} /rx_prio0_discards/{print $2}'))
    if [ -z "$ETHTOOL" ]; then
        ETHTOOL=($(ethtool -S $1 | awk '/tx_packets/{print $2} /rx_packets/{print $2} /tx_bytes/{print $2} /rx_bytes/{print $2}'))
    fi

    TS[$2]=$(date +%s%6N) # in usec
    R_OOB[$2]=${ETHTOOL[0]}
    T_PKT[$2]=${ETHTOOL[1]}
    R_PKT[$2]=${ETHTOOL[2]}
    T_BYTE[$2]=${ETHTOOL[3]}
    R_BYTE[$2]=${ETHTOOL[4]}
    R_DISC[$2]=${ETHTOOL[5]}
    T_DISC[$2]=${ETHTOOL[6]}
    R_DISC_PRIO0[$2]=${ETHTOOL[7]}
}

if [ -z $1 ] && [ -x /usr/bin/ibdev2netdev ]; then
    NETIF=$(ibdev2netdev | awk '/mlx/{print $5}')
else
    NETIF=$@
fi

if [ -z "$NETIF" ]; then
    printf "No interface can't be found\n"
    exit 1
fi

# set initial value
index=0
for name in $NETIF; do 
    update_stats $name $index
    ((index++))
done

index=0
for name in $NETIF; do 
    R_PKT_INIT[$index]=${R_PKT[$index]}
    T_PKT_INIT[$index]=${T_PKT[$index]}
    R_PKT_RATE_MAX[$index]=0
    T_PKT_RATE_MAX[$index]=0
    R_BYTE_INIT[$index]=${R_BYTE[$index]}
    T_BYTE_INIT[$index]=${T_BYTE[$index]}
    R_BIT_RATE_MAX[$index]=0
    T_BIT_RATE_MAX[$index]=0
    R_OOB_INIT[$index]=${R_OOB[$index]}
    R_DISC_INIT[$index]=${R_DISC[$index]}
    T_DISC_INIT[$index]=${T_DISC[$index]}
    R_DISC_PRIO0_INIT[$index]=${R_DISC_PRIO0[$index]}
    ((index++))
done

sleep 1

while true; do

    index=0
    for name in $NETIF; do 
        update_stats $name $index

        TS_DIFF=$((${TS[$index]} - ${TS_LAST[$index]}))

        R_PKT_DELTA=$(( ${R_PKT[$index]} - ${R_PKT_LAST[$index]} ))
        R_PKT_RATE=$(( $R_PKT_DELTA * 1000000 / $TS_DIFF))
        if [ $R_PKT_RATE -gt ${R_PKT_RATE_MAX[$index]} ]; then
            R_PKT_RATE_MAX[$index]=$R_PKT_RATE
        elif [ $R_PKT_RATE -eq 0 ]; then
            R_PKT_RATE_MAX[$index]=0
        fi

        R_BIT_DELTA=$(( (${R_BYTE[$index]} - ${R_BYTE_LAST[$index]} + $IFG_PLUS_PREAMBLE * $R_PKT_DELTA) * 8 ))
        R_BIT_RATE=$(( $R_BIT_DELTA * 1000000 / $TS_DIFF))
        if [ $R_BIT_RATE -gt ${R_BIT_RATE_MAX[$index]} ]; then
            R_BIT_RATE_MAX[$index]=$R_BIT_RATE
        elif [ $R_BIT_RATE -eq 0 ]; then
            R_BIT_RATE_MAX[$index]=0
        fi

        T_PKT_DELTA=$(( ${T_PKT[$index]} - ${T_PKT_LAST[$index]} ))
        T_PKT_RATE=$(( $T_PKT_DELTA * 1000000 / $TS_DIFF))
        if [ $T_PKT_RATE -gt ${T_PKT_RATE_MAX[$index]} ]; then
            T_PKT_RATE_MAX[$index]=$T_PKT_RATE
        elif [ $T_PKT_RATE -eq 0 ]; then
            T_PKT_RATE_MAX[$index]=0
        fi

        T_BIT_DELTA=$(( (${T_BYTE[$index]} - ${T_BYTE_LAST[$index]} + $IFG_PLUS_PREAMBLE * $T_PKT_DELTA) * 8 ))
        T_BIT_RATE=$(( $T_BIT_DELTA * 1000000 / $TS_DIFF))
        if [ $T_BIT_RATE -gt ${T_BIT_RATE_MAX[$index]} ]; then
            T_BIT_RATE_MAX[$index]=$T_BIT_RATE
        elif [ $T_BIT_RATE -eq 0 ]; then
            T_BIT_RATE_MAX[$index]=0
        fi

        R_PKT_TOTAL=$(( ${R_PKT[$index]} - ${R_PKT_INIT[$index]} ))
        T_PKT_TOTAL=$(( ${T_PKT[$index]} - ${T_PKT_INIT[$index]} ))

        R_BYTE_TOTAL=$(( ${R_BYTE[$index]} - ${R_BYTE_INIT[$index]} ))
        T_BYTE_TOTAL=$(( ${T_BYTE[$index]} - ${T_BYTE_INIT[$index]} ))

        R_DISC_TOTAL=$((( ${R_DISC[$index]} - ${R_DISC_INIT[$index]} ) + ( ${R_DISC_PRIO0[$index]} - ${R_DISC_PRIO0_INIT[$index]} )))
        T_DISC_TOTAL=$(( ${T_DISC[$index]} - ${T_DISC_INIT[$index]} ))
        R_OOB_TOTAL=$(( ${R_OOB[$index]} - ${R_OOB_INIT[$index]} ))

        printf "[%'9s Rx]: %'16d pkts  %'8d (%'8d) Kpps  | %'20d bytes %'8d (%'8d) Mbps  | %'16d pkts discarded | %'16d pkts oob\n" $name $R_PKT_TOTAL $((R_PKT_RATE / 1000)) $((R_PKT_RATE_MAX[$index] / 1000))  $R_BYTE_TOTAL $((R_BIT_RATE / 1000000)) $((R_BIT_RATE_MAX[$index] / 1000000)) $R_DISC_TOTAL $R_OOB_TOTAL
        printf "[%'9s Tx]: %'16d pkts  %'8d (%'8d) Kpps  | %'20d bytes %'8d (%'8d) Mbps  | %'16d pkts discarded\n" $name $T_PKT_TOTAL $((T_PKT_RATE / 1000)) $((T_PKT_RATE_MAX[$index] / 1000)) $T_BYTE_TOTAL $((T_BIT_RATE / 1000000)) $((T_BIT_RATE_MAX[$index] / 1000000)) $T_DISC_TOTAL


        ((index++))
    done

    printf "\n"
    sleep 1
done
```
</details>

#### Single CPU core GPU IO forwarding : `testpmd`

As the forwarder, use the modified version of testpmd contained in this repository. Use the following command line to set up IO packet forwarding in GPU memory:

```
sudo ./build/app/dpdk-testpmd -l 2-6 --main-lcore=2 -a b5:00.1,txq_inline_max=0 -- \
        --port-numa-config=0.0 --socket-num=0 --burst=64 --txd=256 --rxd=256 --mbcache=512 \
        --rxq=8 --txq=8 --forward-mode=io --nb-cores=4 -a --mbuf-size=2048g
```

Or, to set up header-data split forwarding with first 60 bytes of each message being delivered to CPU memory and the rest to GPU memory while not enabling inlining of the CPU part:

```
sudo ./build/app/dpdk-testpmd -l 2-6 --main-lcore=2 -a b5:00.1,txq_inline_max=0 -- \
        --port-numa-config=0.0 --socket-num=0 --burst=64 --txd=256 --rxd=256 --mbcache=512 \
        --rxq=8 --txq=8 --forward-mode=io --nb-cores=4 -a --mbuf-size=60,2048g \
        --rxpkts=60,2048 --rx-offloads=0x00102000 --tx-offloads=0x00008000
```

Note, `RTE_PKTMBUF_HEADROOM` value was changed from 128 bytes (default) to 0.

#### GPU processing : `l2fwd-nv`

Use the following command line to run `l2fwd-nv`:

```
sudo ./build/examples/dpdk-l2fwd-nv -l 2-10 --main-lcore=10 -a b5:00.1,txq_inline_max=0 -- \
        -m 1 -w 3 -P 0 -b 64 -B 1 -c 8
```

Please note l2fwd-nv's purpose it to demonstrate different ways of running GPU workloads on the incoming traffic; it is not meant to be a definitve benchmark of GPUDirect RDMA performance or as a guideline to optimizing resource usage. For example, it depends on using 2 CPU cores for each pair of RX/TX queues.

# Caveats, limitations

## Inlining

Many NICs implement an optimisation technique called _inlining_. In principle it allows the NIC driver (PMD in this case) to include some (or all of) packet payload in the NIC's work queues. This allows the NIC to skip DMAing packet content from memory and thus results in lower PCIe overhead and lower overall latency. It is currently not possible to efficiently perform this optimisation with packets which reside in GPU memory which is why this feature should be disabled. For Mellanox NICs, this is done by a parameter that EAL passes to the PMD, e.g. when you specify a particular device to be used by the application:

```
-w b5:00.1,txq_inline_max=0
```

Alternatively, it is possible to take advantage of mbuf dynamic flags to hint whether inlining is to be performed or not: please see the documentation of `RTE_PMD_MLX5_FINE_GRANULARITY_INLINE`.

## Issues with mapping virtual addresses

Depending on your test environment, you may observe several warnings from EAL functions like:
```
EAL: WARNING! Base virtual address hint (0xc00054000 != 0x7f6a40000000) not respected!
EAL:    This may cause issues with mapping memory into secondary processes
```

A WAR for this is the [EAL flag](https://doc.dpdk.org/guides/linux_gsg/linux_eal_parameters.html) is to set the default initial virtual address for mmap to an higher value (e.g. `--base-virtaddr 0x7f0000000000`).

## Buffer Split

Modifications required for buffer split on Rx to work have only been introduced in the mlx5 PMD.

# Notes on supporting other network adapters

While not supported, GPU DPDK has been shown to perform GPUDirect RDMA with non-Mellanox NICs (specifically, Intel's XL710-QDA2 (40Gbps) and X540-AT2 (10Gbps)). Different approaches need to be taken for different NICs, depending on how they register memory.

In case of Intel NICs, we've found that with the `igb_uio` [DPDK kernel module](https://doc.dpdk.org/guides/linux_gsg/linux_drivers.html)), DPDK PMDs "trusted" the mappings provided by the user in the IOVA argument of the memory registration APIs.
To leverage that, we modified both the userspace and the kernelspace parts of [gdrcopy](https://github.com/NVIDIA/gdrcopy) to expose the BAR1 addresses as part of its pinning/mapping operation.
These addresses can then be used as the IOVA when performing memory registration.
In addition, the GPU buffers can then be accessed (read/write) directly from CPU.

In short, the sequence is as follows:

- Allocate a buffer of unmanaged GPU memory (e.g. using `cudaMalloc`)
- Initialize gdrcopy
- Pin and map the buffer
- Create and initialize the mempools by providing the gdrcopy-mapped CPU VA as the virtual address and the "physical address" as the IOVA address

The modified kernelspace part of gdrcopy can be found under `kernel/linux/gdrdrv/`, while the userspace part is exposed in form of a library called `librte_nv`, which can be found under `lib/librte_nv`.

Note that for GPU to access the buffers from the mbuf, however, one must map the `buf_addr` (which will be in the CPU VA space) to a GPU VA.
