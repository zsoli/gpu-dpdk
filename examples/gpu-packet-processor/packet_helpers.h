#ifndef PACKET_HELPERS_H
#define PACKET_HELPERS_H

#include <netinet/in.h>

struct packet {
	struct rte_mbuf* mbuf;
	struct rte_ether_hdr *ether_hdr;
	struct rte_ipv4_hdr *ipv4_hdr;
	struct rte_udp_hdr *udp_hdr;
	struct rte_tcp_hdr *tcp_hdr;
};

struct packet_values {
	in_addr_t src_ip;
	in_addr_t dst_ip;
	in_port_t sport;
	in_port_t dport;
};

#endif /* PACKET_HELPERS_H */