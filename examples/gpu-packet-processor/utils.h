#ifndef UTILS_H
#define UTILS_H

#include "firewall.h"
#include "nv_queue.h"

#include <inttypes.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda_profiler_api.h>

/* functions */
void l2fwdnv_usage(const char *prgname);

unsigned int l2fwd_parse_nqueue(const char *q_arg);

unsigned int l2fwd_parse_queue_elems(const char *q_arg);

int l2fwd_parse_nb_streams(const char *q_arg);

int l2fwd_parse_dataroom_size(const char *q_arg);

int l2fwd_parse_burst_size(const char *q_arg);

int l2fwd_parse_cores(const char *q_arg);

int l2fwd_parse_warmup_packets(const char *q_arg);

int l2fwd_parse_performance_packets(const char *q_arg);

int l2fwd_parse_burst_use_payload(const char *q_arg);

int l2fwd_parse_workload(const char *q_arg);

int l2fwd_parse_memtype(const char *q_arg);

unsigned int l2fwd_parse_device_id(const char *q_arg);

int l2fwd_parse_mac_update(const char *q_arg);

void check_all_ports_link_status(uint32_t port_mask);

void str_to_lower(char *str[]);

void nvl2fwd_macswap_cpu(uintptr_t *i_ptr, int nmbuf, int payload, int mac_update);

int nvl2fwd_process_gpu(uintptr_t *i_ptr, int nmbuf, int payload, cudaStream_t stream, uint8_t *sync_flag, int mac_update, int cuda_blocks, int cuda_threads, struct nv_firewall *fw);

int nvl2fwd_persistent_kernel(struct nv_pk_ring_obj *pk_ring, int tot_pkring_obj, uint8_t *pk_quit_d, int use_payload, int mac_update, int cuda_blocks, int cuda_threads, cudaStream_t stream);

#endif /* UTILS_H */