/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright (c) 2018-2019, NVIDIA CORPORATION.  All rights reserved.
 */

#include "nv_queue.h"

#include <rte_mbuf.h>
#include <rte_malloc.h>

/* ======================== NV QUEUE ========================  */
struct nv_queue *__rte_experimental nv_create_nvqueue(int num_queues, int num_queue_obj, int burst_size, int use_gmem, int flags) 
{
	struct nv_queue *nvqueue;
	int index_queue = 0, index_obj = 0, persistent_kernel = 0;
	uintptr_t useless_phy;

	if (num_queues <= 0 || num_queue_obj <= 0 || burst_size <= 0) {
		fprintf(stderr, "Input params error\n");
		return NULL;
	}

	if (flags & NVQ_PERSISTENT)
		persistent_kernel = 1;

	// printf("NV queue with %d elems, %d obj each of %d mbufs, persistent kernel is %d\n", 
	//  num_queues, num_queue_obj, burst_size, persistent_kernel);

	//Optimization: prealloc these mem structs numBufs/sizePktsBurst
	nvqueue = rte_zmalloc("rte_nv::nv_queue", num_queues * sizeof(struct nv_queue), 0);
	if (nvqueue == NULL) {
		fprintf(stderr, "nv_queue malloc error\n");
		return NULL;
	}

	for (index_queue = 0; index_queue < num_queues; index_queue++) {
		nvqueue[index_queue].id = index_queue;
		nvqueue[index_queue].tot_obj = num_queue_obj;
		nvqueue[index_queue].cur_obj = 0;

		nvqueue[index_queue].nv_obj = rte_zmalloc("rte_nv::nv_obj", num_queue_obj * sizeof(struct nv_burst_obj), 0);
		if (nvqueue[index_queue].nv_obj == NULL) {
			fprintf(stderr, "nv_queue %d obj malloc  error\n", index_queue);
			return NULL;
		}
		//nv_obj: mbufs and burst
		for (index_obj = 0; index_obj < num_queue_obj; index_obj++) {
			nvqueue[index_queue].nv_obj[index_obj].id = index_obj;
			nvqueue[index_queue].nv_obj[index_obj].stream = 0;
			nvqueue[index_queue].nv_obj[index_obj].ddev_mem = 0;
			nvqueue[index_queue].nv_obj[index_obj].rxtx_burst = burst_size;
			nvqueue[index_queue].nv_obj[index_obj].alloc_burst = burst_size * 2;
			nvqueue[index_queue].nv_obj[index_obj].curr_mbuf_burst = 0;
			nvqueue[index_queue].nv_obj[index_obj].mbuf_len = 0;

			NV_CUDA_CHECK(cudaMallocHost ((void **) &(nvqueue[index_queue].nv_obj[index_obj].hdr_burst_h),
				       nvqueue[index_queue].nv_obj[index_obj].  alloc_burst * 5 * sizeof(uintptr_t)));
			nvqueue[index_queue].nv_obj[index_obj].hdr_burst_d =
			    nvqueue[index_queue].nv_obj[index_obj].hdr_burst_h + (nvqueue[index_queue].nv_obj[index_obj].  alloc_burst * 1);
			nvqueue[index_queue].nv_obj[index_obj].pl_burst_host_h =
			    nvqueue[index_queue].nv_obj[index_obj].hdr_burst_h + (nvqueue[index_queue].nv_obj[index_obj].  alloc_burst * 2);
			nvqueue[index_queue].nv_obj[index_obj].pl_burst_dev_h = 
				nvqueue[index_queue].nv_obj[index_obj].hdr_burst_h + (nvqueue[index_queue].nv_obj[index_obj].  alloc_burst * 3);
			nvqueue[index_queue].nv_obj[index_obj].pl_burst_phy =
			    nvqueue[index_queue].nv_obj[index_obj].hdr_burst_h + (nvqueue[index_queue].nv_obj[index_obj].alloc_burst * 4);
			if (use_gmem == 1) {
				nvqueue[index_queue].nv_obj[index_obj].ddev_mem = 1;
				NV_CUDA_CHECK(cudaMalloc ((void **) &(nvqueue[index_queue].  nv_obj[index_obj].  hdr_burst_dd),
					       nvqueue[index_queue].  nv_obj[index_obj].alloc_burst * 2 * sizeof(uintptr_t)));
				nvqueue[index_queue].nv_obj[index_obj].pl_burst_dev_d =
					nvqueue[index_queue].nv_obj[index_obj].hdr_burst_dd + (nvqueue[index_queue].nv_obj[index_obj].alloc_burst * 1);
			} else
				NV_CUDA_CHECK(cudaMalloc((void **) &(nvqueue[index_queue].nv_obj[index_obj].hdr_burst_dd),
					       nvqueue[index_queue].nv_obj[index_obj].alloc_burst * sizeof(uintptr_t)));

			cudaEventCreate(& (nvqueue[index_queue].nv_obj[index_obj].  sync_event));
			NV_CUDA_CHECK(cudaMallocHost ((void **) &(nvqueue[index_queue].nv_obj[index_obj].  sync_flag), sizeof(uint8_t)));
			nvqueue[index_queue].nv_obj[index_obj].sync_flag[0] = 0;

			//Check allocated memory!
			//printf("nvqueue[%d].nv_obj[%d] created (%p). Burst size=%d\n", index_queue, index_obj, &(nvqueue[index_queue].nv_obj[index_obj]), nvqueue[index_queue].nv_obj[index_obj].alloc_burst);
		}

		for (index_obj = 0; index_obj < num_queue_obj; index_obj++)
			nvqueue[index_queue].nv_obj[index_obj].busy.cnt = 0;

		//Persistent kernel section
		if (persistent_kernel) {
			NV_CUDA_CHECK(cudaStreamCreateWithFlags (&(nvqueue[index_queue].pk_stream), cudaStreamNonBlocking));
			NV_CUDA_CHECK(cudaMallocHost ((void **)&(nvqueue[index_queue].pk_ring), num_queue_obj * sizeof(struct nv_pk_ring_obj)));

			for (index_obj = 0; index_obj < num_queue_obj;
			     index_obj++) {
				//Host or device memory ?
				nvqueue[index_queue].pk_ring[index_obj].status = NV_PK_FREE;
				nvqueue[index_queue].pk_ring[index_obj].num_pkts = 0;
				nvqueue[index_queue].pk_ring[index_obj].ptr = 0;
				//TX core only
				nvqueue[index_queue].pk_ring[index_obj].nvobj_index = 0;
			}

			//Flush flag: a read from CPU thread
			nvqueue[index_queue].g = gdr_open();

			if (nvqueue[index_queue].g == NULL) {
				fprintf(stderr, "nv_init_gdrcopy failed\n");
				return NULL;
			}

			if (0 != alloc_pin_gdrcopy(&(nvqueue[index_queue].g),
						      &(nvqueue[index_queue].mh),
						      &(nvqueue[index_queue].pk_flush_d),
						      &(nvqueue[index_queue].pk_flush_h),
						      &(useless_phy),
                              &(nvqueue[index_queue].pk_flush_free),
						      &(nvqueue[index_queue].pk_flush_size),
						      sizeof(uint32_t))
			    ) {
				fprintf(stderr,
					"alloc_pin_gdrcopy flush failed\n");
				return NULL;
			}

			if (0 != alloc_pin_gdrcopy(&(nvqueue[index_queue].g),
						      &(nvqueue[index_queue].mh),
						      &(nvqueue[index_queue].pk_quit_d),
						      &(nvqueue[index_queue].pk_quit_h),
						      &(useless_phy),
                              &(nvqueue[index_queue].pk_quit_free),
						      &(nvqueue[index_queue].pk_quit_size),
						      sizeof(uint32_t))
			    ) {
				fprintf(stderr, "alloc_pin_gdrcopy quit failed\n");
				return NULL;
			}
			//GDRCopy write
			((uint32_t *) nvqueue[index_queue].pk_quit_h)[0] = 0;
			((uint32_t *) nvqueue[index_queue].pk_flush_h)[0] = NV_FLASH_VALUE;

			nvqueue[index_queue].pk_enabled = 1;
		}

		nvqueue[index_queue].tx_obj = 0;
		nvqueue[index_queue].rx_obj = 0;
	}

	return nvqueue;
}

void __rte_experimental nv_destroy_nvqueue(struct nv_queue **nvqueue, int num_queues, int num_queue_obj) 
{
	struct nv_queue *nq;
	int index_queue = 0, index_obj = 0;

	if (nvqueue == NULL || (*nvqueue) == NULL) {
		fprintf(stderr, "nv_destroy_nvqueue input error\n");
		return;
	}

	nq = (*nvqueue);
	for (index_queue = 0; index_queue < num_queues; index_queue++) {
		for (index_obj = 0; index_obj < num_queue_obj; index_obj++) {
			//mbufs headers
			if (nq[index_queue].nv_obj[index_obj].hdr_burst_h)
				cudaFreeHost(nq[index_queue].nv_obj[index_obj].hdr_burst_h);
			if (nq[index_queue].nv_obj[index_obj].hdr_burst_dd)
				cudaFree(nq[index_queue].nv_obj[index_obj].hdr_burst_dd);

			cudaEventDestroy(nq[index_queue].nv_obj[index_obj].sync_event);
			cudaFreeHost(nq[index_queue].nv_obj[index_obj].sync_flag);
		}

		rte_free(nq[index_queue].nv_obj);

		if (nq[index_queue].pk_enabled == 1) {
			cudaStreamDestroy(nq[index_queue].pk_stream);
			cudaFreeHost(nq[index_queue].pk_ring);

			//Flush flag
			cleanup_gdrcopy(nq[index_queue].g, 
                                (CUdeviceptr) (nq[index_queue].pk_flush_free), 0,	//freed by next call
                                (void *)(nq[index_queue].pk_flush_h),
                                nq[index_queue].pk_flush_size);

			//Quit flag
			cleanup_gdrcopy(nq[index_queue].g,
                                (CUdeviceptr) (nq[index_queue].pk_quit_free),
                                nq[index_queue].mh,
                                (void *)(nq[index_queue].pk_quit_h),
                                nq[index_queue].pk_quit_size);

			gdr_close(nq[index_queue].g);
		}
	}

	return;
}

int __rte_experimental nv_prepare_burst(struct nv_burst_obj **nvobj_in, int nb, cudaStream_t stream, int flags) 
{
	int j = 0, mbuf_payload_size = 0;
	struct rte_mbuf *m = NULL;
	uintptr_t initial_payload = 0;
	struct nv_burst_obj *nvobj = *(nvobj_in);

	int hdrptr_devmem = (flags & NV_PKTMBUF_HDRPTR_DEVMEM) ? 1 : 0;
	int plptr_devmem = (flags & NV_PKTMBUF_PLPTR_DEVMEM) ? 1 : 0;
	int sync_after = (flags & NV_PKTMBUF_SYNC) ? 1 : 0;
	int pldata_devmem = (flags & NV_PKTMBUF_PLDATA_DEVMEM) ? 1 : 0;
	
	/*
	printf("hdrptr_devmem: %i\n", hdrptr_devmem);
	printf("plptr_devmem: %i\n", hdrptr_devmem);
	printf("sync_after: %i\n", hdrptr_devmem);
	printf("pldata_devmem: %i\n", hdrptr_devmem);
	*/

	if (nvobj == NULL) {
		fprintf(stderr, "ERROR input\n");
		return 1;
	}

	nvobj->curr_mbuf_burst = nb;	//warning if curr > max
	nvobj->curr_burst_bytes = 0;

	memset(nvobj->hdr_burst_d, 0, nvobj->alloc_burst * sizeof(uintptr_t));
	memset(nvobj->pl_burst_host_h, 0, nvobj->alloc_burst * sizeof(uintptr_t));
	memset(nvobj->pl_burst_dev_h, 0, nvobj->alloc_burst * sizeof(uintptr_t));

	//Storing in host pinned memory, mbufs hdr and payload pointers
	for (j = 0; j < nvobj->curr_mbuf_burst && j < nvobj->rxtx_burst; j++) {
		m = (struct rte_mbuf *)nvobj->hdr_burst_h[j];

		nvobj->curr_burst_bytes += m->pkt_len;
		nvobj->hdr_burst_d[j] = (uintptr_t) m;

// #ifdef DEBUG_PRINT
// 		nv_dump_pktinfo(nv_mbuf_get_pktinfo(m));
// 		fprintf(stderr,
// 			"nv_prepare_burst: m=%p, nvobj->hdr_burst_h[%d]=%ld, nvobj->hdr_burst_d[%d]=%ld, m->pkt_len=%d, m->data_len=%d, m->data_off=%d m->buf_len=%d\n",
// 			m, j, nvobj->hdr_burst_h[j], j, nvobj->hdr_burst_d[j],
// 			m->pkt_len, m->data_len, m->data_off, m->buf_len);
// #endif

		if (j == 0)
			nvobj->mbuf_len = m->data_len;

		nvobj->pl_burst_phy[j] = (uintptr_t) m->buf_iova;
		nvobj->pl_burst_host_h[j] = (uintptr_t) m->buf_addr;
		nvobj->pl_burst_dev_h[j] = (uintptr_t) m->buf_addr;
		if (j == 0 && pldata_devmem) {
			mbuf_payload_size = (int)m->buf_len;
			initial_payload = (uintptr_t) nvobj->pl_burst_host_h[j];
		}
	}

	//Set stream in nvobj
	nvobj->stream = stream;

	//Copy headers pointers in device memory
	if (hdrptr_devmem) {
		NV_CUDA_CHECK(cudaMemcpyAsync(nvobj->hdr_burst_dd,
					      nvobj->hdr_burst_d,
					      nvobj->curr_mbuf_burst *
					      sizeof(uintptr_t),
					      cudaMemcpyDefault, stream));

		if (sync_after)
			NV_CUDA_CHECK(cudaStreamSynchronize(stream));
	}
	//Copy payload pointers in device memory
	if (plptr_devmem) {
		NV_CUDA_CHECK(cudaMemcpyAsync(nvobj->pl_burst_dev_d,
					      nvobj->pl_burst_dev_h,
					      nvobj->curr_mbuf_burst *
					      sizeof(uintptr_t),
					      cudaMemcpyDefault, stream));

		if (sync_after)
			NV_CUDA_CHECK(cudaStreamSynchronize(stream));
	}

	/*
	   Copy mbufs payloads in device memory, assuming:
	   - host pinned memory mempool
	   - mbufs are consecutive
	 */
	if (pldata_devmem) {
		printf("mbuf_payload_size= %d, initial_payload=%ld\n", mbuf_payload_size, initial_payload);
		NV_CUDA_CHECK(cudaMemcpyAsync
			      (nvobj->hdr_burst_dd, nvobj->hdr_burst_d,
			       nvobj->curr_mbuf_burst *
			       sizeof(struct rte_mbuf *), cudaMemcpyDefault,
			       stream)
		    );

		if (sync_after)
			NV_CUDA_CHECK(cudaStreamSynchronize(stream));
	}

	return 0;

}