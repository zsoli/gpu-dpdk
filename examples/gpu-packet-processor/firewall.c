#include "firewall.h"
#include "array.h"
#include "nv_queue.h"
#include "rte_common.h"
#include "rte_malloc.h"

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* starting size of the list of chains in bytes */
#define NV_FIREWALL_CHAIN_SIZE 2

/* starting size of the list of entries in chains in bytes */
#define NV_FIREWALL_ENTRY_SIZE 2

struct nv_firewall_chain* nv_firewall_add_chain(struct nv_array *chain_array, const char *chain_name)
{
    struct nv_firewall_chain *chain;

    if((chain = (typeof(chain)) nv_array_add(chain_array, sizeof *chain)) == NULL)
        return NULL;

    NV_CUDA_CHECK(cudaMallocManaged((void**) &chain->name, strlen(chain_name) + 1, cudaMemAttachGlobal));
    if (chain->name == NULL) {
        // TODO remove chain from array
        return NULL;
    }
    strcpy(chain->name, chain_name);

    nv_array_init(&chain->entries, NV_FIREWALL_ENTRY_SIZE);

    FW_LOG(INFO, "added new chain '%s' to firewall \n", chain->name);
    return chain;
}

int nv_firewall_add_rule(struct nv_array *chain_array, const char *chain_name, struct nv_firewall_rule rule) 
{
    struct nv_firewall_chain *chain;
    struct nv_firewall_rule *nr;

    if (chain_array == NULL)
        return -errno;

    if ((chain = nv_firewall_get_chain(chain_array, chain_name)) == NULL)
        return -errno;

    if ((nr = (typeof(nr)) nv_array_add(chain->entries, sizeof rule)) == NULL)
        return -errno;
    
    nr->operation = rule.operation;
    nr->proto = rule.proto;
    nr->src_port = rule.src_port;
    nr->dst_port = rule.dst_port;
    nr->src_ip.v4 = rule.src_ip.v4;
    nr->dst_ip.v4 = rule.dst_ip.v4;

    if (rule.jump_chain != NULL) {
        NV_CUDA_CHECK(cudaMallocManaged((void**) &nr->jump_chain, strlen(rule.jump_chain), cudaMemAttachGlobal));
        strcpy(nr->jump_chain, rule.jump_chain);
    }

    strcpy(nr->recent_list_name, rule.recent_list_name);
    
    FW_LOG(NOTICE, 
        "added rule: \n"
        "\t operation: %s \n"
        "\t proto: %s \n"
        "\t src_port: %u \n"
        "\t dst_port: %u \n"
        "\t src_ip: %u \n"
        "\t dst_ip: %u \n"
        "\t jump_chain: %s \n"
        "\t recent_list_name: %s \n",
        pktop_to_str(nr->operation),
        proto_to_str(nr->proto),
        nr->src_port,
        nr->dst_port,
        nr->src_ip.v4,
        nr->dst_ip.v4,
        nr->jump_chain,
        nr->recent_list_name
    );

    return 0;
}

int nv_firewall_init(struct nv_firewall **fw) 
{
    NV_CUDA_CHECK(cudaMallocManaged((void**) fw, sizeof **fw, cudaMemAttachGlobal));
    if (*fw == NULL)
        return -errno;

    nv_array_init(&(*fw)->chains, NV_FIREWALL_CHAIN_SIZE);
    if ((*fw)->chains == NULL)
        return -errno;

    /* init default chain */
    if (nv_firewall_add_chain((*fw)->chains, DEFAULT_CHAIN_NAME) == NULL)
        return -errno;

    return 0;
}

int nv_firewall_destroy(struct nv_firewall *fw) 
{
    if (fw== NULL)
        return -errno;

    return 0;
}