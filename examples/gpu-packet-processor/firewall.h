#ifndef FIREWALL_H
#define FIREWALL_H

#include "packet_helpers.h"
#include "module_recent.h"

#include <netinet/in.h>
#include <rte_log.h>
#include <cuda.h>
#include <cuda_runtime.h>

/* logging */
#define RTE_LOGTYPE_GPU_FW RTE_LOGTYPE_USER2

#define FW_LOG(level, fmt, args...) \
    rte_log(RTE_LOG_ ## level, RTE_LOGTYPE_GPU_FW, "GPU_FW: " fmt, ## args)

#define FIREWALL_MAX_RULES  128
#define FIREWALL_MAX_CHAINS 128
#define DEFAULT_CHAIN_NAME  "input"

enum pktop {
    PKTOP_IGNORE        = 0,
    PKTOP_DROP          = 1 << 1,
    PKTOP_JUMP          = 1 << 2,
    PKTOP_RECENT_SET    = 1 << 3, 
    PKTOP_RECENT_REMOVE = 1 << 4,
    PKTOP_RECENT_RCHECK = 1 << 5, 
    PKTOP_ACCEPT        = 1 << 6, 
    PKTOP_END_MARKER    = 1 << 7
};

enum rule_type {
    RULE_TYPE_INBOUND,
    RULE_TYPE_OUTBOUND
};

enum {
#define IPPROTO_ANY IPPROTO_ANY
    IPPROTO_ANY = -1
};

struct nv_firewall_rule {
    int16_t proto;
    enum pktop operation;
    char *jump_chain;

    union {
        in_addr_t v4;
        uint8_t v4_bytes[4];
    } src_ip;

    union {
        in_addr_t v4;
        uint8_t v4_bytes[4];
    } dst_ip;

    in_port_t src_port;
    in_port_t dst_port;

    struct mod_recent_data *recent_data;
    char recent_list_name[32];
};

struct nv_firewall_chain {
    char *name;
    struct nv_array *entries;
};

struct nv_firewall {
    struct nv_array *chains;
    struct nv_array *module_recent_lists;
};

/* functions */
struct nv_firewall_chain* nv_firewall_add_chain(struct nv_array *chain_array, const char *chain_name);

__host__ __device__ struct nv_firewall_chain* nv_firewall_get_chain(struct nv_array *chains, const char *chain_name);

int nv_firewall_add_rule(struct nv_array *chain_array, const char *chain_name, struct nv_firewall_rule rule);

int nv_firewall_init(struct nv_firewall **fw);

void nv_firewall_debug_print(struct nv_firewall *fw);

int nv_firewall_destroy(struct nv_firewall *chain_array);

__device__ void nv_firewall_dev_mac_swap(struct packet *p);

__device__ int nv_firewall_dev_proc_packet(uintptr_t *mbuf_ptr, struct packet *p, struct nv_firewall *nv_fw, int idx);

static inline const char* pktop_to_str(enum pktop op) 
{
    switch (op) {
    case PKTOP_IGNORE:
        return "PKTOP_IGNORE";
    case PKTOP_DROP:
        return "PKTOP_DROP";
    case PKTOP_JUMP:
        return "PKTOP_JUMP";
    case PKTOP_RECENT_SET:
        return "PKTOP_RECENT_SET";
    case PKTOP_RECENT_REMOVE:
        return "PKTOP_RECENT_REMOVE";
    case PKTOP_RECENT_RCHECK:
        return "PKTOP_RECENT_RCHECK";
    default:
        if (op >= PKTOP_END_MARKER) {
            return "composite";
        } else {
            FW_LOG(ERR, "WARNING: unknown pktop: %i\n", op);
            return "unkown";
        }
    }
}

static inline const char* proto_to_str(int64_t proto)
{
    switch (proto) {
    case -1:
        return "IPPROTO_ANY";
    case IPPROTO_TCP:
        return "IPPROTO_TCP";
    case IPPROTO_UDP:
        return "IPPROTO_UDP";
    default:
        FW_LOG(ERR, "WARNING: unknown protocol: %li\n", proto);
        return "unknown";
    }
}

#endif /* FIREWALL_H */