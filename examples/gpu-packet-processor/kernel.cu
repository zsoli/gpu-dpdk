/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright (c) 2018-2019, NVIDIA CORPORATION.  All rights reserved.
 */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "kernel.h"
#include "common.h"
#include "kernel_utils.h"
#include "module_recent.h"

#include <rte_mbuf.h>
#include <rte_ether.h>

#ifdef __cplusplus
}
#endif /* __cplusplus */

static __global__ void gpu_main_kernel(uintptr_t *mbufs_ptr, int num_pkts, uint8_t * sync_flag, struct nv_firewall *nv_fw)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (mbufs_ptr == NULL) {
		KLOG(ERR, "mbufs_ptr is NULL in gpu_main_kernel\n");
		goto ERR;
	}

	if (idx < num_pkts) {
		uintptr_t *mbuf = (uintptr_t *) mbufs_ptr[idx];
		struct packet p = {NULL};

		int ret = nv_firewall_dev_proc_packet(mbuf, &p, nv_fw, idx);
		if (ret == 0) {
			nv_firewall_dev_mac_swap(&p);
		} else if (ret == 1) {
			KLOG(INFO, "dropped packet \n");
		} else {
			KLOG(ERR, "error occured during packet processing: %i \n", ret);
		}
	}

	if (idx == 0)
		module_recent_print_data(nv_fw->module_recent_lists);

ERR:
	if (sync_flag != NULL) {
		__threadfence_system();
		__syncthreads();
		// For the sake of simplicity, we assume only 1 CUDA block
		if (idx == 0) {
			sync_flag[0] = 1;
			__threadfence_system();
		}
	}
}

void nvl2fwd_launch_gpu_processing(uintptr_t *i_ptr, int nb_rx, int payload, uint8_t * sync_flag, int mac_update, int cuda_blocks, int cuda_threads, cudaStream_t stream, struct nv_firewall *nv_fw) 
{
	//For the sake of simplicity, we assume only 1 CUDA block
	assert(cuda_blocks == 1);
	assert(cuda_threads > 0);

	KLOG(DEBUG, "launching kernel...\n");

	NV_CUDA_CHECK(cudaGetLastError());
	gpu_main_kernel <<<cuda_blocks, cuda_threads, 0, stream>>> (i_ptr, nb_rx, sync_flag, nv_fw);
	NV_CUDA_CHECK(cudaGetLastError());
}

/* ================ PERSISTENT KERNEL ================ */
#define PK_CHECK_QUIT(x) (NV_ACCESS_ONCE(x) == 1)

//Assumin only 1 CUDA block
static __global__ void pk_macupdate(struct nv_pk_ring_obj *pk_ring, int tot_pkring_obj, uint8_t * pk_quit_d, int use_payload, int mac_update) 
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x, pk_ring_index = 0;
	struct rte_ether_hdr *eth;
	uint16_t *src_addr, *dst_addr, temp;
	uint8_t *payload_ptr;
	uintptr_t *tmp_ptr;
	__shared__ int quit;

	if (idx == 0)
		quit = 0;
	
	__syncthreads();

	while (1) {
		//Th0 waits for RX core to prepare next data
		if (idx == 0) {
			//Check exit condition
			if (PK_CHECK_QUIT(*pk_quit_d))
				quit = 1;
			else {
				while (NV_ACCESS_ONCE(pk_ring[pk_ring_index].status) != NV_PK_READY) {
					if (PK_CHECK_QUIT(*pk_quit_d)) {
						quit = 1;
						break;
					}
				}
			}
		}
		//non-zero threads wait for data being ready
		__syncthreads();

		//Th0 writes, all threads read
		if (quit == 1)
			break;

		if (idx < pk_ring[pk_ring_index].num_pkts) {
			tmp_ptr = (uintptr_t *) (pk_ring[pk_ring_index].ptr);

			if (use_payload == 1)
				eth = (struct rte_ether_hdr *)(((uint8_t *) (tmp_ptr[idx])) + RTE_PKTMBUF_HEADROOM);
			else {
				payload_ptr = (uint8_t *)((struct rte_mbuf *)tmp_ptr[idx])->buf_addr;
				eth = (struct rte_ether_hdr *)(((uint8_t *) payload_ptr) + RTE_PKTMBUF_HEADROOM);
			}

			src_addr = (uint16_t *) (&eth->s_addr);
			dst_addr = (uint16_t *) (&eth->d_addr);

			/* Code to verify source and dest of ethernet addresses */
			uint8_t *src = (uint8_t *) (&eth->s_addr);
			uint8_t *dst = (uint8_t *) (&eth->d_addr);
			KLOG(DEBUG, "1 Source: %02x:%02x:%02x:%02x:%02x:%02x Dest: %02x:%02x:%02x:%02x:%02x:%02x\n", src[0], src[1], src[2], src[3], src[4], src[5], dst[0], dst[1], dst[2], dst[3], dst[4], dst[5]);
			if (mac_update) {
				temp = dst_addr[0];
				dst_addr[0] = src_addr[0];
				src_addr[0] = temp;
				temp = dst_addr[1];
				dst_addr[1] = src_addr[1];
				src_addr[1] = temp;
				temp = dst_addr[2];
				dst_addr[2] = src_addr[2];
				src_addr[2] = temp;
			}
		}
		//Ensure all threads completed the swap
		__threadfence_system();
		__syncthreads();
		//Th0 signal to TX core
		if (idx == 0) {
			//System fence to ensure writes
			NV_ACCESS_ONCE(pk_ring[pk_ring_index].status) = NV_PK_DONE;
			__threadfence_system();
		}

		pk_ring_index = (pk_ring_index + 1) % tot_pkring_obj;
	}
}

void nvl2fwd_launch_pk(struct nv_pk_ring_obj *pk_ring, int tot_pkring_obj, uint8_t * pk_quit_d, int use_payload, int mac_update, int cuda_blocks, int cuda_threads, cudaStream_t stream) 
{
	assert(cuda_blocks == 1);
	assert(cuda_threads > 0);

	printf("nvl2fwd_launch_pk()\n");

	NV_CUDA_CHECK(cudaGetLastError());
	pk_macupdate <<<cuda_blocks, cuda_threads, 0, stream>>> (pk_ring, tot_pkring_obj, pk_quit_d, use_payload, mac_update);
	NV_CUDA_CHECK(cudaGetLastError());
}