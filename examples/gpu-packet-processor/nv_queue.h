/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright (c) 2018-2019, NVIDIA CORPORATION.  All rights reserved.
 */

#ifndef NV_QUEUE_H_
#define NV_QUEUE_H_

#include "common.h"

#include <gdrapi.h>
#include <rte_atomic.h>
#include <stdio.h>

#define NV_FLASH_VALUE 3

/*
 * ENUMS
 */
enum nv_pktmbuf_burst_opts {
	NV_PKTMBUF_HOST = 0,
	NV_PKTMBUF_HDRPTR_DEVMEM = 1,
	NV_PKTMBUF_PLPTR_DEVMEM = 1 << 1,
	NV_PKTMBUF_PLDATA_DEVMEM = 1 << 2,
	NV_PKTMBUF_SYNC = 1 << 3,
};

enum nv_queue_flags {
	NVQ_DEFAULT = 0,
	NVQ_PERSISTENT = 1 << 1
};

enum nv_pk_status {
	NV_PK_FREE = 0,
	NV_PK_READY = 1 << 1,
	NV_PK_DONE = 1 << 2
};

/*
 * STRUCTS
 */

//persistent kernel sync flag
struct nv_pk_ring_obj {
	uint32_t status;
	uintptr_t ptr;
	uint32_t num_pkts;
	int nvobj_index;
};

struct nv_burst_obj {
	int id;
	cudaStream_t stream;

	//mbufs headers
	int mbuf_len;		//Should be an array with all mbuf size
	uintptr_t *hdr_burst_h; // allocated on host but accessible by device
	uintptr_t *hdr_burst_d; // hdr_burst_h shifted (+1)
	uintptr_t *hdr_burst_dd;// allocated on deivice

	//burst
	int rxtx_burst;
	int alloc_burst;
	int curr_mbuf_burst;
	uint64_t curr_burst_bytes;
	int ddev_mem;

	//Payload buffers
	uintptr_t *pl_burst_host_h; // hdr_burst_h shifted (+2)
	uintptr_t *pl_burst_dev_h;	// hdr_burst_h shifted (+3)  
	uintptr_t *pl_burst_dev_d;	// hdr_burst_dd shifted
	uintptr_t *pl_burst_phy;	// hdr_burst_h shifted (+4)

	cudaEvent_t sync_event;
	uint8_t *sync_flag;

	//Persistent kernel -- instead of using ring
	// 0 -> 1
	//rte_atomic16_cmpset((volatile uint16_t *)&busy->cnt, 0, 1);
	// CPU r/w
	rte_atomic16_t busy;
};

struct nv_queue {
	int id;
	int tot_obj;	                // number of mbuffers
	int cur_obj;
	struct nv_mempool_info *nv_memp;
	struct nv_burst_obj *nv_obj;	// mbuffers

	//Persistent kernel
	int pk_enabled;
	cudaStream_t pk_stream;
	int rx_obj;
	int tx_obj;
	//GDRCopy for pinning flags
	gdr_t g;
	gdr_mh_t mh;
	//Flush -- CPU read into device memory
	uintptr_t pk_flush_d;	//must be device memory
	uintptr_t pk_flush_h;	//must be host memory
	uintptr_t pk_flush_free;//must be used to free devmem
	size_t pk_flush_size;
	//Kill kernel -- CPU write, GPU read
	uintptr_t pk_quit_d;
	uintptr_t pk_quit_h;
	uintptr_t pk_quit_free;
	size_t pk_quit_size;
	struct nv_pk_ring_obj *pk_ring;
};

/*
 * FUNCS
 */

struct nv_queue *__rte_experimental nv_create_nvqueue(int num_queues, int num_queue_obj, int burst_size, int use_gmem, int flags);

int __rte_experimental nv_prepare_burst(struct nv_burst_obj **nvobj, int nb, cudaStream_t stream, int flags);

void __rte_experimental nv_destroy_nvqueue(struct nv_queue **nvqueue, int num_queues, int num_queue_obj);

/* ======================== SHINFO MBUF MANAGEMENT FUNCTIONS ========================  */
#if 0
__host__ __device__ struct rte_nv_pkt_info *(struct rte_mbuf *m) 
{
	if (NULL == m || NULL == m->shinfo || NULL == m->shinfo->fcb_opaque)
		return NULL;

	return (struct rte_nv_pkt_info *)m->shinfo->fcb_opaque;
}

__host__ __device__ struct rte_mbuf *nv_mbuf_get_devptr_hdr(struct rte_mbuf *m) 
{
	struct rte_nv_pkt_info *pkt_info;

	if (NULL == m)
		return NULL;

	pkt_info = nv_mbuf_get_pktinfo(m);

	return (struct rte_mbuf *)pkt_info->hdr_dptr;
}

__host__ __device__ uint8_t *nv_mbuf_get_devptr_pl(struct rte_mbuf *m) 
{
	struct rte_nv_pkt_info *pkt_info;

	if (NULL == m)
		return NULL;

	pkt_info = nv_mbuf_get_pktinfo(m);

	return (uint8_t *) pkt_info->buf_addr_d;
}
#endif

/* ======================== SHINFO MBUF MANAGEMENT FUNCTIONS ========================  */
//#define nv_mbuf_get_pktinfo(mbuf) ((struct rte_nv_pkt_info *)((struct rte_mbuf *)mbuf)->shinfo->fcb_opaque)
//#define nv_mbuf_get_devptr_hdr(mbuf) ({ (struct rte_mbuf *)((struct rte_nv_pkt_info *)nv_mbuf_get_pktinfo(mbuf))->hdr_dptr; })
//#define nv_mbuf_get_devptr_pl(mbuf) ({ (uint8_t*)((struct rte_nv_pkt_info *)nv_mbuf_get_pktinfo(mbuf))->buf_addr_d; })

/* ======================== BURST MANAGEMENT FUNCTIONS ========================  */

#define nvobj_get_burst_hdr_hptr(nvobj) (uintptr_t *) ((struct nv_burst_obj *)nvobj)->hdr_burst_h
#define nvobj_get_burst_hdr_dptr(nvobj) (uintptr_t *) ((struct nv_burst_obj *)nvobj)->hdr_burst_d
#define nvobj_get_burst_hdr_dptrdev(nvobj) (uintptr_t *) ((struct nv_burst_obj *)nvobj)->hdr_burst_dd
#define nvobj_get_burst_pl_hptr(nvobj) (uintptr_t *) ((struct nv_burst_obj *)nvobj)->pl_burst_host_h
#define nvobj_get_burst_pl_dptr(nvobj) (uintptr_t *) ((struct nv_burst_obj *)nvobj)->pl_burst_dev_h
#define nvobj_get_burst_pl_dptrdev(nvobj) (uintptr_t *) ((struct nv_burst_obj *)nvobj)->pl_burst_dev_d

static inline int alloc_pin_gdrcopy(gdr_t* pgdr, gdr_mh_t* pgdr_handle, uintptr_t* pdev_addr, uintptr_t* phost_ptr, uintptr_t* pphy_addr, uintptr_t* free_address, size_t* palloc_size, size_t input_size) 
{
    gdr_t g = *pgdr;
    gdr_mh_t mh = 0;
    gdr_info_t info;
    CUdeviceptr  dev_addr = 0;
    void *host_ptr  = NULL;
    const unsigned int FLAG = 1;
    size_t pin_size, alloc_size, rounded_size;
    
    if((NULL == g) || (NULL == pdev_addr) || (NULL == phost_ptr) || (NULL == palloc_size) || (0 == input_size)) {
        fprintf(stderr, "alloc_pin_gdrcopy: erroneous input parameters, g=%p, pdev_addr=%p, phost_ptr=%p, palloc_size=%p, input_size=%zd\n",
                g, pdev_addr, phost_ptr, palloc_size, input_size);
        return 1;
    }

    /*----------------------------------------------------------------*
    * Setting sizes                                                   */
    if(input_size < NV_MIN_PIN_SIZE)
        input_size = NV_MIN_PIN_SIZE;

    rounded_size = (input_size + GPU_PAGE_SIZE - 1) & GPU_PAGE_MASK;
    pin_size = rounded_size;
    alloc_size = rounded_size + input_size;

    /*----------------------------------------------------------------*
     * Allocate device memory.                                        */
    #ifdef NV_TEGRA
        void* cudaHost_A;
        CUresult e = cuMemHostAlloc(&cudaHost_A, alloc_size, 0);
        if(CUDA_SUCCESS != e) {
            fprintf(stderr, "cuMemHostAlloc\n");
            return 1;
        }
        e = cuMemHostGetDevicePointer(&dev_addr, cudaHost_A, 0);
        if(CUDA_SUCCESS != e) {
            fprintf(stderr, "cuMemHostGetDevicePointer\n");
            return 1;
        }
    #else
        CUresult e = cuMemAlloc(&dev_addr, alloc_size);
        if(CUDA_SUCCESS != e) {
            fprintf(stderr, "cuMemAlloc\n");
            return 1;
        }
    #endif

    *free_address = (uintptr_t)dev_addr;
    //GDRDRV needs a 64kB aligned address. No more guaranteed with recent cuMemAlloc/cudaMalloc
    if(dev_addr % GPU_PAGE_SIZE) {
        // fprintf(stderr, "cuMalloc unaligned address dev_addr=%llx, offset=%llu alloc_size=%zd, GPU_PAGE_SIZE=%ld, to be added %llu\n", 
        //     dev_addr, dev_addr % GPU_PAGE_SIZE, alloc_size, 
        //     GPU_PAGE_SIZE, (GPU_PAGE_SIZE - (dev_addr % GPU_PAGE_SIZE))
        // );

        pin_size = input_size;
        dev_addr += (GPU_PAGE_SIZE - (dev_addr % GPU_PAGE_SIZE));
    }
    //fprintf(stderr, "New aligned address dev_addr=%llx, addr offset=%llu, pin_size=%zd\n", dev_addr, dev_addr % GPU_PAGE_SIZE, pin_size);

    /*----------------------------------------------------------------*
     * Set attributes for the allocated device memory.                */
    if(CUDA_SUCCESS != cuPointerSetAttribute(&FLAG, CU_POINTER_ATTRIBUTE_SYNC_MEMOPS, dev_addr)) {
        fprintf(stderr, "cuPointerSetAttribute\n");
        cuMemFree(dev_addr);
        gdr_close(g);
        return 1;
    }
    /*----------------------------------------------------------------*
     * Pin the device buffer                                          */
    if(0 != gdr_pin_buffer(g, dev_addr, pin_size, 0, 0, &mh)) {
        fprintf(stderr, "gdr_pin_buffer\n");
        cuMemFree(dev_addr);
        gdr_close(g);
        return 1;
    }
    /*----------------------------------------------------------------*
     * Map the buffer to user space                                   */
    if(0!= gdr_map(g, mh, &host_ptr, pin_size)) {
        fprintf(stderr, "gdr_map\n");
        gdr_unpin_buffer(g, mh);
        cuMemFree(dev_addr);
        gdr_close(g);
        return 1;
    }
    /*----------------------------------------------------------------*
     * Retrieve info about the mapping                                */
    if(0 != gdr_get_info(g, mh, &info)) {
        fprintf(stderr, "gdr_get_info\n");
        gdr_unmap(/*g, mh, */host_ptr, pin_size);
        gdr_unpin_buffer(g, mh);
        cuMemFree(dev_addr);
        gdr_close(g);
        return 1;        
    }
    /*----------------------------------------------------------------*
     * Success - set up return values                                 */

    pgdr[0]           = g;
    pdev_addr[0]      = (uintptr_t)dev_addr;
    pphy_addr[0]      = (uintptr_t)info.pa;
    pgdr_handle[0]    = mh;
    phost_ptr[0]      = (uintptr_t)host_ptr;
    //*pmmap_offset = dev_addr - info.va;
    palloc_size[0]    = pin_size;

    return 0;
}

static inline void cleanup_gdrcopy(gdr_t g, CUdeviceptr free_dev_addr, gdr_mh_t gdr_handle, void* host_ptr, size_t alloc_size) 
{
    if(NULL != host_ptr) {
        gdr_unmap(/*g, gdr_handle,*/ host_ptr, alloc_size);
    }

    if(0 != gdr_handle) {
        gdr_unpin_buffer(g, gdr_handle);
    }

    if(free_dev_addr) {
        cuMemFree(free_dev_addr);
    }
}

#endif /* NV_QUEUE_H_ */
