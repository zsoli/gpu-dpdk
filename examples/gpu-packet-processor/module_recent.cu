#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "common.h"
#include "kernel_utils.h"
#include "module_recent.h"
#include "array.h"
#include <stdio.h>

#ifdef __cplusplus
}
#endif /* __cplusplus */

#define MUTEX_LOCK(m)   while(atomicCAS((int *) &(m), 0, 1) != 0)
#define MUTEX_UNLOCK(m) atomicExch((int *) &(m), 0)
__device__ volatile int mutex = 0;

static __device__ struct module_recent_list* module_recent_dev_get_list(const char *name, struct nv_array *list_array)
{
    struct module_recent_list *list;

    nv_array_for_each(list, list_array) {
        if (dstrcmp(list->name, name) == 0) {
            return list;
        }
    }

    return NULL;
}

static __device__ int module_recent_dev_del_entry(in_addr_t ipv4, struct module_recent_list *list)
{
    in_addr_t *entry;

    nv_array_for_each(entry, list->entries) {
        if (*entry == ipv4) {
            KLOG(DEBUG, "module_recent remove: deleted '%u.%u.%u.%u' from '%s' \n", 
                *(0 + ((uint8_t *) entry)),
                *(1 + ((uint8_t *) entry)),
                *(2 + ((uint8_t *) entry)),
                *(3 + ((uint8_t *) entry)),
                list->name);

            *entry = 0;
            break;
        }
    }

    return 0;
}

static __device__ bool module_recent_dev_get_entry(in_addr_t ipv4, struct module_recent_list *list)
{
    bool found = false;
    in_addr_t *entry;

    assert(list != NULL);

    nv_array_for_each(entry, list->entries) {
        if (*entry == ipv4) {
            found = true;
        }
    }

    uint8_t *ip = (uint8_t *) &ipv4;
    if (found) {
        KLOG(DEBUG, "module_recent rcheck: found '%u.%u.%u.%u' in list '%s' \n", ip[0], ip[1], ip[2], ip[3], list->name);
    } else {
        KLOG(DEBUG, "module_recent rcheck: '%u.%u.%u.%u' is not in list '%s' \n", ip[0], ip[1], ip[2], ip[3], list->name);
    }

    return found;
}

__device__ int module_recent_dev_set(in_addr_t ipv4, const char *list_name, struct nv_array *list_array)
{
    in_addr_t *entry = NULL;
    struct module_recent_list *list;
    
    list = module_recent_dev_get_list(list_name, list_array);
    if (list == NULL) {
        KLOG(ERR, "no list named '%s' exists \n", list_name);
        goto err;
    }

    if (module_recent_dev_get_entry(ipv4, list)) {
        KLOG(ERR, "entry '%u' is already in the list \n", ipv4);
        goto skip;
    }

    nv_array_for_each(entry, list->entries) {
        if (*entry == 0) {
            *entry = ipv4;
            KLOG(DEBUG, "using available slot in '%s' for new entry '%u' \n", list_name, ipv4);
            goto skip;
        }
    }

    // add new entry
    entry = (in_addr_t *) nv_array_add(list->entries, sizeof *entry);
    if (entry == NULL) {
        KLOG(ERR, "failed to add new entry to list '%s' \n", list_name);
        goto err;
    }

    *entry = ipv4;
    KLOG(DEBUG, "added new entry: %u to list '%s' \n", *entry, list_name);
    
skip:
    return 0;

err:
    return -1;
}

__device__ int module_recent_dev_remove(in_addr_t ipv4, const char *list_name, struct nv_array *list_array)
{
    struct module_recent_list *list = module_recent_dev_get_list(list_name, list_array);
    if (list == NULL) {
        KLOG(ERR, "no list '%s' exists \n", list_name);
        goto err;
    }

    if (module_recent_dev_del_entry(ipv4, list) != 0) {
        KLOG(ERR, "error happened during entry deletion \n");
        goto err;
    }
    
    return 0;

err:
    return -1;
}

__device__ int module_recent_dev_rcheck(in_addr_t ipv4, const char *list_name, bool *found, struct nv_array *lists)
{
    struct module_recent_list *list = module_recent_dev_get_list(list_name, lists);
    if (list == NULL) {
        KLOG(ERR, "no list '%s' exists \n", list_name);
        goto err;
    }

    *found = module_recent_dev_get_entry(ipv4, list);
    KLOG(DEBUG, "rcheck result for '%u' in list '%s': %s \n", ipv4, list_name, *found ? "true" : "false");
    
    return 0;

err:
    return -1;
}

void __host__ __device__ module_recent_print_data(struct nv_array *list_array)
{
    struct module_recent_list *list;
    in_addr_t *entry;
    uint8_t *ip;

    KLOG(INFO, "MODULE RECENT STRUCTURE: (size:%lu / allocated:%lu) \n", 
        list_array->size, list_array->alloc);

    nv_array_for_each(list, list_array) { 
        KLOG(INFO, "module recent list name: '%s' (size:%lu / allocated:%lu, dev_alloc: %s) \n",
            list->name,
            list->entries->size,
            list->entries->alloc,
            list->entries->dev_alloc ? "TRUE" : "FALSE");

#ifndef __CUDA_ARCH__
        if (list->entries->dev_alloc == true)
            continue;
#endif

        KLOG(INFO, "entries: \n");
        nv_array_for_each(entry, list->entries) {
            if (*entry != 0) {
                ip = (uint8_t *) entry;
                KLOG(INFO, "\t %u.%u.%u.%u \n", ip[0], ip[1], ip[2], ip[3]);
            }
        }

        KLOG(INFO, "\n");
    }
}