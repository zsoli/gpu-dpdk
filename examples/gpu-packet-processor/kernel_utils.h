#ifndef KERNEL_UTILS_H
#define KERNEL_UTILS_H

#include <netinet/in.h>

/* logging */
#define _KLOG_PRINT(level, fmt, args...)                     \
    do {                                                     \
        printf("[kernel::"#level"] " fmt, ##args);           \
    } while(0)

#define KLOG_ERR(...)
#define KLOG_INFO(...)
#define KLOG_DEBUG(...)

#ifdef KERNEL_LOG_LEVEL_DEBUG
    #ifdef KLOG_DEBUG
        #undef KLOG_DEBUG
    #endif

    #define KLOG_DEBUG(...)  _KLOG_PRINT(DEBUG, __VA_ARGS__)
    #define KERNEL_LOG_LEVEL_INFO
#endif

#ifdef KERNEL_LOG_LEVEL_INFO
    #ifdef KLOG_INFO
        #undef KLOG_INFO
    #endif
    #define KLOG_INFO(...)   _KLOG_PRINT(INFO, __VA_ARGS__)
#define KERNEL_LOG_LEVEL_ERR
#endif

#ifdef KERNEL_LOG_LEVEL_ERR
    #ifdef KLOG_ERR
        #undef KLOG_ERR
    #endif
    #define KLOG_ERR(...)    _KLOG_PRINT(ERR, __VA_ARGS__)
#endif

#define KLOG(level, fmt, args...) KLOG_##level(fmt, ##args)
 
/* functions */
static __forceinline__ __device__ int dstrcmp(const char *str1, const char *str2)
{
    while(*str1 && (*str1 == *str2)) {
        str1++;
        str2++;
    }

    return *str1 - *str2;
}

#endif /* KERNEL_UTILS_H */

// log levels
// NONE
// ERR
// INFO
// DEBUG