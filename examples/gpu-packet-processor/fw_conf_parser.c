#include "fw_conf_parser.h"
#include "utils.h"
#include "kernel.h"
#include "firewall.h"
#include "module_recent.h"

#include <rte_common.h>
#include <rte_ip.h>
#include <rte_malloc.h>
#include <bits/getopt_core.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#define CMD_LINE_OPT_DPORT  "dport"
#define CMD_LINE_OPT_SPORT  "sport"
#define CMD_LINE_OPT_NAME   "name"
#define CMD_LINE_OPT_SET    "set"
#define CMD_LINE_OPT_REMOVE "remove"
#define CMD_LINE_OPT_RCHECK "rcheck"

enum {
	/* long options mapped to a short option */
	/* first long only option value must be >= 256, so that we won't conflict with short options */
	CMD_LINE_OPT_OFFSET = 256,
	CMD_LINE_OPT_DPORT_NUM,
	CMD_LINE_OPT_SPORT_NUM,
	CMD_LINE_OPT_NAME_NUM,
	CMD_LINE_OPT_SET_NUM,
	CMD_LINE_OPT_REMOVE_NUM,
	CMD_LINE_OPT_RCHECK_NUM,
};

static const struct option lgopts[] = {
	{CMD_LINE_OPT_DPORT, required_argument, 0, CMD_LINE_OPT_DPORT_NUM},
	{CMD_LINE_OPT_SPORT, required_argument, 0, CMD_LINE_OPT_SPORT_NUM},
	{CMD_LINE_OPT_NAME, required_argument, 0, CMD_LINE_OPT_NAME_NUM},
	{CMD_LINE_OPT_SET, no_argument, 0, CMD_LINE_OPT_SET_NUM},
	{CMD_LINE_OPT_REMOVE, no_argument, 0, CMD_LINE_OPT_REMOVE_NUM},
	{CMD_LINE_OPT_RCHECK, no_argument, 0, CMD_LINE_OPT_RCHECK_NUM},
};

static const char fw_short_options[] = 
	"s:"			/* src ip */
	"d:"			/* dst ip */
	"j:"			/* jump & operation */
	"p:"			/* protocol */
    "A:"            /* append to chain */
    "N:"            /* create new chain */
    "D:"            /* delete chain */
    "m:"            /* module */
    ;

static inline int parse_recent_name(struct module_recent_args *recent_args, const char *optarg)
{
    if (strlen(optarg) > 32) {
        printf("list name too long \n");
        return -1;
    }

    strcpy(recent_args->name, optarg);
    return 0;
}

static inline int parse_chain_name(struct conf_cmd *cc, const char *optarg)
{
    if (strlen(optarg) > 32) {
        printf("chain name too long \n");
        return -1;
    }

    cc->chain = optarg;
    return 0;
}

static inline int parse_ip(char *ip_str, in_addr_t *ip) 
{
    if (UNLIKELY(ip == NULL || ip_str == NULL)) {
        FW_LOG(ERR, "parse_ip input error\n");
        goto err;
    }

    int64_t ip_parts[4];
    uint8_t *ip_result = (uint8_t *) ip;

	int ret = sscanf(ip_str, "%li.%li.%li.%li", 
                     ip_parts + 0, 
                     ip_parts + 1, 
                     ip_parts + 2, 
                     ip_parts + 3);
    
    for (int i = 0; i < 4; ++i) {
        if (ip_parts[i] > 255 || ip_parts[i] < 0)
            goto err;
        ip_result[i] = (uint8_t) ip_parts[i];
    }

    if (*ip == 0 || ret == EOF)
        goto err;

    return 0;

err:
    printf("invalid ip address: '%s' \n", ip_str);
    return -1;
}

static inline int parse_port(char *port_str, in_port_t *port) 
{
    if (port_str == NULL)
        goto err;

    int64_t p = atoi(port_str);

    if (p == 0 || p < 0 || p > IN_CLASSB_MAX) {
        printf("invalid port number '%s' \n", port_str);
        goto err;
    }

    *port = (in_port_t) p;
    return 0;

err:
    return -1;
}

//static inline enum pktop parse_operation(struct nv_array *fw, char *op_str) 
static int parse_operation(struct nv_array *fw, char *op_str, enum pktop *op) 
{
    *op = PKTOP_IGNORE;

    if (strcmp(op_str, "drop") == 0) {
        *op = PKTOP_DROP;
    } else if (strcmp(op_str, "accept") == 0) {
        *op = PKTOP_ACCEPT;
    } else {
        struct nv_firewall_chain *p = nv_firewall_get_chain(fw, op_str);
        if (p != NULL) {
            *op = PKTOP_JUMP;
        } else {
            printf("unknown jump operation provided: %s \n", op_str);
            return -1;
        }
    }

    return 0;
}

static int parse_protocol(char *proto_str) 
{
    int16_t proto = -1;

    if (proto_str == NULL)
        goto EXIT;

    if (strcmp(proto_str, "udp") == 0) {
        proto = IPPROTO_UDP;
    } else if(strcmp(proto_str, "tcp") == 0) {
        proto = IPPROTO_TCP;
    } else {
        printf("invalid protocol type: %s \n", proto_str);
        proto = IPPROTO_ANY;
    }

EXIT:
    return proto;
}

static int parse_args(char **argv, size_t argc, struct nv_array *fw) 
{
	int longind, ret;
    enum pktop op = PKTOP_IGNORE;
    int16_t proto = IPPROTO_ANY;

    struct conf_cmd p = {
        .cmd = 0,
        .chain = ""
    };

    struct command_state cs = {
        .options = 0,
        .c = 0
    };
    
    struct command_arguments args = {
        .sip = 0,
        .dip = 0,
        .sport = 0,
        .dport = 0,
    };

    struct module_recent_args recent_args = {
        .cmd = 0,
        .name = DEFAULT_LIST_NAME,
    };

    if (argv == NULL || fw == NULL) {
        FW_LOG(ERR, "invalid arguments for parse_args");
        goto ERR;
    }
    
    /* reset getopt */
    optind = 1;

	while ((cs.c = getopt_long(argc, argv, fw_short_options, lgopts, &longind)) != EOF) {
        if (optarg != NULL) {
            FW_LOG(DEBUG, "processing arg: %c -> %s \n", cs.c, optarg);
            
            /* standardize optargs to lowercase */
            str_to_lower(&optarg);
        } else {
            FW_LOG(DEBUG, "processing arg: %c \n", cs.c);
        }

		switch (cs.c) {
		case 's':
            FLAG_SET(cs.options, OPT_SOURCE);
            if (parse_ip(optarg, &args.sip) != 0)
                goto ERR;
			break;
		case 'd':
            FLAG_SET(cs.options, OPT_DESTINATION);
            if (parse_ip(optarg, &args.dip) != 0)
                goto ERR;
			break;
		case 'j':
            if (parse_operation(fw, optarg, &op) != 0)
                goto ERR;
            strcpy(args.jump_chain, optarg);
			break;
		case 'p':
            if ((proto = parse_protocol(optarg)) == -1)
                goto ERR;
            FLAG_SET(cs.options, OPT_PROTOCOL);
			break;
        case 'A':
            FLAG_SET(p.cmd, CMD_APPEND);
            p.chain = optarg;
            if (parse_chain_name(&p, optarg) != 0)
                goto ERR;
            break;
        case 'N':
            FLAG_SET(p.cmd, CMD_NEW_CHAIN);
            p.chain = optarg;
            if (parse_chain_name(&p, optarg) != 0)
                goto ERR;
            break;
        case 'D':
            FLAG_SET(p.cmd, CMD_DELETE);
            p.chain = optarg;
            if (parse_chain_name(&p, optarg) != 0)
                goto ERR;
            break;
        case 'm':
            /* do nothing */
            break;
        /* long options */
        case CMD_LINE_OPT_DPORT_NUM:
            if (parse_port(optarg, &args.dport) != 0)
                goto ERR;
            break;
        case CMD_LINE_OPT_SPORT_NUM:
            if (parse_port(optarg, &args.sport) != 0)
                goto ERR;
            break;
        case CMD_LINE_OPT_NAME_NUM:
            if (parse_recent_name(&recent_args, optarg) != 0)
                goto ERR;
            break;
        case CMD_LINE_OPT_SET_NUM:
            FLAG_SET(recent_args.cmd, PKTOP_RECENT_SET);
            break;
        case CMD_LINE_OPT_RCHECK_NUM:
            FLAG_SET(recent_args.cmd, PKTOP_RECENT_RCHECK);
            break;
        case CMD_LINE_OPT_REMOVE_NUM:
            FLAG_SET(recent_args.cmd, PKTOP_RECENT_REMOVE);
            break;
        default:
            FW_LOG(ERR, "error in firewall conf file format! format specifier=%s\n", optarg);
            goto ERR;
        }
    }

    FW_LOG(DEBUG, 
          "command state: \n"
          "\t options: \n"
          "\t\t OPT_PROTOCOL: %i \n"
          "\t\t OPT_SOURCE: %i \n"
          "\t\t OPT_DESTINATION: %i \n"
          "\t c: %i \n",
          FLAG_IS_SET(cs.options, OPT_PROTOCOL),
          FLAG_IS_SET(cs.options, OPT_SOURCE),
          FLAG_IS_SET(cs.options, OPT_DESTINATION),
          cs.c);
    
    FW_LOG(DEBUG,
           "conf_cmd: \n"
           "\t chain: %s \n"
           "\t cmd: \n"
           "\t\t CMD_APPEND: %i \n"
           "\t\t CMD_NEW_CHAIN: %i \n"
           "\t\t CMD_DELETE: %i \n",
           p.chain,
           FLAG_IS_SET(p.cmd, CMD_APPEND),
           FLAG_IS_SET(p.cmd, CMD_NEW_CHAIN),
           FLAG_IS_SET(p.cmd, CMD_DELETE));

    struct nv_firewall_rule new_rule = {
        .operation = op,
        .src_port = args.sport,
        .dst_port = args.dport,
        .proto = proto,
        .src_ip.v4 = args.sip,
        .dst_ip.v4 = args.dip,
    };

    if (op == PKTOP_JUMP) {
        struct nv_firewall_chain *c = nv_firewall_get_chain(fw, args.jump_chain);
        if (c != NULL)
            new_rule.jump_chain = c->name;
    }

    if (FLAG_IS_SET(p.cmd, CMD_NEW_CHAIN)) {
        if (nv_firewall_add_chain(fw, p.chain) == NULL) {
            FW_LOG(ERR, "failed to add new chain '%s' to gpu firewall \n", p.chain);
            goto ERR;
        }
    }

    if (FLAG_IS_SET(p.cmd, CMD_APPEND)) {
        if (recent_args.cmd != 0) {
            ret = module_recent_add_list(recent_args.name);
            if (ret != 0) {
                FW_LOG(ERR, "failed to create new list in recent module \n");
                goto ERR;
            }

            strcpy(new_rule.recent_list_name, recent_args.name);
            FLAG_SET(new_rule.operation, recent_args.cmd);
        }

        ret = nv_firewall_add_rule(fw, p.chain, new_rule);
        if (ret != 0) {
            FW_LOG(ERR, "failed to add rule to gpu firewall \n");
            goto ERR;
        }
    }

    /* TODO: implement chain deleting */
    // if (p.cmd & CMD_DELETE)
    //     delete_chain(fw, p.chain);

    return 0;

ERR:
    return -1;
}

static int nv_fw_parser_str_to_words(char *str, size_t l, size_t *argc, char ***argv) 
{
    char **argv_ptr = NULL;

    if (UNLIKELY(str == NULL || argc == NULL || l == 0))
        goto ERR;
    
    if (UNLIKELY(argv != NULL))
        rte_free(argv);
    
    *argc = 0;
    argv_ptr = *argv = rte_zmalloc(NULL, sizeof(char) * l, 0);
    if (argv_ptr == NULL) {
        FW_LOG(ERR, "failed to rte_zmalloc argv \n");
        goto ERR;
    }

    char *t = strtok(str, " ");
    while (t != NULL) {
        int length = strlen(t);
        argv_ptr[*argc] = rte_zmalloc(NULL, length * sizeof(char), 0);

        strcpy(argv_ptr[*argc], t);

        ++(*argc);
        t = strtok(NULL, " ");
    }

    free(t);
    return 0;

ERR:
    return 1;
}

static int nv_fw_parser_parse_line(char *line, size_t size, struct nv_array *fw) 
{
    char **argv = NULL;
    int ret;
    size_t argc;
    
    if (UNLIKELY(fw == NULL || line == NULL))
        goto ERR;

    if (line[0] == '\n') {
        FW_LOG(DEBUG, "skipping empty line \n");
        goto SKIP;
    }

    if (line[0] == '#') {
        FW_LOG(DEBUG, "skipping comment line: %s \n", line);
        goto SKIP;
    }

    /* replace trailing carriage return with null terminator if neccessary */
    if (line[size - 1] == '\n')
        line[size - 1] = '\0';

    FW_LOG(DEBUG, "parsing line: %s \n", line);

    ret = nv_fw_parser_str_to_words(line, size, &argc, &argv);
    if (argv == NULL) {
        FW_LOG(ERR, "str_to_words returned null ptr argv \n");
        goto ERR;
    }
    
    ret = parse_args(argv, argc, fw);
    if (ret != 0) {
        FW_LOG(ERR, "argument parsing error occured \n");
        goto ERR;
    }

    rte_free(argv);
SKIP:
    return 0;

ERR:
    return -1;
}

static int nv_fw_parser_parse_config_file(char *path, struct nv_array *fw) 
{
    FILE *f = NULL;
    char * line = NULL;
    size_t len = 0;
    ssize_t size;

    if (path == NULL || fw == NULL) {
        FW_LOG(ERR, "parse_config_file input error\n");
        goto ERR;
    }

    f = fopen(path, "r");
    if (f == NULL) {
        FW_LOG(ERR, "failed to open firewall config file %s\n", path);
        goto ERR;
    }

    FW_LOG(INFO, "opened config file: %s \n", realpath(path, NULL));

    while ((size = getline(&line, &len, f)) != -1) {
        int ret = nv_fw_parser_parse_line(line, size, fw);
        if (ret != 0) {
            FW_LOG(ERR, "failed to parse line \n");
            goto ERR_PARSE_LINE;
        }
    }

    free(line);
    fclose(f);

    return 0;

ERR_PARSE_LINE:
    free(line);
    fclose(f);
ERR:
    return -1;
}

int nv_fw_parser_init_from_file(char *path, struct nv_firewall **fw) 
{
    int ret;

    if (path == NULL)
        goto ERR;

    ret = nv_firewall_init(fw);
    if (ret != 0)
        goto ERR;

    assert(fw != NULL);

    ret = nv_fw_parser_parse_config_file(path, (*fw)->chains);
    if (ret != 0) {
        nv_firewall_destroy(*fw);
        goto ERR;
    }

    nv_firewall_debug_print(*fw);

    return 0;

ERR:
    return -1;
}