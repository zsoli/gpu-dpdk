/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright (c) 2018-2019, NVIDIA CORPORATION.  All rights reserved.
 */

#include "utils.h"
#include "kernel.h"

#include <stdio.h>
#include <rte_ethdev.h>
#include <rte_ether.h>

void str_to_lower(char *str[]) {
	if (str == NULL) {
		RTE_LOG(ERR, APP, "str_to_lower input error, str ptr is null \n");
		return;
	}

    for(int i = 0; (*str)[i]; i++){
        (*str)[i] = tolower((*str)[i]);
    }
}

/* ================== WORKLOAD ================== */
void nvl2fwd_macswap_cpu(uintptr_t * i_ptr, int nmbuf, int payload, int mac_update) 
{
	struct rte_ether_hdr *eth;
	uint8_t *data_ptr;
	uint16_t temp;
	uint16_t *src_addr, *dst_addr;
	struct rte_mbuf *mbuf;
	uintptr_t *uptrs;
	int i = 0;

	if (i_ptr == NULL)
		return;

	uptrs = (uintptr_t *) i_ptr;

	for (i = 0; i < nmbuf; i++) {
		if (payload == 1) {
			data_ptr = ((uint8_t *) (uptrs[i])) + RTE_PKTMBUF_HEADROOM;
		} else {
			mbuf = (struct rte_mbuf *)uptrs[i];
			data_ptr = (uint8_t *) mbuf->buf_addr + mbuf->data_off;
		}
		eth = (struct rte_ether_hdr *)data_ptr;

		src_addr = (uint16_t *) (&eth->s_addr);
		dst_addr = (uint16_t *) (&eth->d_addr);

#ifdef DEBUG_PRINT
		uint8_t *src = (uint8_t *) (&eth->s_addr);
		uint8_t *dst = (uint8_t *) (&eth->d_addr);
		printf ("#%d, mbuf_addr=%lx, Source: %02x:%02x:%02x:%02x:%02x:%02x Dest: %02x:%02x:%02x:%02x:%02x:%02x\n",
		     i, mbuf, src[0], src[1], src[2], src[3], src[4], src[5],
		     dst[0], dst[1], dst[2], dst[3], dst[4], dst[5]);
#endif

		if (mac_update) {
			temp = dst_addr[0];
			dst_addr[0] = src_addr[0];
			src_addr[0] = temp;
			temp = dst_addr[1];
			dst_addr[1] = src_addr[1];
			src_addr[1] = temp;
			temp = dst_addr[2];
			dst_addr[2] = src_addr[2];
			src_addr[2] = temp;
		}
	}
}

int nvl2fwd_process_gpu(uintptr_t * i_ptr, int nmbuf, int payload, cudaStream_t stream, uint8_t * sync_flag, int mac_update, int cuda_blocks, int cuda_threads, struct nv_firewall *fw) 
{
	//Optionally add burst_dev_ptr = rte_upload_burst_gmem_nv(burst_ptr_hmem)
	if (cuda_blocks <= 0 || cuda_threads <= 0) {
		fprintf(stderr, "cuda_blocks nor cuda_threads are invalid\n");
		return 1;
	}

	nvl2fwd_launch_gpu_processing(i_ptr, nmbuf, payload, sync_flag, mac_update, cuda_blocks, cuda_threads, stream, fw);
	//NV_CUDA_CHECK(cudaDeviceSynchronize());
	return 0;
}

int nvl2fwd_persistent_kernel(struct nv_pk_ring_obj *pk_ring, int tot_pkring_obj, uint8_t * pk_quit_d, int use_payload, int mac_update, int cuda_blocks, int cuda_threads, cudaStream_t stream) 
{
	if (pk_ring == NULL || tot_pkring_obj <= 0 || pk_quit_d == NULL || cuda_blocks != 1 || cuda_threads <= 0)
		return 1;

	nvl2fwd_launch_pk(pk_ring, tot_pkring_obj, pk_quit_d, use_payload, mac_update, cuda_blocks, cuda_threads, stream);
	//NV_CUDA_CHECK(cudaDeviceSynchronize());

	return 0;
}

/* ================== CMD LINE OPTIONS ================== */
void l2fwdnv_usage(const char *prgname) 
{
	printf("\n\n%s%s [EAL options] -- b|c|d|e|g|m|s|w|f|B|E|N|P|W|--firewall-config-file\n"
	       " -b BURST SIZE: how many pkts x burst to RX\n"
	       " -c CORES: how many RX/TX cores to use (2 cores == 1 RX/TX queue)\n"
	       " -d DATA ROOM SIZE: mbuf payload size\n"
	       " -e MBUFS: how many mbufs x NV mempool\n"
	       " -g GPU DEVICE: GPU device ID\n"
	       " -m MEMP TYPE: allocate mbufs payloads in 0: host pinned memory, 1: GPU device memory\n"
	       " -s CUDA STREAMS: how many CUDA streams x queue (RX/TX pair of cores)\n"
	       " -w WORKLOAD TYPE: who is in charge to swap the MAC address, 0: No swap, 1: CPU, 2: GPU with one dedicated CUDA kernel for each burst of received packets, 3: GPU with a persistent CUDA kernel\n"
		   " -f IP: filter the supplied ip\n"
	       " -B WORKLOAD POINTERS: during workload, received packets are accessed from 0: mbuf structure (header), 1: a direct pointer to the payload (default: 1)\n"
	       " -E CUDA EVENT: in case of -w 2, sync CUDA kernel and TX core with a CUDA Event, 0: No, 1: Yes (default: 0)\n"
	       " -M update MAC address during workload\n"
	       " -N CUDA PROFILER: Enable CUDA profiler with NVTX for nvvp\n"
	       " -P PERFORMANCE PKTS: packets to be received before closing the application. If 0, l2fwd-nv keeps running until the CTRL+C\n"
	       " -W WARMUP PKTS: wait this amount of packets before starting to measure performance\n",
	       " --firewall-config-file PATH: specify the firewall config file path\n",
		   prgname);
}

unsigned int l2fwd_parse_device_id(const char *q_arg) 
{
	char *end = NULL;
	unsigned long n;

	n = strtoul(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return 0;

	return n;
}

int l2fwd_parse_mac_update(const char *q_arg) 
{
	char *end = NULL;
	unsigned long n;

	n = strtoul(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return 0;

	if (n > 0)
		return 1;

	return 0;
}

unsigned int l2fwd_parse_nqueue(const char *q_arg) 
{
	char *end = NULL;
	unsigned long n;

	n = strtoul(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return 0;
	if (n == 0)
		return 0;

	if (n >= MAX_RX_QUEUE_PER_LCORE)
		return 0;

	return n;
}

unsigned int l2fwd_parse_queue_elems(const char *q_arg) 
{
	char *end = NULL;
	unsigned long n;

	n = strtoul(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return 0;
	if (n == 0)
		return 0;

	return n;
}

int l2fwd_parse_nb_streams(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;
	if (n > MAX_NB_STREAMS)
		return -1;
	if (n == 0) {
		return 1;
	}
	return n;
}

int l2fwd_parse_dataroom_size(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;
	if (n <= 0)		// || n > 2048)
		return -1;
	return n;
}

int l2fwd_parse_burst_size(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;
	if (n > DEFAULT_PKT_BURST_INPUT)
		return -1;
	return n;
}

int l2fwd_parse_cores(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;
	if (n < 0)
		return -1;
	if ((n > 1) && (n % 2 != 0))
		return -1;
	if (n > MAX_CORES)
		return -1;

	return n;
}

int l2fwd_parse_warmup_packets(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;

	if (n > 0 && n < DRIVER_MIN_RX_PKTS)
		return DRIVER_MIN_RX_PKTS;

	return n;
}

int l2fwd_parse_performance_packets(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;

	if (n > 0 && n < DRIVER_MIN_RX_PKTS)
		return DRIVER_MIN_RX_PKTS;

	return n;
}

int l2fwd_parse_burst_use_payload(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;
	return n;
}

int l2fwd_parse_workload(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;

	if (n == 1)
		return CPU_WORKLOAD;
	if (n == 2)
		return GPU_WORKLOAD;
	if (n == 3)
		return GPU_PK_WORKLOAD;

	return NO_WORKLOAD;
}

int l2fwd_parse_memtype(const char *q_arg) 
{
	char *end = NULL;
	int n;

	n = strtol(q_arg, &end, 10);
	if ((q_arg[0] == '\0') || (end == NULL) || (*end != '\0'))
		return -1;

	if (n == MEM_DEVMEM)
		return MEM_DEVMEM;

	return MEM_HOST_PINNED;
}

/* Check the link status of all ports in up to 9s, and print them finally */
void check_all_ports_link_status(uint32_t port_mask) 
{
#define CHECK_INTERVAL 100	/* 100ms */
#define MAX_CHECK_TIME 90	/* 9s (90 * 100ms) in total */
	uint16_t portid;
	uint8_t count, all_ports_up, print_flag = 0;
	struct rte_eth_link link;

	//printf("\nChecking link status");
	for (count = 0; count <= MAX_CHECK_TIME; count++) {
		//if (force_quit) return;
		all_ports_up = 1;
		RTE_ETH_FOREACH_DEV(portid) {
			//if (force_quit) return;
			if ((port_mask & (1 << portid)) == 0)
				continue;
			memset(&link, 0, sizeof(link));
			rte_eth_link_get_nowait(portid, &link);
			/* print link status if flag set */
			if (print_flag == 1) {
				if (link.link_status)
					printf("Port%d Link Up. Speed %u Mbps - %s\n", portid, link.link_speed,
					     (link.link_duplex == ETH_LINK_FULL_DUPLEX) ? ("full-duplex") : ("half-duplex\n"));
				else
					printf("Port %d Link Down\n", portid);
				continue;
			}
			/* clear all_ports_up flag if any link down */
			if (link.link_status == ETH_LINK_DOWN) {
				all_ports_up = 0;
				break;
			}
		}
		/* after finally printing all link status, get out */
		if (print_flag == 1)
			break;

		if (all_ports_up == 0) {
			//printf(".");
			fflush(stdout);
			rte_delay_ms(CHECK_INTERVAL);
		}

		/* set the print_flag if all ports up or timeout */
		if (all_ports_up == 1 || count == (MAX_CHECK_TIME - 1)) {
			print_flag = 1;
			//printf("done\n");
		}
	}
}
