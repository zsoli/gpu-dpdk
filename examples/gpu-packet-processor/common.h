#ifndef COMMON_H
#define COMMON_H

#include <rte_common.h>
#include <rte_log.h>
#include <time.h>
#include <assert.h>
#include <string.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <cuda_profiler_api.h>

#define PROFILE_NVTX_RANGES

// #define KERNEL_LOG_LEVEL_DEBUG
#define KERNEL_LOG_LEVEL_INFO
// #define KERNEL_LOG_LEVEL_ERR

#define MAX(a, b)                           \
({                                          \
    __typeof__(a) _max_a = (a);             \
    __typeof__(b) _max_b = (b);             \
    (_max_a) > (_max_b) ? _max_a : _max_b;  \
})

#define MIN(a, b)                           \
({                                          \
    __typeof__(a) _min_a = (a);             \
    __typeof__(b) _min_b = (b);             \
    (_min_a) > (_min_b) ? _min_a : _min_b;  \
})

#define NV_PTROFF(ptr, offset, type) ((type*) ((uint8_t*) (ptr) + (int) (offset)))

#define FLAG_MASK(field, mask, flag)	(((field) & (mask)) == (flag))

#define FLAG_IS_SET(field, flag) FLAG_MASK(field, flag, flag)

#define FLAG_SET(field, flag) ((field) |= (flag))

#define FLAG_CLEAR(field, flag) ((field) &= ~(flag))

#define FLAG_UPDATE(field, flag, val) ((val) ? FLAG_SET(field, flag) : FLAG_CLEAR(field, flag))

#ifndef LIKELY
    #ifdef __GNUC__
        #define LIKELY(x) (__builtin_expect(!!(x), 1))
        #define UNLIKELY(x) (__builtin_expect(!!(x), 0))
    #else
        #define LIKELY(x) (x)
        #define UNLIKELY(x) (x)
    #endif
#endif

/* logging */
#define RTE_LOGTYPE_APP                 RTE_LOGTYPE_USER1
#define LOG_TO_FILE

#define LOG(level, fmt, args...) \
    rte_log(RTE_LOG_ ## level, RTE_LOGTYPE_APP, "APP: " fmt, ## args)


#define DEFAULT_PKT_BURST               32
#define DEFAULT_PKT_BURST_INPUT         8192
#define MAC_CUDA_BLOCKS                 1
#define MAC_THREADS_BLOCK               256
#define PK_CUDA_BLOCKS                  1
#define PK_CUDA_THREADS                 256

/*
 * Configurable number of RX/TX ring descriptors
 */
#define RTE_TEST_RX_DESC_DEFAULT        256
#define RTE_TEST_TX_DESC_DEFAULT        256
#define MAX_CORES                       16
#define MAX_RX_QUEUE_PER_LCORE          (MAX_CORES / 2)
#define MAX_TX_QUEUE_PER_LCORE          MAX_RX_QUEUE_PER_LCORE

/* Others */
#define MAX_NB_STREAMS                  64
#define DEF_NB_MBUF                     65536
#define DRIVER_MIN_RX_PKTS              4

#ifdef PROFILE_NVTX_RANGES
#include <nvToolsExt.h>

#define COMM_COL 1
#define SM_COL   2
#define SML_COL  3
#define OP_COL   4
#define COMP_COL 5
#define SOLVE_COL 6
#define WARMUP_COL 7
#define EXEC_COL 8

#define SEND_COL 9
#define WAIT_COL 10
#define KERNEL_COL 11

#define PUSH_RANGE(name,cid)                                            \
      do {                                                                  \
        const uint32_t colors[] = {                                         \
                0x0000ff00, 0x000000ff, 0x00ffff00, 0x00ff00ff, 0x0000ffff, 0x00ff0000, 0x00ffffff, 0xff000000, 0xff0000ff, 0x55ff3300, 0xff660000, 0x66330000  \
        };                                                                  \
        const int num_colors = sizeof(colors)/sizeof(colors[0]);            \
        int color_id = cid%num_colors;                                  \
        nvtxEventAttributes_t eventAttrib = {0};                        \
        eventAttrib.version = NVTX_VERSION;                             \
        eventAttrib.size = NVTX_EVENT_ATTRIB_STRUCT_SIZE;               \
        eventAttrib.colorType = NVTX_COLOR_ARGB;                        \
        eventAttrib.color = colors[color_id];                           \
        eventAttrib.messageType = NVTX_MESSAGE_TYPE_ASCII;              \
        eventAttrib.message.ascii = name;                               \
        nvtxRangePushEx(&eventAttrib);                                  \
      } while(0)

#define PUSH_RANGE_STR(cid, FMT, ARGS...)       \
      do {                                          \
        char str[128];                              \
        snprintf(str, sizeof(str), FMT, ## ARGS);   \
        PUSH_RANGE(str, cid);                       \
      } while(0)

#define POP_RANGE do { nvtxRangePop(); } while(0)
#else
#define PUSH_RANGE(name,cid)
#define POP_RANGE
#endif /* PROFILE_NVTX_RANGES */

/* ================ TIMING ================ */
#define TIMER_DEF_MAIN                  struct timespec temp_1, temp_2
#define TIMER_DEF_INIT                  struct timespec temp_7, temp_8
#define TIMER_START_MAIN                if(clock_gettime(CLOCK_MONOTONIC, &temp_1)) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_START_INIT                if(clock_gettime(CLOCK_MONOTONIC, &temp_7)) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_STOP_MAIN                 if(clock_gettime(CLOCK_MONOTONIC, &temp_2)) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_STOP_INIT                 if(clock_gettime(CLOCK_MONOTONIC, &temp_8)) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_ELAPSED_NS_MAIN           (temp_2.tv_sec-temp_1.tv_sec)*1.e9 + (temp_2.tv_nsec-temp_1.tv_nsec)
#define TIMER_ELAPSED_NS_INIT           (temp_8.tv_sec-temp_7.tv_sec)*1.e9 + (temp_8.tv_nsec-temp_7.tv_nsec)

#define TIMER_DEF_CORE_RX(x)            struct timespec temp_rx_start[x]; struct timespec temp_rx_end[x];
#define TIMER_START_CORE_RX(x)          if(clock_gettime(CLOCK_MONOTONIC, &temp_rx_start[x])) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_STOP_CORE_RX(x)           if(clock_gettime(CLOCK_MONOTONIC, &temp_rx_end[x])) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_ELAPSED_NS_CORE_RX(x)     (temp_rx_end[x].tv_sec-temp_rx_start[x].tv_sec)*1.e9 + (temp_rx_end[x].tv_nsec-temp_rx_start[x].tv_nsec)

#define TIMER_DEF_CORE_TX(x)            struct timespec temp_tx_start[x]; struct timespec temp_tx_end[x];
#define TIMER_START_CORE_TX(x)          if(clock_gettime(CLOCK_MONOTONIC, &temp_tx_start[x])) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_STOP_CORE_TX(x)           if(clock_gettime(CLOCK_MONOTONIC, &temp_tx_end[x])) rte_exit(EXIT_FAILURE, "Timer error!\n");
#define TIMER_ELAPSED_NS_CORE_TX(x)     (temp_tx_end[x].tv_sec-temp_tx_start[x].tv_sec)*1.e9 + (temp_tx_end[x].tv_nsec-temp_tx_start[x].tv_nsec)

#define MEM_DEVMEM 1
#define MEM_HOST_PINNED 0

#ifdef NV_TEGRA
    #define NV_MIN_PIN_SIZE NV_GPU_PAGE_SIZE
    #define NV_GPU_PAGE_SHIFT 12
#else
    #define NV_MIN_PIN_SIZE 4
    #define NV_GPU_PAGE_SHIFT 16
#endif /* NV_TEGRA */

#define GPU_PAGE_SIZE (1UL << GPU_PAGE_SHIFT)

enum workload_flags {
	CPU_WORKLOAD = 0,
	GPU_WORKLOAD = 1 << 0,
	GPU_PK_WORKLOAD = 1 << 1,
	NO_WORKLOAD = 1 << 2
};

#ifndef NV_CUDA_CHECK
    #define NV_CUDA_CHECK(stmt)                                 		\
        do {                                                       		\
            cudaError_t result = (stmt);                           		\
            if (result != cudaSuccess) {                        	    \
                fprintf(stderr, "[%s:%d] cuda failed with error: %s \n",\
                   __FILE__, __LINE__,cudaGetErrorString(result));  	\
				rte_exit(EXIT_FAILURE, "cuda error\n");					\
            }                                                       	\
            assert(cudaSuccess == result);                          	\
        } while (0)
#endif /* NV_CUDA_CHECK */

#ifndef NV_ACCESS_ONCE
    #define NV_ACCESS_ONCE(x) (*(volatile typeof(x) *)&(x))
#endif /* NV_ACCESS_ONCE */

#endif /* COMMON_H */