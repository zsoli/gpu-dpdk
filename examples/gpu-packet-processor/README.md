# l2fwd-nv example

In the vanilla l2fwd DPDK example each thread (namely, DPDK core) receives a burst of packets, does a swap of the src/dst MAC addresses and transmits back the same burst of modified packets.
l2fwd-nv is an improvement of l2fwd to show the usage of mbuf pool with GPU pinned data buffers (NV mempool) using the vanilla DPDK API. The overall flow of the app is organised as follows:
- Start a number of cores and organize them in pairs. For each pair:
    - one core receives data (RX core)
    - one core transmits the data received from the related RX core (TX core)
- Each pair of RX/TX cores work on a different DPDK queue
- Each DPDK queue uses a different NV mempool (which can reside in host pinned memory or GPU device memory)
- RX core:
    - Waits to receive a burst of packets
    - Triggers (asynchronously) the work (MAC swapping) using a CUDA kernel
- TX core:
    - Waits for the completion of the CUDA kernel triggered by the related RX core
    - Transmits the packets

## Building

Assuming GPU DPDK has been built in the `build` directory, from the top-level GPU DPDK directory run:

```
meson configure build -Dexamples=l2fwd-nv
cd build
ninja
```

## Running

With the -h option, l2fwd-nv prints the usage:

```
./build/examples/dpdk-l2fwd-nv [EAL options] -- b|c|d|e|g|m|s|w|B|E|N|P|W
  -b BURST SIZE: how many pkts x burst to RX
  -c CORES: how many RX/TX cores to use (2 cores == 1 RX/TX queue)
  -d DATA ROOM SIZE: mbuf payload size
  -e MBUFS: how many mbufs x NV mempool
  -g GPU DEVICE: GPU device ID
  -m MEMP TYPE: allocate mbufs payloads in 0: host pinned memory, 1: GPU device memory
  -s CUDA STREAMS: how many CUDA streams x queue (RX/TX pair of cores)
  -w WORKLOAD TYPE: who is in charge to swap the MAC address, 0: No swap, 1: CPU, 2: GPU with one dedicated CUDA kernel for each burst of received packets, 3: GPU with a persistent CUDA kernel
  -B WORKLOAD POINTERS: during workload, received packets are accessed from 0: mbuf structure (header), 1: a direct pointer to the payload (default: 1)
  -E CUDA EVENT: in case of -w 2, sync CUDA kernel and TX core with a CUDA Event, 0: No, 1: Yes (default: 0, use an host pinned memory flag instead of a CUDA Event)
  -N CUDA PROFILER: Enable CUDA profiler with NVTX for nvvp
  -P PERFORMANCE PKTS: packets to be received before closing the application. If 0, l2fwd-nv keeps running until the CTRL+C
  -W WARMUP PKTS: wait this amount of packets before starting to measure performance
```

For further details about the EAL options please refer to https://doc.dpdk.org/guides/linux_gsg/linux_eal_parameters.html 

Best practices:
- If `-m 1`, the best configuration is `-B 1 -w 3 -E 0`
- With 10Gbps and 40Gbps NICs, line rate is easily reached with any type of workload using at least 2 cores `-c 2`
- With 100Gbps, you need at least 8 cores `-c 8` and 2 CUDA streams per queue `-s 2`
- `-N` option should be used only in case of `nvprof`
- Common values for the following options are `-e 131072`, `-W 30000`, `-P 3000000`, `-b 256`

Notes:
- During our tests, the `l2fwd-nv`/`testpmd` NICs were always connected back-to-back

### l2fwd-nv command line examples

`sudo ./build/examples/dpdk-l2fwd-nv -l 0-8 -n 8 -a b5:00.1 -- -m 1 -g 0 -w 3 -b 256 -c 8 -e 131072 -P 3000000 -W 30000`

Which means:
- Alloc the NV mempools on GPU device memory (`-m 1`)
- Use the GPU #0 (`-g 0`)
- For the workload, use a persistent kernel (`-w 3`)
- Receive a burst of 256 pkts before triggering the work on the GPU (`-b 256`)
- Use 8 DPDK cores: 4 RX and 4 TX cores (`-c 8`) which means 4 DPDK queues (and 4 NV mempools`)
- Use 131072 mbufs splitted among the 4 NV mempools (that is, 32768 mbufs per mempool`)
- Start to measure performance after 30000 warmup packets (`-W 30000`) and stop the execution after receiving 3000000 packets (`-P 3000000`)

`sudo ./build/examples/dpdk-l2fwd-nv -l 0-8 -n 8 -a b5:00.1 -- -m 1 -g 0 -w 2 -b 256 -c 8 -e 131072 -P 3000000 -W 30000 -s 2`

The differences with respect to the previous command line are:
- For the workload, launch a different CUDA kernel for each burst of received packets (`-w 2`)
- Each RX core has 2 CUDA streams available to launch the CUDA kernels (`-s 2`)

`sudo ./build/examples/dpdk-l2fwd-nv -l 0-8 -n 8 -a b5:00.1 -- -m 0 -w 0 -b 256 -c 8 -e 131072 -P 3000000 -W 30000`

The differences with respect to the previous command line are:
- Alloc the NV mempools on host pinned memory (`-m 0`)
- Do not trigger any workload (`-w 0`). This is useful to measure performance with communications only

## Packet generator

l2fwd-nv forwards the received packets but it doesn't generate new packets. To run this example, you need a packet generator and we recommend using testpmd for that purpose. Please refer to the "Traffic generator" section in `README.md` in the root directory of the repository.
