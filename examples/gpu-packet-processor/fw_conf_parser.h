#ifndef FW_CONF_PARSER_H
#define FW_CONF_PARSER_H

#include "firewall.h"
#include <netinet/in.h>

enum {
    CMD_NONE          = 0,
    CMD_APPEND        = 1 << 0,
    CMD_NEW_CHAIN     = 1 << 1,
    CMD_DELETE        = 1 << 2,
    CMD_RECENT_SET    = 1 << 3,
    CMD_RECENT_RCHECK = 1 << 4,
    CMD_RECENT_REMOVE = 1 << 5,
};

enum {
    OPT_NONE        = 0,
    OPT_SOURCE      = 1 << 0,
    OPT_DESTINATION = 1 << 1,
    OPT_PROTOCOL    = 1 << 2,
};

struct command_state {
    unsigned int options;
    int c;
};

struct command_arguments {
    in_port_t sport;
    in_port_t dport;
    in_addr_t sip;
    in_addr_t dip;
    char jump_chain[32];
};

struct conf_cmd {
    unsigned int cmd;
    const char *chain;
};

struct module_recent_args {
    unsigned int cmd;
    char name[32];
};

/* funcs */
int nv_fw_parser_init_from_file(char *path, struct nv_firewall **fw);

#endif /* FW_CONF_PARSER_H */