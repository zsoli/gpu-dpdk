#ifndef KERNEL_H
#define KERNEL_H

#include "firewall.h"
#include "nv_queue.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>

/* functions */
void nvl2fwd_launch_pk(struct nv_pk_ring_obj *pk_ring, int tot_pkring_obj, uint8_t * pk_quit_d, int use_payload, int mac_update, int cuda_blocks, int cuda_threads, cudaStream_t stream);

void nvl2fwd_launch_gpu_processing(uintptr_t *i_ptr, int nb_rx, int payload, uint8_t * sync_flag, int mac_update, int cuda_blocks, int cuda_threads, cudaStream_t stream, struct nv_firewall *nv_fw);

#endif /* KERNEL_H */