#ifndef L2FWD_ARRAY_H
#define L2FWD_ARRAY_H

#include "rte_log.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stddef.h>
#include <errno.h>
#include <stdio.h>
#include <stdbool.h>
#include "common.h"
#include "kernel_utils.h"

#ifdef __cplusplus
}
#endif /* __cplusplus */

#define nv_array_end(array) NV_PTROFF((array)->data, (array)->size, void)

#define nv_array_first(array) ((array)->data)

#define nv_array_check(array, ptr) (NV_PTROFF(ptr, sizeof(*ptr), void) <= nv_array_end(array))

#define nv_array_for_each(pos, array) \
	for (pos = (__typeof__(pos)) nv_array_first(array); nv_array_check(array, pos);	(pos)++)

struct nv_array {
    void *data;
    size_t size;
    size_t alloc;
    size_t extend;
	bool dev_alloc;
};

static inline int nv_array_init(struct nv_array **array, size_t extend)
{
	assert(array != NULL);
	
    cudaMallocManaged((void**) array, sizeof **array, cudaMemAttachGlobal);
	struct nv_array *a = *array;

	if (UNLIKELY(a == NULL))
		return -errno;

    a->data = NULL;
    a->dev_alloc = false;
    a->size = a->alloc = 0;
    a->extend = extend;

	return 0;
}

static __forceinline__ __device__ __host__ void* nv_array_realloc(struct nv_array *array, size_t alloc)
{
	assert(array != NULL);
	void *p = NULL;

#ifdef __CUDA_ARCH__
	p = malloc(alloc);
	KLOG(DEBUG, "nv_array (dev): allocated new buffer \n");

	if (LIKELY(p != NULL)) {
		memcpy(p, array->data, array->size);
		KLOG(DEBUG, "nv_array (dev): copied old data to new buff \n");
	}

	free(array->data);
	KLOG(DEBUG, "nv_array (dev): freed old buffer \n");

	atomicExch((int *) &array->dev_alloc, (int) true);
#else
	if (LIKELY(!array->dev_alloc)) {
		cudaMallocManaged((void**) &p, alloc, cudaMemAttachGlobal);
		LOG(DEBUG, "nv_array (host): allocated new buffer \n");

		if (LIKELY(p != NULL)) {
			cudaMemcpy(p, array->data, array->size, cudaMemcpyDeviceToDevice);
			LOG(DEBUG, "nv_array (host): copied old data to new buff \n");
		}

		cudaFree(array->data);
		LOG(DEBUG, "nv_array (host): freed old buffer \n");
		
		array->dev_alloc = false;
	} else {
		LOG(ERR, "array buffer is allocated on dev memory, cant access it from host \n");
	}
#endif
	
	return p;
}

static __forceinline__ __device__ __host__ int nv_array_ensure_size(struct nv_array *array, size_t size)
{
	assert(array != NULL);
	size_t alloc, need;

	alloc = array->alloc;
	need = array->size + size;

#ifdef __CUDA_ARCH__
	KLOG(DEBUG, "nv_array (dev): allocated: %lu, need: %lu \n", alloc, need);
#else
	LOG(DEBUG, "nv_array (host): allocated: %lu, need: %lu \n", alloc, need);
#endif

	if (UNLIKELY(alloc < need)) {
		void *data;

		alloc = MAX(alloc, array->extend);
		assert(alloc != 0);

		while (alloc < need)
			alloc *= 2;
        
		if (UNLIKELY((data = nv_array_realloc(array, alloc)) == NULL))
			return -1;
        
		array->data = data;
		array->alloc = alloc;
	}

	return 0;
}

static __forceinline__ __device__ __host__ void* nv_array_add(struct nv_array *array, size_t size)
{
	assert(array != NULL);
	void *p;

	if (nv_array_ensure_size(array, size) < 0)
		return NULL;

	p = NV_PTROFF(array->data, array->size, void);
	array->size += size;

	return p;
}

#endif /* L2FWD_ARRAY_H */