#include <netinet/in.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "module_recent.h"
#include "firewall.h"
#include "common.h"
#include "array.h"

#define LIST_EXISTS(name, found) list_exists(name, found, NULL)

/* starting size of array of lists in bytes */
#define MODULE_RECENT_LIST_SIZE 2

/* starting size of the list of entries in each list in bytes */
#define MODULE_RECENT_ENTRY_SIZE 2

struct nv_array *list_array;

static struct module_recent_list* get_list(const char *name)
{
    struct module_recent_list *list;

    nv_array_for_each(list, list_array) {
        if (strcmp(list->name, name) == 0) {
            return list;
        }
    }

    return NULL;
}

int module_recent_init(void)
{
    int ret;
    RC_LOG(DEBUG, "initializing recent module... \n");

    /* init list array */
    nv_array_init(&list_array, MODULE_RECENT_LIST_SIZE);
    
    /* add default list */
    ret = module_recent_add_list(DEFAULT_LIST_NAME);
    if (ret != 0) {
        RC_LOG(ERR, "failed to add default list \n");
        goto err_2;
    }

    RC_LOG(DEBUG, "initialized recent module \n");
    return 0;

err_2:
    cudaFree(list_array);
    return -1;
}

// TODO: currently not implemented. not necessary
int module_recent_destroy(void) {
    return 0;
}

int module_recent_add_list(const char *list_name)
{
    struct module_recent_list *list = get_list(list_name);
    if (list != NULL) {
        RC_LOG(DEBUG, "list '%s' already exists in the array \n", list_name);
        goto skip;
    }
    
    list = nv_array_add(list_array, sizeof *list);
    if (list == NULL) {
        FW_LOG(ERR, "failed to allocate new list \n");
        goto err;
    }

    // NV_CUDA_CHECK(cudaMallocManaged((void**) &list, sizeof *list, cudaMemAttachGlobal));
    // if (list == NULL) {
    //     FW_LOG(ERR, "failed to allocate new list \n");
    //     goto err;
    // }

    NV_CUDA_CHECK(cudaMallocManaged((void**) &list->name, sizeof(char) * strlen(list_name), cudaMemAttachGlobal));
    strcpy(list->name, list_name);

    nv_array_init(&list->entries, MODULE_RECENT_ENTRY_SIZE);
    if (list->entries == NULL) {
        RC_LOG(ERR, "failed to allocate list entry array for list '%s' \n", list_name);
        goto err;
    }

skip:
    return 0;

err:
    return -1;
}

int module_recent_get_list_array(struct nv_array **array) 
{
    if (array == NULL)
        return -1;

    // module_recent_check_structure();
    *array = list_array;
    return 0;
}


void module_recent_check_structure(void)
{
    printf("checking module_recent structure \n");
    float usage_percentage = (float) list_array->size / (float) list_array->alloc;
    printf("usage percentage is: %f \n", usage_percentage);
}