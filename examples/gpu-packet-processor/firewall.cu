#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "firewall.h"
#include "kernel.h"
#include "module_recent.h"
#include "kernel_utils.h"

#include <rte_ether.h>
#include <rte_ip.h>
#include <rte_tcp.h>
#include <rte_udp.h>

#ifdef __cplusplus
}
#endif /* __cplusplus */

static __forceinline__ __device__ void nv_firewall_dev_decrease_ttl(struct rte_ipv4_hdr *ip_hdr) 
{
	ip_hdr->time_to_live -= 1;
	KLOG(DEBUG, "ttl:\t%u\n", ip_hdr->time_to_live);
}

static __forceinline__ __device__ int nv_firewall_dev_ips_match(uint32_t ip1, uint32_t ip2) 
{
	if (ip1 == 0 || ip2 == 0)
		return 1;

	return ip1 == ip2;
}

static __forceinline__ __device__ int nv_firewall_dev_ports_match(in_port_t port1, in_port_t port2) 
{
	if (port1 == 0 || port2 == 0) 
		return 1;

	return port1 == port2;
}

static __forceinline__ __device__ void nv_firewall_dev_drop_mbuf(struct rte_mbuf *mbuf)
{
	memset(mbuf, 0, sizeof * mbuf);
}

static __forceinline__ __device__ void nv_firewall_dev_print_rule(struct nv_firewall_rule *rule, int idx, const char* chain_name, size_t n)
{
	KLOG(DEBUG, "[%i:%s] rule %lu matched: \n"
		   "\t\t rule->proto = %i \n"
		   "\t\t rule->operation = %i \n"
		   "\t\t rule->jump_chain = %s \n"
		   "\t\t rule->src_ip = %u \n"
		   "\t\t rule->dst_ip = %u \n"
		   "\t\t rule->src_port = %u \n"
		   "\t\t rule->dst_port = %u \n",
		   idx, chain_name, n,
			rule->proto,
			rule->operation,
			rule->jump_chain,
			rule->src_ip.v4,
			rule->dst_ip.v4,
			rule->src_port,
			rule->dst_port);
}

/* return ret=0 if no error, ret=1 if packet is dropped, ret<0 on error */
static __device__ int nv_firewall_dev_rule_match(struct packet *p, struct packet_values *pv, struct nv_firewall_chain **chain_ptr, struct nv_firewall *nv_fw, int idx)
{
	struct nv_firewall_chain *next_chain = NULL, *chain = *chain_ptr;
	struct nv_firewall_rule *rule;
	size_t n = 0;
	int ret = 0;
	int err;
	
	if (UNLIKELY(p == NULL)) {
		KLOG(ERR, "%i: p is null \n", idx);
		ret = -1;
		goto exit;
	}

	if (UNLIKELY(pv == NULL)) {
		KLOG(ERR, "%i: pv is null \n", idx);
		ret = -2;
		goto exit;
	}

	if (UNLIKELY(chain == NULL)) {
		KLOG(ERR, "%i: chain is null \n", idx);
		ret = -3;
		goto exit;
	}

	nv_array_for_each(rule, chain->entries) {
		in_addr_t src_filter = rule->src_ip.v4;
		in_addr_t dst_filter = rule->dst_ip.v4;
		in_port_t sport_filter = rule->src_port;
		in_port_t dport_filter = rule->dst_port;
		int16_t proto = rule->proto;
		enum pktop ip_op = rule->operation;
		const char *next_chain_name = rule->jump_chain;

		bool ip_matched = nv_firewall_dev_ips_match(pv->src_ip, src_filter) && nv_firewall_dev_ips_match(pv->dst_ip, dst_filter);
		bool port_matched = nv_firewall_dev_ports_match(pv->sport, sport_filter) && nv_firewall_dev_ports_match(pv->dport, dport_filter);
		bool proto_matched = proto == p->ipv4_hdr->next_proto_id || proto == IPPROTO_ANY;

		++n;

		KLOG(DEBUG, "checking rule %u from chain %s. ip_matched: %u, port_match: %u (sport: %u vs %u, dport: %u vs %u), proto_match: %u \n", n - 1, chain->name, ip_matched, port_matched, pv->sport, sport_filter, pv->dport, dport_filter, proto_matched);

		if (!(ip_matched && port_matched && proto_matched))
			continue;

		// nv_firewall_dev_print_rule(rule, idx, chain->name, n - 1);

		/* handle match */
		bool do_operation = true;

		uint8_t *src_ip = (uint8_t *) &pv->src_ip;
		uint8_t *dst_ip = (uint8_t *) &pv->dst_ip;

		/*
		KLOG(ERR, "[%s] protocol: %s, ip: %u.%u.%u.%u -> %u.%u.%u.%u, port: %u -> %u, rule: %s \n",
			chain->name,
			p->ipv4_hdr->next_proto_id == IPPROTO_UDP ? "UDP" : "TCP",
			src_ip[0], src_ip[1], src_ip[2], src_ip[3],
			dst_ip[0], dst_ip[1], dst_ip[2], dst_ip[3],
			pv->sport, pv->dport,
			ip_op == PKTOP_DROP ? "DROP" : (ip_op == PKTOP_JUMP ? "JUMP" : "UNKNOWN"));
		*/

		if (FLAG_IS_SET(ip_op, PKTOP_RECENT_SET)) {
			module_recent_dev_set(pv->src_ip,  rule->recent_list_name, nv_fw->module_recent_lists);
		} else if (FLAG_IS_SET(ip_op, PKTOP_RECENT_REMOVE)) {
			module_recent_dev_remove(pv->src_ip, rule->recent_list_name, nv_fw->module_recent_lists);
		} else if (FLAG_IS_SET(ip_op, PKTOP_RECENT_RCHECK)) {
			err = module_recent_dev_rcheck(pv->src_ip, rule->recent_list_name, &do_operation, nv_fw->module_recent_lists);
			if (err != 0) {
				KLOG(ERR, "error occured in device recent rcheck code \n");
				ret = err;
				goto exit;
			}
		}

		if (!do_operation) {
			KLOG(DEBUG, "skipping operation \n");
			continue;
		}
		
		if (FLAG_IS_SET(ip_op, PKTOP_ACCEPT)) {
			break;
		} else if (FLAG_IS_SET(ip_op, PKTOP_DROP)) {
			KLOG(DEBUG, "[%s] PKTOP_DROP flag is set \n", chain->name);
			nv_firewall_dev_drop_mbuf(p->mbuf);
			ret = 1;
			break;
		} else if (FLAG_IS_SET(ip_op, PKTOP_JUMP)) {
			KLOG(DEBUG, "[%s] PKTOP_JUMP flag is set \n", chain->name);

			struct nv_firewall_chain *ch = nv_firewall_get_chain(nv_fw->chains, next_chain_name);
			if (ch == NULL)
				KLOG(ERR, "chain search for '%s' failed \n", ch->name);
			else
				KLOG(DEBUG, "found next chain with name '%s' \n", ch->name);
			
			next_chain = ch;
			break;
		} else {
			KLOG(DEBUG, "[%s] no pktop set, continuing rule matching \n", chain->name);
		}
	}

	*chain_ptr = next_chain;

exit:
	return ret;
}

static __device__ int nv_firewall_dev_strip_packet(struct packet *p)
{
	if (UNLIKELY(p->mbuf == NULL)) {
		KLOG(ERR, "ether_ptr is NULL!\n");
		return -1;
	}

	uintptr_t mbuf_ptr = (uintptr_t) p->mbuf;

	/* ethernet header */
	uint8_t *ether_hdr_ptr = (uint8_t *) (mbuf_ptr + RTE_PKTMBUF_HEADROOM);
	p->ether_hdr = (struct rte_ether_hdr*) ether_hdr_ptr;
	if (UNLIKELY(p->ether_hdr == NULL)) {
		KLOG(ERR, "ethernet header is null\n");
		return -2;
	}
	
	/* ipv4 header */
	uint8_t *ip_hdr_ptr = (uint8_t *) (ether_hdr_ptr + sizeof(struct rte_ether_hdr));
	p->ipv4_hdr = (struct rte_ipv4_hdr *) ip_hdr_ptr;
	if (UNLIKELY(p->ipv4_hdr == NULL)) {
		KLOG(ERR, "ipv4 header is null\n");
		return -3;
	}

	/* udp or tcp header */
	const uint8_t ip_hdr_len = (p->ipv4_hdr->version_ihl & RTE_IPV4_HDR_IHL_MASK) * RTE_IPV4_IHL_MULTIPLIER;
	uint8_t *proto_hdr_ptr = (uint8_t *) (ip_hdr_ptr + ip_hdr_len);
	
	switch (p->ipv4_hdr->next_proto_id) {
	case IPPROTO_UDP:
		p->udp_hdr = (struct rte_udp_hdr *) proto_hdr_ptr;
		break;
	case IPPROTO_TCP:
		p->tcp_hdr = (struct rte_tcp_hdr *) proto_hdr_ptr;
		break;
	default:
		KLOG(ERR, "unhandled protocol id: %i\n", p->ipv4_hdr->next_proto_id);
		return -4;
	};

	return 0;
}

__device__ int nv_firewall_dev_proc_packet(uintptr_t *mbuf_ptr, struct packet *p, struct nv_firewall *nv_fw, int idx)
{
	struct nv_firewall_chain *chain = (typeof(chain)) nv_array_first(nv_fw->chains);
	struct packet_values pv = {0};
	int ret = 0;

	if (UNLIKELY((p->mbuf = (struct rte_mbuf *) mbuf_ptr) == NULL)) {
		KLOG(ERR, "mbuf is null\n");
		ret = -1;
		goto err;
	}

	/* parse packet */
	if (UNLIKELY(nv_firewall_dev_strip_packet(p) != 0)) {
		KLOG(ERR, "failed to strip packet! \n");
		ret = -2;
		goto err;
	}
	
	
	/* extract values */
	pv.src_ip = (in_addr_t) p->ipv4_hdr->src_addr;
	pv.dst_ip = (in_addr_t) p->ipv4_hdr->dst_addr;

	if (p->udp_hdr != NULL) {
		pv.sport = p->udp_hdr->src_port;
		pv.dport = p->udp_hdr->dst_port;
	} else if (p->tcp_hdr != NULL) {
		pv.sport = p->tcp_hdr->src_port;
		pv.dport = p->tcp_hdr->dst_port;
	} else {
		KLOG(ERR, "protocol headers are null\n");
		ret = -3;
		goto err;
	}

	/* TODO: these macros should not be used in performant code. stop gap solution */
	pv.sport = RTE_BE16(pv.sport);
	pv.dport = RTE_BE16(pv.dport);

	if (p->udp_hdr != NULL) {
		KLOG(DEBUG, "UDP header \n"
			   "\t sport: %hu \n"
			   "\t dport: %hu \n"
			   "\t len: %hu \n",
				pv.sport, 
				pv.dport,
				p->udp_hdr->dgram_len);
	} else if (p->tcp_hdr != NULL) {
		KLOG(DEBUG, "TCP header \n"
			   "\t sport: %hu \n"
			   "\t dport: %hu \n"
			   "\t flags: %hu \n",
				pv.sport, 
				pv.dport,
				p->tcp_hdr->tcp_flags);
	}

	/* rule matching starting on default input chain */
	while (chain != NULL) {
		ret = nv_firewall_dev_rule_match(p, &pv, &chain, nv_fw, idx);
	}
	
err:
	return ret;
}

__device__ void nv_firewall_dev_mac_swap(struct packet *p) 
{
	if (UNLIKELY(p == NULL)) {
		KLOG(ERR, "macswap: packet ptr is null! \n");
		return;
	}
	
	struct rte_ether_hdr *ether_hdr = p->ether_hdr;
	uint16_t *src_mac = (uint16_t *) &ether_hdr->s_addr;
	uint16_t *dst_mac = (uint16_t *) &ether_hdr->d_addr;

	uint16_t temp;
	temp = dst_mac[0];
	dst_mac[0] = src_mac[0];
	src_mac[0] = temp;
	temp = dst_mac[1];
	dst_mac[1] = src_mac[1];
	src_mac[1] = temp;
	temp = dst_mac[2];
	dst_mac[2] = src_mac[2];
	src_mac[2] = temp;
	
	KLOG(DEBUG, "mac swapped ether header: \n"
		   "\t s_addr: %02X.%02X.%02X.%02X.%02X.%02X \n"
		   "\t d_addr: %02X.%02X.%02X.%02X.%02X.%02X \n",
			src_mac[0], src_mac[1], src_mac[2], src_mac[3], src_mac[4], src_mac[5],
			dst_mac[0], dst_mac[1], dst_mac[2], dst_mac[3], dst_mac[4], dst_mac[5]);
}

static __global__ void nv_firewall_debug_print_dev(struct nv_firewall *nv_fw)
{
	struct nv_firewall_chain *chain;
	struct nv_firewall_rule *rule;

	if (UNLIKELY(nv_fw == NULL))
		return;

	nv_array_for_each(chain, nv_fw->chains) {
		KLOG(DEBUG, "nv_fw: chain name: '%s' \n", chain->name);

		nv_array_for_each(rule, chain->entries) {
			KLOG(DEBUG, "\t rule \n"
				   "\t\t rule->proto = %i \n"
				   "\t\t rule->operation = %i \n"
				   "\t\t rule->jump_chain = %s \n"
				   "\t\t rule->src_ip = %u \n"
				   "\t\t rule->dst_ip = %u \n"
				   "\t\t rule->src_port = %u \n"
				   "\t\t rule->dst_port = %u \n",
					rule->proto,
					rule->operation,
					rule->jump_chain,
					rule->src_ip.v4,
					rule->dst_ip.v4,
					rule->src_port,
					rule->dst_port);
		}
	}
}

void nv_firewall_debug_print(struct nv_firewall *nv_fw) {
#ifdef DEBUG_PRINT
	NV_CUDA_CHECK(cudaGetLastError());
	nv_firewall_debug_print_dev<<<1, 1, 0>>>(nv_fw);
	NV_CUDA_CHECK(cudaGetLastError());
#endif
}

__host__ __device__ struct nv_firewall_chain* nv_firewall_get_chain(struct nv_array *chain_array, const char *chain_name)
{
    struct nv_firewall_chain *chain;
	
	if (UNLIKELY(chain_name == NULL))
		return NULL;

    nv_array_for_each(chain, chain_array) {
#ifdef __CUDA_ARCH__
        if (dstrcmp(chain->name, chain_name) == 0) {
#else
        if (strcmp(chain->name, chain_name) == 0) {
#endif
            return chain;
        }
    }

    return NULL;
}