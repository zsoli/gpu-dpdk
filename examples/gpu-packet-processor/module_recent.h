#ifndef MODULE_RECENT_H
#define MODULE_RECENT_H

#include <stdbool.h>
#include <netinet/in.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <stddef.h>

#include "array.h"

#define DEFAULT_LIST_NAME "default"
#define RC_LOG(level, fmt, args...) \
    rte_log(RTE_LOG_ ## level, RTE_LOGTYPE_GPU_FW, "MODULE_RECENT: " fmt, ## args)

enum recent_operation {
    RECEMT_OP_SET,
    RECEMT_OP_REMOVE,
    RECEMT_OP_RCHECK,
};

struct mod_recent_data {
    char *list_name;
};

struct module_recent_list {
    char *name;
    struct nv_array *entries;
};

/* funcs */
int module_recent_get_list_array(struct nv_array **array);

int module_recent_init(void);

int module_recent_destroy(void);

int module_recent_add_list(const char *name);

void __host__ __device__ module_recent_print_data(struct nv_array *list_array);

int module_recent_add_entry(in_addr_t ipv4, const char *list_name);

void module_recent_check_structure(void);

__device__ int module_recent_dev_set(in_addr_t ipv4, const char *list_name, struct nv_array *list_array);

__device__ int module_recent_dev_remove(in_addr_t ipv4, const char *list_name, struct nv_array *list_array);

__device__ int module_recent_dev_rcheck(in_addr_t ipv4, const char *list_name, bool *found, struct nv_array *list_array);

#endif /* MODULE_RECENT_H */