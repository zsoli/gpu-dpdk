/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright (c) 2018-2019, NVIDIA CORPORATION.  All rights reserved.
 */

#include "firewall.h"
#include "utils.h"
#include "fw_conf_parser.h"
#include "module_recent.h"

#include <generic/rte_byteorder.h>
#include <rte_common.h>
#include <rte_ethdev.h>
#include <rte_log.h>
#include <rte_mbuf.h>
#include <rte_mbuf_core.h>
#include <rte_mbuf_ptype.h>
#include <rte_malloc.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

/* this must not be here, not in common.h, couses multiple definitions of temp_x */
TIMER_DEF_MAIN;
TIMER_DEF_INIT;

/* firewall variables */
struct nv_firewall *fw;

/* ================ NV variables ================ */

/* ++++++ Input options ++++++ */
static uint32_t l2fwd_enabled_port_mask = 0;	//mask of enabled ports
//static int l2fwd_rx_queue_per_lcore     = 1;
static int l2fwd_mem_type = MEM_HOST_PINNED;
static int l2fwd_gpu_device_id = 0;
static int l2fwd_workload = GPU_WORKLOAD;
static int l2fwd_nb_streams = 1;
static int l2fwd_port_id = 0;
static int l2fwd_data_room_size = RTE_MBUF_DEFAULT_DATAROOM;
static int l2fwd_pkt_burst_size = DEFAULT_PKT_BURST;
static int l2fwd_num_cores = 1;
static int l2fwd_queue_elems = 8192;
static int l2fwd_warmup_packets = 1000000;
static int l2fwd_performance_packets = 10000000;
static int l2fwd_burst_use_payload = 1;	//Best option for performance
static int l2fwd_nvprofiler = 0;
static int l2fwd_num_rx_queue = 1;
static int l2fwd_num_tx_queue = 1;
static int l2fwd_use_cuda_event = 0;
static int l2fwd_mempool_cache = RTE_MEMPOOL_CACHE_MAX_SIZE;
static int l2fwd_nb_mbufs = DEF_NB_MBUF;
static int l2fwd_mac_update = 1;

/* ++++++ Global memory structures ++++++ */
struct nvl2fwd_streams {
	cudaStream_t *l2fwd_streams;
	int cur_stream;
	int tot_streams;
};
struct nvl2fwd_streams nv_str_handler[MAX_RX_QUEUE_PER_LCORE];

struct nv_queue *nvqueue;
struct rte_ether_addr l2fwd_ports_eth_addr[RTE_MAX_ETHPORTS];
struct rte_mempool *mpool;
struct rte_pktmbuf_extmem ext_mem;
void *mpool_gpu_mem;
struct nv_burst_obj **rx_nv_obj = NULL;

struct lcore_queue_conf {
	unsigned n_rx_port;
	unsigned rx_port_list[MAX_RX_QUEUE_PER_LCORE];
} __rte_cache_aligned;
struct lcore_queue_conf lcore_queue_conf[RTE_MAX_LCORE];

static struct rte_eth_conf port_conf = {
	.rxmode = {
		   .split_hdr_size = 0,
		   .max_rx_pkt_len = 1514,
		   .offloads = DEV_RX_OFFLOAD_JUMBO_FRAME,
		   .mq_mode = ETH_MQ_RX_RSS,
		   },
	.rx_adv_conf = {
			.rss_conf = {
				     .rss_key = NULL,
				     .rss_hf = ETH_RSS_IP},
			},
	.txmode = {
		   .mq_mode = ETH_MQ_TX_NONE,
		   .offloads = 0,
		   },
};

volatile bool force_quit;
//Configurable number of RX/TX ring descriptors
uint16_t nb_rxd = 256;
uint16_t nb_txd = 256;

/* ================ TIMERS & STATS ================ */
/* Per-port statistics struct */
struct l2fwd_port_statistics {
	uint64_t tx_pkts[MAX_TX_QUEUE_PER_LCORE];
	uint64_t tx_brst[MAX_TX_QUEUE_PER_LCORE];
	uint64_t tx_bytes[MAX_TX_QUEUE_PER_LCORE];
	uint64_t tx_tot_ns[MAX_TX_QUEUE_PER_LCORE];

	uint64_t rx_pkts[MAX_RX_QUEUE_PER_LCORE];
	uint64_t rx_brst[MAX_RX_QUEUE_PER_LCORE];
	uint64_t rx_bytes[MAX_RX_QUEUE_PER_LCORE];
	uint64_t rx_warmup_bytes[MAX_RX_QUEUE_PER_LCORE];
	uint64_t rx_tot_ns[MAX_RX_QUEUE_PER_LCORE];

	uint64_t dropped[MAX_RX_QUEUE_PER_LCORE];
} __rte_cache_aligned;
struct l2fwd_port_statistics port_statistics[RTE_MAX_ETHPORTS];

int rx_force_quit[MAX_CORES] = { 0 };
int tx_force_quit[MAX_CORES] = { 0 };
int rx_measure[MAX_CORES] = { 0 };
int tx_measure[MAX_CORES] = { 0 };

struct timespec temp_rx_start[MAX_CORES];
struct timespec temp_rx_end[MAX_CORES];
struct timespec temp_tx_start[MAX_CORES];
struct timespec temp_tx_end[MAX_CORES];

static void print_stats(void) 
{
	struct rte_eth_stats stats;
	int index_queue = 0;
	uint64_t tot_core_rx_pkts = 0, tot_core_rx_byte = 0, tot_core_rx_wbyte = 0;
	double max_core_rx_time = 0, avg_core_rx_pkts = 0, avg_core_rx_byte = 0, avg_core_rx_wbyte = 0;

	uint64_t tot_core_tx_pkts = 0, tot_core_tx_byte = 0;
	double max_core_tx_time = 0, avg_core_tx_pkts = 0, avg_core_tx_byte = 0;

	uint64_t single_core_time = 0, single_core_pkts = 0, single_core_byte = 0;

	rte_eth_stats_get(l2fwd_port_id, &stats);

	const char clr[] = { 27, '[', '2', 'J', '\0' };
	const char topLeft[] = { 27, '[', '1', ';', '1', 'H', '\0' };

	/* Clear screen and move to top left */
	printf("%s%s", clr, topLeft);

	printf("\n\nStats ===============================\n\n");
	printf("Warmup packets: %d, performance packets: %d\n", l2fwd_warmup_packets, l2fwd_performance_packets);
	printf("Initialization: %7.2f us (%f sec)\n", ((double)TIMER_ELAPSED_NS_INIT) / 1000, (((double)TIMER_ELAPSED_NS_INIT) / 1.e9));
	printf("Main loop:  %7.2f us (%f sec)\n", ((double)TIMER_ELAPSED_NS_MAIN) / 1000, (((double)TIMER_ELAPSED_NS_MAIN) / 1.e9));

	printf("\n== RX:\n");

	printf("\tDPDK RX\n");
	for (index_queue = 0; index_queue < l2fwd_num_rx_queue; index_queue++)
		printf ("\t\tQueue %d: packets = %ld bytes = %ld dropped = %ld\n", index_queue, stats.q_ipackets[index_queue],
		     stats.q_ibytes[index_queue], stats.q_errors[index_queue]);

	printf("\tNVl2fwd RX\n");
	for (index_queue = 0; index_queue < l2fwd_num_rx_queue; index_queue++) {
		if (l2fwd_performance_packets > 0) {
			printf ("\t\tQueue %d: packets = %ld bursts = %ld bytes = %ld dropped = %ld core time = %7.2f us \n",
			     index_queue,
			     port_statistics[l2fwd_port_id].
			     rx_pkts[index_queue],
			     port_statistics[l2fwd_port_id].
			     rx_brst[index_queue],
			     port_statistics[l2fwd_port_id].
			     rx_bytes[index_queue],
			     port_statistics[l2fwd_port_id].
			     dropped[index_queue],
			     (((double) (port_statistics[l2fwd_port_id].  rx_tot_ns[index_queue])) / 1000));

			max_core_rx_time = MAX(max_core_rx_time, port_statistics[l2fwd_port_id].rx_tot_ns[index_queue]);
		} else {
			printf ("\t\tQueue %d: packets = %ld bursts = %ld bytes = %ld dropped = %ld\n",
			     index_queue,
			     port_statistics[l2fwd_port_id].
			     rx_pkts[index_queue],
			     port_statistics[l2fwd_port_id].
			     rx_brst[index_queue],
			     port_statistics[l2fwd_port_id].
			     rx_bytes[index_queue],
			     port_statistics[l2fwd_port_id].dropped[index_queue]);
		}

		tot_core_rx_pkts += port_statistics[l2fwd_port_id].rx_pkts[index_queue];
		tot_core_rx_byte += port_statistics[l2fwd_port_id].rx_bytes[index_queue];
		tot_core_rx_wbyte += port_statistics[l2fwd_port_id].rx_warmup_bytes[index_queue];
	}

	avg_core_rx_pkts = (double) tot_core_rx_pkts / l2fwd_num_rx_queue;
	avg_core_rx_byte = (double) tot_core_rx_byte / l2fwd_num_rx_queue;
	avg_core_rx_wbyte = (double) tot_core_rx_wbyte / l2fwd_num_rx_queue;

	printf("\n\tTOT DPDK ipackets: %ld, TOT nvl2fwd ipackets: %ld\n"
	       "\tTOT DPDK ibytes: %ld, TOT nvl2fwd ibytes: %ld\n",
	       stats.ipackets, tot_core_rx_pkts,
	       stats.ibytes, tot_core_rx_byte);

	if (l2fwd_performance_packets > 0) {
		printf("\tMAX Single core activity time: %7.2f us (%f sec)\n"
		       "\tAVG Packets: %7.2f\n"
		       "\tAVG Warmup bytes: %7.2f, Perf bytes: %7.2f\n",
		       ((double)max_core_rx_time) / 1000,
		       ((double)max_core_rx_time) / 1.e9, avg_core_rx_pkts,
		       avg_core_rx_wbyte, avg_core_rx_byte);

		printf("\n\tPerformance: %.2f pkts/sec, %.2f MB/s, %.2f Gbps\n",
		       ((double)tot_core_rx_pkts) / (((double)max_core_rx_time) / 1.e9),
		       (double)(tot_core_rx_byte / 1024.0 / 1024) / (((double)max_core_rx_time) / 1.e9),
		       ((((double)tot_core_rx_byte) * 8) / 1000 / 1000 / 1000) / (((double)max_core_rx_time) / 1.e9));
	}

	printf("\n== TX:\n");

	printf("\tDPDK TX\n");
	for (index_queue = 0; index_queue < l2fwd_num_tx_queue; index_queue++)
		printf("\t\tQueue %d: packets = %ld bytes = %ld\n", index_queue, stats.q_opackets[index_queue], stats.q_obytes[index_queue]);

	printf("\tNVl2fwd TX\n");
	for (index_queue = 0; index_queue < l2fwd_num_tx_queue; index_queue++) {
		if (l2fwd_performance_packets > 0) {
			printf ("\t\tQueue %d: packets = %ld bursts = %ld bytes = %ld dropped = %ld, core time = %7.2f us\n",
			     index_queue,
			     port_statistics[l2fwd_port_id].
			     tx_pkts[index_queue],
			     port_statistics[l2fwd_port_id].
			     tx_brst[index_queue],
			     port_statistics[l2fwd_port_id].
			     tx_bytes[index_queue],
			     port_statistics[l2fwd_port_id].
			     dropped[index_queue],
			     (((double) (port_statistics[l2fwd_port_id].tx_tot_ns[index_queue])) / 1000));

			max_core_tx_time = MAX(max_core_tx_time, port_statistics[l2fwd_port_id].tx_tot_ns[index_queue]);
		} else {
			printf ("\t\tQueue %d: packets = %ld bursts = %ld bytes = %ld dropped = %ld\n",
			     index_queue,
			     port_statistics[l2fwd_port_id].
			     tx_pkts[index_queue],
			     port_statistics[l2fwd_port_id].
			     tx_brst[index_queue],
			     port_statistics[l2fwd_port_id].
			     tx_bytes[index_queue],
			     port_statistics[l2fwd_port_id].dropped[index_queue]
			    );
		}

		tot_core_tx_pkts += port_statistics[l2fwd_port_id].tx_pkts[index_queue];
		tot_core_tx_byte += port_statistics[l2fwd_port_id].tx_bytes[index_queue];
	}

	avg_core_tx_pkts = (double) tot_core_tx_pkts / l2fwd_num_tx_queue;
	avg_core_tx_byte = (double) tot_core_tx_byte / l2fwd_num_tx_queue;

	printf("\n\tTOT DPDK opackets: %ld, TOT nvl2fwd opackets: %ld\n"
	       "\tTOT DPDK obytes: %ld, TOT nvl2fwd obytes: %ld\n",
	       stats.opackets, tot_core_tx_pkts,
	       stats.obytes, tot_core_tx_byte);

	if (l2fwd_performance_packets > 0) {
		printf("\tMAX Single core activity time: %7.2f us (%f sec)\n"
		       "\tAVG Packets: %7.2f\n"
		       "\tAVG Perf bytes: %7.2f\n",
		       ((double)max_core_tx_time) / 1000,
		       ((double)max_core_tx_time) / 1.e9, avg_core_tx_pkts,
		       avg_core_tx_byte);

		printf("\n\tPerformance: %.2f pkts/sec, %.2f MB/s, %.2f Gbps\n",
		       ((double) tot_core_tx_pkts) / (((double) max_core_tx_time) / 1.e9),
		       (double) (tot_core_tx_byte / 1024.0 / 1024) / (((double)max_core_tx_time) / 1.e9),
		       ((((double)tot_core_tx_byte) * 8) / 1000 / 1000 / 1000) / (((double)max_core_tx_time) / 1.e9));
	}

	if (l2fwd_num_cores == 1) {
		if (l2fwd_performance_packets > 0)
			single_core_time = MAX(max_core_rx_time, max_core_tx_time);

		single_core_pkts = MIN(tot_core_rx_pkts, tot_core_tx_pkts);

		if (tot_core_tx_byte > 0)
			single_core_byte = MIN(tot_core_rx_byte, tot_core_tx_byte);
		else
			single_core_byte = tot_core_rx_byte;

		if (l2fwd_performance_packets > 0) {
			printf ("\n== Single-core: (us=%.2f, pkts=%ld, bytes=%ld, MB=%.2f), %.2f pkts/sec, %.2f MB/sec, %.2f Gbps\n",
			     ((double)single_core_time) / 1000,
			     single_core_pkts, single_core_byte,
			     (((double)single_core_byte) / 1024 / 1024),
			     ((double)single_core_pkts) /
			     (((double)single_core_time) / 1.e9),
			     (((double)single_core_byte) / 1024 / 1024) /
			     (((double)single_core_time) / 1.e9),
			     (((((double)single_core_byte) / 1024 / 1024) /
			       (((double)single_core_time) / 1.e9)) * 0.008)
			    );
		} else {
			printf ("\n== Single-core: pkts=%ld, bytes=%ld, MB=%.2f\n", single_core_pkts, single_core_byte, (((double)single_core_byte) / 1024 / 1024));
		}
	}

	printf("\n== ERRORS:\n");
	printf ("Total of RX packets dropped by the HW, because there are no available buffer (i.e. RX queues are full)=%" PRIu64 "\n", stats.imissed);
	printf("Total number of erroneous received packets=%" PRIu64 "\n", stats.ierrors);
	printf("Total number of failed transmitted packets=%" PRIu64 "\n", stats.oerrors);
	printf("Total number of RX mbuf allocation failures=%" PRIu64 "\n", stats.rx_nombuf);
	printf("\n====================================================\n");
	fflush(stdout);
}

/* ================== CMD LINE OPTIONS ================== */
static void print_opts(void) 
{
	cudaError_t cuda_ret = cudaSuccess;
	struct cudaDeviceProp deviceProp;

	printf("============== INPUT OPTIONS ==============\n");
	printf("NV Mempool memory type = %s\n", (l2fwd_mem_type == MEM_HOST_PINNED ? "Host pinned memory" : "Device memory"));
	printf("RX/TX queues = %d, Mempools x queue = 1\n", l2fwd_num_rx_queue);
	printf("Mbuf payload size = %d\n", l2fwd_data_room_size);
	printf("Tot mbufs = %d, mbufs per mempool = %d\n", l2fwd_queue_elems, l2fwd_nb_mbufs);
	printf("DPDK cores = %d (%d RX and %d TX)\n", l2fwd_num_cores,
	       (l2fwd_num_cores == 1) ? 1 : l2fwd_num_cores / 2,
	       (l2fwd_num_cores == 1) ? 1 : l2fwd_num_cores / 2);

	if (l2fwd_workload == CPU_WORKLOAD)
		printf("Workload type = CPU_WORKLOAD\n");
	if (l2fwd_workload == NO_WORKLOAD)
		printf("Workload type = NO_WORKLOAD\n");
	if (l2fwd_workload == GPU_WORKLOAD)
		printf("Workload type = GPU_WORKLOAD\n");
	if (l2fwd_workload == GPU_PK_WORKLOAD)
		printf("Workload type = GPU_PK_WORKLOAD\n");
	if (l2fwd_workload == GPU_WORKLOAD || l2fwd_workload == GPU_PK_WORKLOAD || l2fwd_mem_type == MEM_DEVMEM) {
		NV_CUDA_CHECK(cudaSetDevice(l2fwd_gpu_device_id));
		cuda_ret = cudaGetDeviceProperties(&deviceProp, l2fwd_gpu_device_id);
		if (cuda_ret != cudaSuccess)
			return;

		printf("Using GPU #%d, %s, %04x:%02x:%02x\n",
		       l2fwd_gpu_device_id, deviceProp.name,
		       deviceProp.pciDomainID, deviceProp.pciBusID,
		       deviceProp.pciDeviceID);

		if (l2fwd_workload == GPU_WORKLOAD) {
			printf("CUDA streams x queue = %d\n", l2fwd_nb_streams);
			printf("Sync kernels with CUDA event = %s\n", (l2fwd_use_cuda_event == 1 ? "Yes" : "No"));
		}
	}
	printf("MAC update = %s\n", (l2fwd_mac_update == 1 ? "Yes" : "No"));
	printf("Device port number = %d\n", l2fwd_port_id);
	printf("RX pkts x burst = %d\n", l2fwd_pkt_burst_size);
	if (l2fwd_performance_packets > 0)
		printf("Warmup packets = %d, Performance packets = %d\n", l2fwd_warmup_packets, l2fwd_performance_packets);
	else
		printf ("Performance packets = infinite (stop execution with CTRL+C)\n");

	printf("Access packets from = %s\n",
	       (l2fwd_burst_use_payload == 1 ? "direct buffer pointer" : "rte_mbuf structure"));
	printf("============================================\n\n");
}

#define CMD_LINE_OPT_FIREWALL_CONF_FILE "firewall-config-file"

enum {
	/* long options mapped to a short option */

	/* first long only option value must be >= 256, so that we won't
	 * conflict with short options */
	CMD_LINE_OPT_MIN_NUM = 256,
	CMD_LINE_OPT_FIREWALL_CONF_FILE_NUM,
};

static const struct option lgopts[] = {
	{CMD_LINE_OPT_FIREWALL_CONF_FILE, required_argument, 0, CMD_LINE_OPT_FIREWALL_CONF_FILE_NUM}
};

static const char short_options[] = 
	"b:"			/* burst size */
    "c:"			/* core to use */
    "d:"			/* data room size */
    "e:"			/* nvqueue (rings) elems */
    "g:"			/* GPU device id */
    "h"				/* help */
    "m:"			/* mempool type */
    "s:"			/* number of streams x queue */
    "w:"			/* workload type */
    "B:"			/* create burst with mbufs->buf_addr payload pointer instead of mbufs (headers) */
    "E:"			/* use cudaEvent as synch method */
    "M:"			/* MAC update */
    "N"				/* Enable CUDA profiler */
    "P:"			/* performance packets */
    "W:"			/* warmup packets */
	"f:"			/* filter ip */
    ;

static int nvl2fwd_parse_args(int argc, char **argv) 
{
	int opt, ret = 0;
	char **argvopt;
	int option_index;
	char *prgname = argv[0];
	int totDevs;
	cudaError_t cuda_ret = cudaSuccess;
	argvopt = argv;

	while ((opt = getopt_long(argc, argvopt, short_options, lgopts, &option_index)) != EOF) {
		switch (opt) {
		case 'b':
			l2fwd_pkt_burst_size = l2fwd_parse_burst_size(optarg);
			if (l2fwd_pkt_burst_size < 0) {
				fprintf(stderr, "Invalid burst size\n");
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 'B':
			l2fwd_burst_use_payload =
			    l2fwd_parse_burst_use_payload(optarg);
			if (l2fwd_burst_use_payload < 0) {
				fprintf(stderr, "Invalid l2fwd_burst_use_payload\n");
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 'c':
			l2fwd_num_cores = l2fwd_parse_cores(optarg);
			if (l2fwd_num_cores < 0) {
				fprintf(stderr, "Invalid value for number of cpu threads (%d)\n",
					l2fwd_num_cores);
				return -1;
			}
			break;
		case 'd':
			l2fwd_data_room_size =
			    l2fwd_parse_dataroom_size(optarg);
			if (l2fwd_data_room_size < 0) {
				fprintf(stderr, "Invalid value for size of mbuf data room\n");
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 'e':
			l2fwd_queue_elems = l2fwd_parse_queue_elems(optarg);
			if (l2fwd_queue_elems == 0) {
				fprintf(stderr, "invalid queue elements number\n");
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 'E':
			l2fwd_use_cuda_event = atoi(optarg);
			break;
		case 'g':
			l2fwd_gpu_device_id = l2fwd_parse_device_id(optarg);
			break;
		case 'h':
			l2fwdnv_usage(prgname);
			return -2;
		case 'm':
			l2fwd_mem_type = l2fwd_parse_memtype(optarg);
			if (l2fwd_mem_type < 0)
				return -1;
			break;
		case 'M':
			l2fwd_mac_update = l2fwd_parse_mac_update(optarg);
			if (l2fwd_mac_update < 0)
				return -1;
			break;
		case 'N':
			l2fwd_nvprofiler = 1;
			break;
		case 'P':
			l2fwd_performance_packets = l2fwd_parse_performance_packets(optarg);
			if (l2fwd_performance_packets < 0) {
				fprintf(stderr, "invalid performance burst\n");
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 's':
			l2fwd_nb_streams = l2fwd_parse_nb_streams(optarg);
			if ((l2fwd_nb_streams < 0) || (l2fwd_nb_streams > MAX_NB_STREAMS)) {
				fprintf(stderr, "Invalid number of streams (%d)\n", l2fwd_nb_streams);
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 'W':
			l2fwd_warmup_packets =
			    l2fwd_parse_warmup_packets(optarg);
			if (l2fwd_warmup_packets < 0) {
				fprintf(stderr, "invalid warmup burst\n");
				l2fwdnv_usage(prgname);
				return -1;
			}
			break;
		case 'w':
			l2fwd_workload = l2fwd_parse_workload(optarg);
			if (l2fwd_workload < 0) {
				fprintf(stderr, "invalid l2fwd_workload\n");
				return -1;
			}
			break;
		/* long options */
		case CMD_LINE_OPT_FIREWALL_CONF_FILE_NUM:
			//parse_firewall_conf_file(optarg);
			if (fw == NULL) {
				if (nv_fw_parser_init_from_file(optarg, &fw) != 0) {
					printf("could not parse firewall config file \n");
					return -1;
				}

				if (fw == NULL) {
					LOG(ERR, "fw is null \n");
					return -1;
				}
			}
			break;
		default:
			l2fwdnv_usage(prgname);
			return -1;
		}
	}

	if (optind >= 0)
		argv[optind - 1] = prgname;

	ret = optind - 1;
	optind = 1;

	if (l2fwd_mem_type == MEM_DEVMEM && l2fwd_workload == CPU_WORKLOAD) {
		fprintf(stderr, "With device memory mempool, workload must be on GPU!\n");
		return -1;
	}

	if (l2fwd_mem_type == MEM_DEVMEM || l2fwd_workload == GPU_WORKLOAD || l2fwd_workload == GPU_PK_WORKLOAD) {
		cuda_ret = cudaGetDeviceCount(&totDevs);
		if (cuda_ret != cudaSuccess) {
			fprintf(stderr, "cudaGetDeviceCount error %d\n", cuda_ret);
			return -1;
		}

		if (totDevs < l2fwd_gpu_device_id) {
			fprintf(stderr, "Erroneous GPU device ID (%d). Tot GPUs: %d\n", l2fwd_gpu_device_id, totDevs);
			return -1;
		}
	}

	if ((l2fwd_num_cores + 1) > (int)rte_lcore_count()) {
		fprintf(stderr, "Required l2fwd_num_cores+1 (%d), cores launched=(%d)\n", l2fwd_num_cores + 1, rte_lcore_count());
		return -1;
	}

	if ((l2fwd_num_cores != 1) && (l2fwd_num_cores % 2 == 1)) {
		fprintf(stderr, "Odd number of cores %d\n", l2fwd_num_cores);
		return -1;
	}

	if (l2fwd_num_cores > 1) {
		l2fwd_num_rx_queue = l2fwd_num_cores / 2;
		l2fwd_num_tx_queue = l2fwd_num_cores / 2;
		if (l2fwd_num_rx_queue > MAX_RX_QUEUE_PER_LCORE) {
			fprintf(stderr, "l2fwd_num_rx_queue %d > MAX_RX_QUEUE_PER_LCORE %d\n", l2fwd_num_rx_queue, MAX_RX_QUEUE_PER_LCORE);
			return -1;
		}
	}
	//Run l2fwd-nv until CTRL+C -- no time stats measuraments
	if (l2fwd_performance_packets == 0)
		l2fwd_warmup_packets = 0;

	return ret;
}

/* =================== CUDA STREAM ====================== */
static void cudastream_setup(void) 
{
	int index, i;

	for (index = 0; index < l2fwd_num_rx_queue; index++) {
		if (l2fwd_nb_streams == 0) {
			nv_str_handler[index].tot_streams = 1;
			nv_str_handler[index].cur_stream = 0;
			nv_str_handler[index].l2fwd_streams = rte_zmalloc("rte_nv::streams", 1 * sizeof(cudaStream_t), 0);
			if (nv_str_handler[index].l2fwd_streams == NULL)
				rte_exit(EXIT_FAILURE, "rte_zmalloc streams failed\n");

			nv_str_handler[index].l2fwd_streams[0] = 0;
		} else {
			nv_str_handler[index].tot_streams = l2fwd_nb_streams;
			nv_str_handler[index].cur_stream = 0;
			nv_str_handler[index].l2fwd_streams = rte_zmalloc("rte_nv::streams", l2fwd_nb_streams * sizeof(cudaStream_t), 0);
			if (nv_str_handler[index].l2fwd_streams == NULL)
				rte_exit(EXIT_FAILURE, "rte_zmalloc streams failed\n");

			for (i = 0; i < l2fwd_nb_streams; i++)
				NV_CUDA_CHECK(cudaStreamCreateWithFlags(&(nv_str_handler[index].l2fwd_streams[i]), cudaStreamNonBlocking));
		}
	}
}

static void cudastream_finalize(void) 
{
	int i, j;

	if (l2fwd_workload == GPU_WORKLOAD) {
		NV_CUDA_CHECK(cudaDeviceSynchronize());
		printf("Destroying streams... ");
		for (i = 0; i < l2fwd_num_rx_queue; i++) {
			if (l2fwd_nb_streams > 0) {
				for (j = 0; j < nv_str_handler[i].tot_streams; j++)
					cudaStreamDestroy(nv_str_handler[i].  l2fwd_streams[j]);
			}

			rte_free(nv_str_handler[i].l2fwd_streams);
		}
	}

	printf("Done!\n");
}

/* ================== RX CORE ================== */
static void rx_core(int port_id, long queue_idx) 
{
	int nb_rx = 0;
	struct nv_queue *nq = &(nvqueue[queue_idx]);
	uint8_t flush_value = 0;

	if (l2fwd_performance_packets > 0 && rx_measure[queue_idx] == 0) {
		if (port_statistics[port_id].rx_pkts[queue_idx] >= (uint32_t) l2fwd_warmup_packets) {
			rx_measure[queue_idx] = 1;
			port_statistics[port_id].rx_pkts[queue_idx] = 0;
			TIMER_START_CORE_RX(queue_idx);
		}
	}

	PUSH_RANGE("next_obj", 1);

	if (rx_nv_obj[queue_idx] == NULL)  {
		if (NV_ACCESS_ONCE(nq->nv_obj[nq->rx_obj].busy.cnt) == 1) {
			nq->rx_obj = (nq->rx_obj + 1) % nq->tot_obj;
			return;	//Check quit condition
		}
		//read membarrier, avoid prediction
		rte_rmb();
		rx_nv_obj[queue_idx] = &(nq->nv_obj[nq->rx_obj]);
	}

	if (rx_nv_obj[queue_idx] == NULL)
		rte_exit(EXIT_FAILURE, "rx_nv_obj[%ld] still NULL. This should not happen!\n", queue_idx);

	POP_RANGE;

	PUSH_RANGE("rte_eth_rx_burst", 1);
	nb_rx = rte_eth_rx_burst(port_id, queue_idx, (struct rte_mbuf **)(rx_nv_obj[queue_idx]->hdr_burst_h), rx_nv_obj[queue_idx]->rxtx_burst);

	while (!force_quit && nb_rx < (rx_nv_obj[queue_idx]->rxtx_burst - DRIVER_MIN_RX_PKTS)) {
		nb_rx += rte_eth_rx_burst(port_id, queue_idx, (struct rte_mbuf **)(&(rx_nv_obj[queue_idx]->hdr_burst_h[nb_rx])), (rx_nv_obj[queue_idx]->rxtx_burst - nb_rx));
	}

	POP_RANGE;

	if (!nb_rx)
		return;

	port_statistics[port_id].rx_pkts[queue_idx] += nb_rx;

	if (rx_measure[queue_idx])
		port_statistics[port_id].rx_brst[queue_idx]++;

	//Prepare pkts to be visibile by both GPU and CPU
	PUSH_RANGE("nv_prepare_burst", 3);
	if (nv_prepare_burst(&(rx_nv_obj[queue_idx]), nb_rx,
			(l2fwd_workload == GPU_WORKLOAD ? nv_str_handler[queue_idx].l2fwd_streams[nv_str_handler[queue_idx].cur_stream] : NULL),
			NV_PKTMBUF_HOST)) {
		fprintf(stderr, "Error nv_prepare_burst\n");
		return;
	}
	POP_RANGE;

	if (l2fwd_workload == GPU_PK_WORKLOAD) {
		PUSH_RANGE("signal_pk", 4);
		nq->pk_ring[nq->rx_obj].ptr = (uintptr_t) (l2fwd_burst_use_payload == 1 ?  nvobj_get_burst_pl_dptr(rx_nv_obj[queue_idx]) : nvobj_get_burst_hdr_dptr(rx_nv_obj[queue_idx]));
		nq->pk_ring[nq->rx_obj].num_pkts = rx_nv_obj[queue_idx]->curr_mbuf_burst;
		nq->pk_ring[nq->rx_obj].nvobj_index = nq->rx_obj;
		//Ensure received packets have been actually written in device memory
		if (l2fwd_mem_type == MEM_DEVMEM) {
			flush_value = NV_ACCESS_ONCE(*((uint8_t *) nq->pk_flush_h));
			if (flush_value != NV_FLASH_VALUE) {
				fprintf(stderr, "Error flush_value(%d) != NV_FLASH_VALUE(%d)\n", flush_value, NV_FLASH_VALUE);
				return;
			}
		}

		POP_RANGE;
	} else if (l2fwd_workload == GPU_WORKLOAD) {
		PUSH_RANGE("macswap_gpu", 4);
		nvl2fwd_process_gpu((l2fwd_burst_use_payload == 1 ? nvobj_get_burst_pl_dptr(rx_nv_obj[queue_idx]) : nvobj_get_burst_hdr_dptr(rx_nv_obj[queue_idx])),	//pl_burst_dev_d
				    rx_nv_obj[queue_idx]->curr_mbuf_burst,
				    l2fwd_burst_use_payload,
				    rx_nv_obj[queue_idx]->stream,
				    l2fwd_use_cuda_event == 1 ? NULL : rx_nv_obj[queue_idx]->sync_flag,
				    l2fwd_mac_update,
					MAC_CUDA_BLOCKS,
				    MIN(MAC_THREADS_BLOCK, nb_rx),
					fw);
		POP_RANGE;

		if (l2fwd_use_cuda_event == 1)
			NV_CUDA_CHECK(cudaEventRecord (rx_nv_obj[queue_idx]->sync_event, rx_nv_obj[queue_idx]->stream));

		nv_str_handler[queue_idx].cur_stream = (nv_str_handler[queue_idx].cur_stream + 1) % nv_str_handler[queue_idx].tot_streams;
	}

	rte_mb();
	if (!(rte_atomic16_cmpset ((volatile uint16_t *)&(rx_nv_obj[queue_idx]->busy.cnt), 0, 1)))
		rte_exit(EXIT_FAILURE, "rte_atomic16_cmpset streams failed\n");

	if (l2fwd_workload == GPU_PK_WORKLOAD) {
		rte_wmb();
		NV_ACCESS_ONCE(nq->pk_ring[nq->rx_obj].status) = NV_PK_READY;
	}

	if (rx_measure[queue_idx])
		port_statistics[port_id].rx_bytes[queue_idx] += rx_nv_obj[queue_idx]->curr_burst_bytes;
	else
		port_statistics[port_id].rx_warmup_bytes[queue_idx] += rx_nv_obj[queue_idx]->curr_burst_bytes;

	nq->rx_obj = (nq->rx_obj + 1) % nq->tot_obj;
	rx_nv_obj[queue_idx] = NULL;

	if (rx_measure[queue_idx] && port_statistics[port_id].rx_pkts[queue_idx] >= (uint32_t) l2fwd_performance_packets) {
		if (l2fwd_workload == GPU_PK_WORKLOAD)
			printf ("Closing RX core (%ld), received packets = %ld, pkring (%p) index=%d\n",
			     queue_idx, port_statistics[port_id].rx_pkts[queue_idx], nq->pk_ring, nq->rx_obj);
		else
			printf("Closing RX core (%ld), received packets = %ld\n", queue_idx, port_statistics[port_id].rx_pkts[queue_idx]);

		TIMER_STOP_CORE_RX(queue_idx);
		port_statistics[port_id].rx_tot_ns[queue_idx] = TIMER_ELAPSED_NS_CORE_RX(queue_idx);
		rx_force_quit[queue_idx] = 1;	//Should be core oriented
	}
}

static int launch_rx_core(void *arg) 
{
	long queue_idx = (long)arg;

	printf("Starting RX Core %ld on lcore %d, socket %d\n", queue_idx, rte_lcore_id(), rte_socket_id());

	if (l2fwd_workload == GPU_WORKLOAD)
		NV_CUDA_CHECK(cudaSetDevice(l2fwd_gpu_device_id));

	PUSH_RANGE("rx loop", 1);
	while (!force_quit && !tx_force_quit[queue_idx]) {
		rx_core(l2fwd_port_id, queue_idx);
	}
	POP_RANGE;

	return 0;
}

/* ================== TX CORE ================== */
static void tx_core(int port_id, long queue_idx) 
{
	int nvobj_index = 0, nb_tx = 0;
	int tx_pkts = l2fwd_pkt_burst_size;

	struct nv_queue *nq = &(nvqueue[queue_idx]);
	struct nv_burst_obj *tx_nv_obj;
	struct rte_mbuf **tx_mbufs;

	if (l2fwd_performance_packets > 0 && tx_measure[queue_idx] == 0) {
		if (port_statistics[port_id].tx_pkts[queue_idx] >= (uint32_t) l2fwd_warmup_packets) {
			tx_measure[queue_idx] = 1;
			port_statistics[port_id].tx_pkts[queue_idx] = 0;
			TIMER_START_CORE_TX(queue_idx);
		}
	}

	if (l2fwd_workload == GPU_PK_WORKLOAD) {
		if (NV_ACCESS_ONCE(nq->pk_ring[nq->tx_obj].status) != NV_PK_DONE)
			return;	//Check quit condition
		rte_rmb();	//Avoid prediction

		if (!NV_ACCESS_ONCE(nq->nv_obj[nq->tx_obj].busy.cnt))
			rte_exit(EXIT_FAILURE, "TX core: NV_PK_DONE of a non busy object, nq->tx_obj=%d, nvobj_index=%d was 0", nq->tx_obj, nvobj_index);

		nvobj_index = nq->pk_ring[nq->tx_obj].nvobj_index;
		tx_nv_obj = &(nq->nv_obj[nvobj_index]);
		tx_pkts = tx_nv_obj->curr_mbuf_burst;
	} else {
		if (NV_ACCESS_ONCE(nq->nv_obj[nq->tx_obj].busy.cnt) == 0)
			return;	//Check quit condition
		rte_rmb();	//Avoid prediction
		tx_nv_obj = &(nq->nv_obj[nq->tx_obj]);
		tx_pkts = tx_nv_obj->curr_mbuf_burst;

		if (l2fwd_workload == GPU_WORKLOAD) {
			if (l2fwd_use_cuda_event == 1)
				cudaEventSynchronize(tx_nv_obj->sync_event);
			else
				while (NV_ACCESS_ONCE(tx_nv_obj->sync_flag[0]) == 0) ;
		}

		if (l2fwd_workload == CPU_WORKLOAD) {
			PUSH_RANGE("nvl2fwd_macswap_cpu", 7);
			nvl2fwd_macswap_cpu(
				(l2fwd_burst_use_payload == 1 ? nvobj_get_burst_pl_hptr(tx_nv_obj) : nvobj_get_burst_hdr_hptr(tx_nv_obj)),
				tx_pkts,
				l2fwd_burst_use_payload,
				l2fwd_mac_update);
			POP_RANGE;
		}
	}

	tx_mbufs = (struct rte_mbuf **) nvobj_get_burst_hdr_hptr(tx_nv_obj);

	PUSH_RANGE("rte_eth_tx_burst", 8);
	nb_tx = rte_eth_tx_burst(port_id, queue_idx, tx_mbufs, tx_pkts);
	while (1 && !force_quit) {
		if (nb_tx < tx_pkts) {
			nb_tx += rte_eth_tx_burst(port_id, queue_idx, &(tx_mbufs[nb_tx]), tx_pkts - nb_tx);
		}

		if (nb_tx == tx_pkts)
			break;
	}
	POP_RANGE;

	PUSH_RANGE("set_complete", 3);
	if (l2fwd_workload == GPU_PK_WORKLOAD) {
		rte_wmb();
		NV_ACCESS_ONCE(nq->pk_ring[nq->tx_obj].status) = NV_PK_FREE;
	} else {
		if (!l2fwd_use_cuda_event)
			tx_nv_obj->sync_flag[0] = 0;
	}
	rte_wmb();
	if (!(rte_atomic16_cmpset((volatile uint16_t *)&(nq->nv_obj[nq->tx_obj].busy.cnt), 1, 0)))
		rte_exit(EXIT_FAILURE, "rte_atomic16_cmpset streams failed\n");
	nq->tx_obj = (nq->tx_obj + 1) % nq->tot_obj;

	POP_RANGE;

	port_statistics[port_id].tx_pkts[queue_idx] += nb_tx;
	if (tx_measure[queue_idx]) {
		port_statistics[port_id].tx_brst[queue_idx]++;
		port_statistics[port_id].tx_bytes[queue_idx] += tx_nv_obj->curr_burst_bytes;
	}

	if (tx_measure[queue_idx] && port_statistics[port_id].tx_pkts[queue_idx] >= (uint32_t) l2fwd_performance_packets) {
		TIMER_STOP_CORE_TX(queue_idx);
		port_statistics[port_id].tx_tot_ns[queue_idx] = TIMER_ELAPSED_NS_CORE_TX(queue_idx);
		tx_force_quit[queue_idx] = 1;
		if (l2fwd_workload == GPU_PK_WORKLOAD)
			printf("Closing TX core (%ld), transmitted packets = %ld, pkring (%p) index=%d\n",
			     queue_idx,
			     port_statistics[port_id].tx_pkts[queue_idx],
			     nq->pk_ring, nq->tx_obj);
		else
			printf("Closing TX core (%ld), transmitted packets = %ld\n",
			     queue_idx,
			     port_statistics[port_id].tx_pkts[queue_idx]);

		fflush(stdout);
	}
}

static int launch_tx_core(void *arg) 
{
	long queue_idx = (long)arg;

	printf("Starting TX Core %ld on lcore %d, socket %d\n", queue_idx, rte_lcore_id(), rte_socket_id());

	if (l2fwd_workload == GPU_WORKLOAD)
		NV_CUDA_CHECK(cudaSetDevice(l2fwd_gpu_device_id));

	PUSH_RANGE("tx loop", 1);
	while (!force_quit && !tx_force_quit[queue_idx]) {
		tx_core(l2fwd_port_id, queue_idx);
	}
	POP_RANGE;

	return 0;
}

/* ================== SINGLE-CORE ================== */
static int launch_single_core( __attribute__ ((unused)) void *dummy) 
{
	printf("Starting single RX/TX on lcore %d\n", rte_lcore_id());

	if (l2fwd_workload == GPU_WORKLOAD)
		NV_CUDA_CHECK(cudaSetDevice(l2fwd_gpu_device_id));

	while (!force_quit && (!rx_force_quit[0] || !tx_force_quit[0])) {
		rx_core(l2fwd_port_id, 0);
		tx_core(l2fwd_port_id, 0);
	}

	return 0;
}

static void signal_handler(int signum) 
{
	if (signum == SIGINT || signum == SIGTERM || signum == SIGUSR1) {
		printf("\n\nSignal %d received, preparing to exit...\n", signum);
		NV_ACCESS_ONCE(force_quit) = 1;
	}
}

static void mpool_mem_register(struct rte_mempool *mp __rte_unused, void *opaque __rte_unused, struct rte_mempool_memhdr *memhdr,
							   unsigned mem_idx __rte_unused) 
{
	NV_CUDA_CHECK(cudaHostRegister(memhdr->addr, memhdr->len, cudaHostRegisterMapped));
	void *pDevice = NULL;
	NV_CUDA_CHECK(cudaHostGetDevicePointer(&pDevice, memhdr->addr, 0));
	if (pDevice != memhdr->addr)
		rte_exit(EXIT_FAILURE, "CPU pointer is not the same as GPU pointer while walking the mempool\n");
}

int main(int argc, char **argv) 
{
	struct rte_eth_dev_info dev_info;
	uint8_t socketid;
	int ret = 0, index_queue = 0, secondary_id = 0;
	uint16_t nb_ports;
	unsigned lcore_id;
	FILE *log_file = NULL;
	
	/* Prevent any useless profiling */
	cudaProfilerStop();

	/* write logs into file or stderr */
#ifdef LOG_TO_FILE
	log_file = fopen("gpu-dpdk-log.txt", "w+");
    if (log_file == NULL) 
		RTE_LOG(ERR, APP, "failed to create log file\n");
#endif /* LOG_TO_FILE */

	ret = rte_openlog_stream(log_file);
	if (ret != 0)
		fprintf(stderr, "failed to open log stream!\n");

	printf("************ L2FWD-NV ************\n\n");

	/* ================ PARSE ARGS ================ */
	/* init EAL */
	ret = rte_eal_init(argc, argv);
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "Invalid EAL arguments\n");
	argc -= ret;
	argv += ret;

	/* init firewall modules before parsing */
	ret = module_recent_init();
	if (ret != 0)
		rte_exit(EXIT_FAILURE, "failed to initialize module recent \n");
	
	/* parse application arguments (after the EAL ones) */
	ret = nvl2fwd_parse_args(argc, argv);
	if (ret == -2)
		rte_exit(EXIT_SUCCESS, "\n");
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "Invalid NVL2FWD arguments\n");

	if (fw == NULL) {
		nv_firewall_init(&fw);
		LOG(INFO, "fw ptr was null\n");
	}
	
	ret = module_recent_get_list_array(&fw->module_recent_lists);
	if (ret != 0)
		rte_exit(EXIT_FAILURE, "failed to get recent module array ptr\n");

	/* ================ FORCE QUIT HANDLER ================ */
	NV_ACCESS_ONCE(force_quit) = 0;
	signal(SIGINT, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGUSR1, signal_handler);

	/* ================ CUDA CONTEXT ================ */
	cudaSetDevice(l2fwd_gpu_device_id);
	cudaFree(0);

	TIMER_START_INIT;

	/* ================ DEVICE SETUP ================ */
	nb_ports = rte_eth_dev_count_avail();
	if (nb_ports == 0)
		rte_exit(EXIT_FAILURE, "No Ethernet ports - bye\n");
	ret = -1;

	rte_eth_dev_info_get(l2fwd_port_id, &dev_info);
	printf("\nDevice driver name in use: %s... \n", dev_info.driver_name);

	if (strcmp(dev_info.driver_name, "mlx5_pci") == 0)
		port_conf.rx_adv_conf.rss_conf.rss_hf = ETH_RSS_IP;
	else 
		rte_exit(EXIT_FAILURE, "Non-Mellanox NICs have not been validated in l2fwd-nv\n");

	/* ================ MEMPOOL ================ */
	l2fwd_nb_mbufs = l2fwd_queue_elems;

	ext_mem.elt_size = l2fwd_data_room_size + RTE_PKTMBUF_HEADROOM;
	ext_mem.buf_len = RTE_ALIGN_CEIL(l2fwd_nb_mbufs * ext_mem.elt_size, GPU_PAGE_SIZE);
	if (l2fwd_mem_type == MEM_HOST_PINNED) {
		ext_mem.buf_ptr = rte_malloc("extmem", ext_mem.buf_len, 0);
		NV_CUDA_CHECK(cudaHostRegister(ext_mem.buf_ptr, ext_mem.buf_len, cudaHostRegisterMapped));
		void *pDevice;
		NV_CUDA_CHECK(cudaHostGetDevicePointer(&pDevice, ext_mem.buf_ptr, 0));
		if (pDevice != ext_mem.buf_ptr)
			rte_exit(EXIT_FAILURE, "GPU pointer does not match CPU pointer\n");
	} else {
		ext_mem.buf_iova = RTE_BAD_IOVA;
		NV_CUDA_CHECK(cudaMalloc(&ext_mem.buf_ptr, ext_mem.buf_len));
		if (ext_mem.buf_ptr == NULL)
			rte_exit(EXIT_FAILURE, "Could not allocate GPU memory\n");
		ret = rte_extmem_register(ext_mem.buf_ptr, ext_mem.buf_len, NULL, ext_mem.buf_iova, GPU_PAGE_SIZE);
		if (ret)
			rte_exit(EXIT_FAILURE, "Could not register GPU memory\n");
	}
	ret = rte_dev_dma_map(rte_eth_devices[l2fwd_port_id].device, ext_mem.buf_ptr, ext_mem.buf_iova, ext_mem.buf_len);
	if (ret)
		rte_exit(EXIT_FAILURE, "Could not DMA map EXT memory\n");
	mpool = rte_pktmbuf_pool_create_extbuf("vidmem_mpool", l2fwd_nb_mbufs,
			l2fwd_mempool_cache, 0, ext_mem.elt_size, rte_socket_id(),
			&ext_mem, 1);
	if (mpool == NULL)
		rte_exit(EXIT_FAILURE, "Could not create EXT memory mempool\n");

	// kernel will access rte_mbuf, we need to register the memory backing the mempool so that it's accessible from the GPU
	if (!l2fwd_burst_use_payload) {
		// not the case
		printf("Registering mbuffers in the mempool for the GPU\n");
		rte_mempool_mem_iter(mpool, mpool_mem_register, NULL);
	}

	/* ================ PORT 0 SETUP ================ */
	printf("Initializing port %u with %d RX queues and %d TX queues...\n", l2fwd_port_id, l2fwd_num_rx_queue, l2fwd_num_tx_queue);

	ret = rte_eth_dev_configure(l2fwd_port_id, l2fwd_num_rx_queue, l2fwd_num_tx_queue, &port_conf);
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "Cannot configure device: err=%d, port=%u\n", ret, l2fwd_port_id);

	ret = rte_eth_dev_adjust_nb_rx_tx_desc(l2fwd_port_id, &nb_rxd, &nb_txd);
	if (ret < 0)
		rte_exit(EXIT_FAILURE, "Cannot adjust number of descriptors: err=%d, port=%u\n", ret, l2fwd_port_id);

	rte_eth_macaddr_get(l2fwd_port_id, &l2fwd_ports_eth_addr[l2fwd_port_id]);

	/* ================ RX QUEUES ================ */
	for (index_queue = 0; index_queue < l2fwd_num_rx_queue; index_queue++) {
		socketid = (uint8_t) rte_lcore_to_socket_id(index_queue);
		ret = rte_eth_rx_queue_setup(l2fwd_port_id, index_queue, nb_rxd, socketid, NULL, mpool);

		if (ret < 0)
			rte_exit(EXIT_FAILURE, "rte_eth_rx_queue_setup: err=%d, port=%u\n", ret, l2fwd_port_id);
	}

	/* ================ TX QUEUES ================ */
	for (index_queue = 0; index_queue < l2fwd_num_tx_queue; index_queue++) {
		socketid = (uint8_t) rte_lcore_to_socket_id(index_queue);
		//printf("Init TX queue %d on port %d, socketid %d\n", index_queue, l2fwd_port_id, socketid);
		ret = rte_eth_tx_queue_setup(l2fwd_port_id, index_queue, nb_txd, socketid, NULL);
		if (ret < 0)
			rte_exit(EXIT_FAILURE, "rte_eth_tx_queue_setup: err=%d, port=%u\n", ret, l2fwd_port_id);
	}

	/* ================ START DEVICE ================ */
	ret = rte_eth_dev_start(l2fwd_port_id);
	if (ret != 0)
		rte_exit(EXIT_FAILURE, "rte_eth_dev_start:err=%d, port=%u\n", ret, l2fwd_port_id);

	rte_eth_promiscuous_enable(l2fwd_port_id);
	printf("Port %u, MAC address: %02X:%02X:%02X:%02X:%02X:%02X\n\n",
	       l2fwd_port_id,
	       l2fwd_ports_eth_addr[l2fwd_port_id].addr_bytes[0],
	       l2fwd_ports_eth_addr[l2fwd_port_id].addr_bytes[1],
	       l2fwd_ports_eth_addr[l2fwd_port_id].addr_bytes[2],
	       l2fwd_ports_eth_addr[l2fwd_port_id].addr_bytes[3],
	       l2fwd_ports_eth_addr[l2fwd_port_id].addr_bytes[4],
	       l2fwd_ports_eth_addr[l2fwd_port_id].addr_bytes[5]);

	memset(&port_statistics, 0, sizeof(port_statistics));
	check_all_ports_link_status(l2fwd_enabled_port_mask);

	print_opts();

	/* ================ SETUP NVQUEUE ================ */
	nvqueue = nv_create_nvqueue((l2fwd_num_cores == 1) ? 1 : (l2fwd_num_cores / 2),	//should be 1 for each couple of cores 
				    l2fwd_nb_mbufs,	//l2fwd_queue_elems, 
				    l2fwd_pkt_burst_size, 0, (l2fwd_workload == GPU_PK_WORKLOAD ? NVQ_PERSISTENT : NVQ_DEFAULT));
	if (nvqueue == NULL)
		rte_exit(EXIT_FAILURE, "nv_create_nvqueue\n");

	if (l2fwd_workload == GPU_WORKLOAD)
		cudastream_setup();

	TIMER_STOP_INIT;

	rx_nv_obj = (struct nv_burst_obj **) rte_zmalloc_socket("rx_nv_obj",
					(l2fwd_num_cores == 1 ?  l2fwd_num_cores : l2fwd_num_cores / 2) * sizeof(struct nv_burst_obj *), 0, rte_eth_dev_socket_id (l2fwd_port_id));
	if (rx_nv_obj == NULL)
		rte_exit(EXIT_FAILURE, "rx_nv_obj\n");

	/* ================ SETUP PERSISTENT KERNEL ================ */
	if (l2fwd_workload == GPU_PK_WORKLOAD) {
		printf("PERSISTENT KERNEL ENABLED\n");
		for (index_queue = 0; index_queue < l2fwd_num_rx_queue; index_queue++) {
			nvl2fwd_persistent_kernel(nvqueue[index_queue].pk_ring, nvqueue[index_queue].tot_obj, (uint8_t *) nvqueue[index_queue].pk_quit_d,	//used by RX core to flush GDR writes from NIC
						  l2fwd_burst_use_payload,	//ptr will refer to mbufs headers or mbufs payloads
						  l2fwd_mac_update,
						  PK_CUDA_BLOCKS,
						  PK_CUDA_THREADS,
						  nvqueue[index_queue].
						  pk_stream);
		}
	}

	if (l2fwd_nvprofiler)
		cudaProfilerStart();

	/* ================ START RX/TX CORES ================ */
	TIMER_START_MAIN;
	if (l2fwd_num_cores == 1) {
		secondary_id = rte_get_next_lcore(0, 1, 0);
		rte_eal_remote_launch(launch_single_core, NULL, secondary_id);

		RTE_LCORE_FOREACH_WORKER(lcore_id) {
			if (rte_eal_wait_lcore(lcore_id) < 0) {
				fprintf(stderr, "bad exit for coreid: %d\n", lcore_id);
				break;
			}
		}
	} else {
		secondary_id = 0;
		long icore = 0;
		while (icore < l2fwd_num_cores/2) {
			PUSH_RANGE("launch_txcore", 5);
			secondary_id = rte_get_next_lcore(secondary_id, 1, 0);
			rte_eal_remote_launch(launch_tx_core, (void *)icore, secondary_id);
			POP_RANGE;

			PUSH_RANGE("launch_rxcore", 6);
			secondary_id = rte_get_next_lcore(secondary_id, 1, 0);
			rte_eal_remote_launch(launch_rx_core, (void *)icore, secondary_id);
			POP_RANGE;

			icore++;
		}

		icore = 0;
		RTE_LCORE_FOREACH_WORKER(icore) {
			if (rte_eal_wait_lcore(icore) < 0) {
				fprintf(stderr, "bad exit for coreid: %ld\n", icore);
				break;
			}
		}
	}

	/* ================ WAITING FOR PERSISTENT KERNELS ================ */
	if (l2fwd_workload == GPU_PK_WORKLOAD) {
		printf("Killing nvqueue persistent kernel...\n");
		for (index_queue = 0; index_queue < ((l2fwd_num_cores == 1) ? 1 : (l2fwd_num_cores / 2)); index_queue++)
			((uint8_t *) nvqueue[index_queue].pk_quit_h)[0] = 1;

		NV_CUDA_CHECK(cudaDeviceSynchronize());
	}

	if (l2fwd_nvprofiler)
		cudaProfilerStop();

	TIMER_STOP_MAIN;

	if (l2fwd_workload == GPU_WORKLOAD)
		cudastream_finalize();

	ret = 0;

	/* ================ NV CLEANUP ================ */
	nv_destroy_nvqueue(&(nvqueue), (l2fwd_num_cores == 1) ? 1 : (l2fwd_num_cores / 2), l2fwd_nb_mbufs);

	ret = nv_firewall_destroy(fw);
	if (ret != 0) {
		FW_LOG(ERR, "failed to destroy gpu firewall \n");
	}

	ret = module_recent_destroy();
	if (ret != 0) {
		FW_LOG(ERR, "failed to destroy module recent \n");
	}

	/* ================ STATS ================ */
	if (0) /* stats not needed now */
		print_stats();

	/* ================ STOP DEVICE ================ */
	printf("Closing port %d...\n", l2fwd_port_id);

	ret = rte_eth_dev_stop(l2fwd_port_id);
	if (ret < 0)
		fprintf(stderr, "failed to stop device %d", l2fwd_port_id);
	else
		printf("Device %d stopped...\n", l2fwd_port_id);
	
	ret = rte_eth_dev_close(l2fwd_port_id);
	if (ret < 0)
		fprintf(stderr, "failed to close device %d\n", l2fwd_port_id);
	else
		printf("Device %d closed...\n", l2fwd_port_id);

	if (log_file != NULL)
		fclose(log_file);

	printf("Done. Bye!\n");
	return ret;
}
